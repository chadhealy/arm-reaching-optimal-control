function ciplotY(left,right,y,colour);
     
% ciplot(lower,upper)       
% ciplot(lower,upper,x)
% ciplot(lower,upper,x,colour)
%
% Plots a shaded region on a graph between specified lower and upper confidence intervals (L and U).
% l and u must be vectors of the same length.
% Uses the 'fill' function, not 'area'. Therefore multiple shaded plots
% can be overlayed without a problem. Make them transparent for total visibility.
% x data can be specified, otherwise plots against index values.
% colour can be specified (eg 'k'). Defaults to blue.

% Raymond Reynolds 24/11/06

if length(left)~=length(right)
    error('lower and upper vectors must be same length')
end

if nargin<4
    colour='b';
end

if nargin<3
    y=1:length(left);
end

% convert to row vectors so fliplr can work
if find(size(y)==(max(size(y))))<2
y=y'; end
if find(size(left)==(max(size(left))))<2
left=left'; end
if find(size(right)==(max(size(right))))<2
right=right'; end

fill([left fliplr(right)],[y fliplr(y)],colour,'EdgeColor','none')




