function varargout = displayAlphaFitResults(varargin)
% DISPLAYALPHAFITRESULTS MATLAB code for displayAlphaFitResults.fig
%      DISPLAYALPHAFITRESULTS, by itself, creates a new DISPLAYALPHAFITRESULTS or raises the existing
%      singleton*.
%
%      H = DISPLAYALPHAFITRESULTS returns the handle to a new DISPLAYALPHAFITRESULTS or the handle to
%      the existing singleton*.
%
%      DISPLAYALPHAFITRESULTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAYALPHAFITRESULTS.M with the given input arguments.
%
%      DISPLAYALPHAFITRESULTS('Property','Value',...) creates a new DISPLAYALPHAFITRESULTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before displayAlphaFitResults_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to displayAlphaFitResults_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help displayAlphaFitResults

% Last Modified by GUIDE v2.5 03-Jul-2014 14:49:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @displayAlphaFitResults_OpeningFcn, ...
                   'gui_OutputFcn',  @displayAlphaFitResults_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before displayAlphaFitResults is made visible.
function displayAlphaFitResults_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to displayAlphaFitResults (see VARARGIN)

% Choose default command line output for displayAlphaFitResults
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes displayAlphaFitResults wait for user response (see UIRESUME)
% uiwait(handles.figure1);

files = dir('DataFiles/*.mat');
bbb = 1;
% Only Loads File Names that contain the proper Data Structure
for a = 1:length(files)
    load(['DataFiles/',num2str(files(a).name)]);
    if exist('Data')
        handles.fileNames{bbb} = files(a).name;
        bbb = bbb + 1;
        clear Data
    end
end
if ~isfield(handles,'fileNames')
    error('No Data Files Found')    
end
set(handles.popupmenu1,'String',handles.fileNames)
set(handles.popupmenu1,'Value',1)
load(['DataFiles/',num2str(handles.fileNames{1})]);
handles.Data = Data;

handles.gridTypes = {'RMS Error','Q(1,1) & Q(7,7)','Q(2,2) & Q(8,8)',...
    'Q(3,3)','Q(5,5) & Q(6,6)','R(1,1) & R(2,2)','Phi(1,1) & Phi(2,2)',...
    'Phi(3,3) & Phi(4,4)','Phi(5,5) & Phi(6,6)'};
set(handles.popupmenu3,'String',handles.gridTypes)
set(handles.popupmenu3,'Value',1)

axes(handles.RMSgrid_axes)
colormap('hot');
handles.gridPlot = pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridAlpha);
handles.cb = colorbar;
shading interp
hold on
plot(Data.alphaValsGrid,Data.alphaFineValsGrid,'co','MarkerSize',4,...
    'MarkerFaceColor','none');
hold on;
if isfield(Data,'gridFlags')
    [ii,jj] = find(Data.gridFlags~=2);
    plot(Data.alphaVals(jj),Data.alphaFineVals(ii),'mo','MarkerSize',4)
end

hold off
set(gca,'XTick', Data.alphaVals)
set(gca,'YTick', Data.alphaFineVals)
% title('RMS Error')
xlabel('Alpha Values for Trajectory to Be Fit')
ylabel('Alpha Values for Resulting Fitted Trajectory')

hold on;
handles.gridMarker = plot(handles.RMSgrid_axes,Data.alphaVals(1),Data.alphaFineVals(1),'o','markerfacecolor','b','markersize',10);
hold off;

set(handles.horiz_slider,'Min',Data.alphaVals(1))
set(handles.horiz_slider,'Max',Data.alphaVals(end))
set(handles.horiz_slider,'SliderStep',...
    [1/(length(Data.alphaVals)-1), 1/(length(Data.alphaVals)-1)])
set(handles.horiz_slider,'Value',Data.alphaVals(1))

set(handles.vert_slider,'Min',Data.alphaFineVals(1))
set(handles.vert_slider,'Max',Data.alphaFineVals(end))
set(handles.vert_slider,'SliderStep',...
    [1/(length(Data.alphaFineVals)-1), 1/(length(Data.alphaFineVals)-1)])
set(handles.vert_slider,'Value',Data.alphaFineVals(1))

% Update To Fit Parameter Values
set(handles.initAlpha,'String',num2str(Data.alphaVals(1)));
set(handles.initQ1177,'String',num2str(Data.toFitParams.Q(1,1)));
set(handles.initQ2288,'String',num2str(Data.toFitParams.Q(2,2)));
set(handles.initQ33,'String',num2str(Data.toFitParams.Q(3,3)));
set(handles.initQ5566,'String',num2str(Data.toFitParams.Q(5,5)));
set(handles.initR1122,'String',num2str(Data.toFitParams.R(1,1),'%5.4g'));
set(handles.initPhi1122,'String',num2str(Data.toFitParams.Phi(1,1),'%5.4g'));
set(handles.initPhi3344,'String',num2str(Data.toFitParams.Phi(3,3),'%5.4g'));
set(handles.initPhi5566,'String',num2str(Data.toFitParams.Phi(5,5),'%5.4g'));

% Update Fitted Parameter Values
paramsFittedTemp = handles.Data.alphaFitted1.alphaToFit1.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.params;
set(handles.fittedAlpha,'String',num2str(paramsFittedTemp.alpha));
set(handles.fittedQ1177,'String',num2str(paramsFittedTemp.Q(1,1)));
set(handles.fittedQ2288,'String',num2str(paramsFittedTemp.Q(2,2)));
set(handles.fittedQ33,'String',num2str(paramsFittedTemp.Q(3,3)));
set(handles.fittedQ5566,'String',num2str(paramsFittedTemp.Q(5,5)));
set(handles.fittedR1122,'String',num2str(paramsFittedTemp.R(1,1),'%5.4g'));
set(handles.fittedPhi1122,'String',num2str(paramsFittedTemp.Phi(1,1),'%5.4g'));
set(handles.fittedPhi3344,'String',num2str(paramsFittedTemp.Phi(3,3),'%5.4g'));
set(handles.fittedPhi5566,'String',num2str(paramsFittedTemp.Phi(5,5),'%5.4g'));

%Pass STD Data - From Young Data, LN2 Phase
handles.maxPerpSTD = 0.0138;
handles.angErrSTD = 2.5419;


xFittedTemp = handles.Data.alphaFitted1.alphaToFit1.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x;
xToFitTemp = handles.Data.toFitTraj.alpha1.x;
axes(handles.XYTraj_axes)
axis equal
grid on
axis([-0.09 0.09 -.12 .12])
title('XY-Trajectory')
set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
xlabel('(m)'),ylabel('(m)')
hold on;
ciplotY(xToFitTemp(:,1)-1.96*handles.maxPerpSTD,xToFitTemp(:,1)+1.96*handles.maxPerpSTD,xToFitTemp(:,2),[200/255 200/255 255/255]);
plot(xToFitTemp(:,1),xToFitTemp(:,2))
plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
plot(0,-0.1,'gx')
plot(0,0.1,'rx')
% Plot Max Perp
[~,maxPerpToFitInd] = max(abs(xToFitTemp(:,1)));
plot(xToFitTemp(maxPerpToFitInd,1),xToFitTemp(maxPerpToFitInd,2),'mo')
[~,maxPerpFittedInd] = max(abs(xFittedTemp(:,1)));
plot(xFittedTemp(maxPerpFittedInd,1),xFittedTemp(maxPerpFittedInd,2),'mo')
hold off;

axes(handles.XVel_axes)
hold on;
plot(Data.time,xToFitTemp(:,3))
plot(Data.time,xFittedTemp(:,3),'r')
hold off;
title('Velocity and Force Profiles')
y1 = ylabel('X-Velocity (m/s)');
posy1 = get(y1, 'position');
set(handles.XVel_axes,'YAxisLocation','right')
set(y1, 'position', posy1);
xlim([Data.time(1) Data.time(end)])
set(handles.XVel_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.YVel_axes)
hold on;
set(gca,'Xgrid','on')
plot(Data.time,xToFitTemp(:,4))
plot(Data.time,xFittedTemp(:,4),'r')
hold off;
y2 = ylabel('Y-Velocity (m/s)');
posy2 = get(y2, 'position');
set(handles.YVel_axes,'YAxisLocation','right')
set(y2, 'position', posy2);
xlim([Data.time(1) Data.time(end)])
set(handles.YVel_axes,'XTickLabel',[])

axes(handles.XForce_axes)
hold on;
set(gca,'Xgrid','on')
plot(Data.time,xToFitTemp(:,5))
plot(Data.time,xFittedTemp(:,5),'r')
hold off;
y3 = ylabel('X-Force (m/s)');
posy3 = get(y3,'position');
set(handles.XForce_axes,'YAxisLocation','right')
set(y3, 'position', posy3);
xlim([Data.time(1) Data.time(end)])
set(handles.XForce_axes,'XTickLabel',[])

axes(handles.YForce_axes)
hold on;
set(gca,'Xgrid','on')
plot(Data.time,xToFitTemp(:,6))
plot(Data.time,xFittedTemp(:,6),'r')
hold off;
y4 = ylabel('Y-Force (m/s)');
posy4 = get(y4, 'position');
set(handles.YForce_axes,'YAxisLocation','right')
set(y4, 'position',posy4);
xlabel('Time (ms)')
xlim([Data.time(1) Data.time(end)])



handles.Data.xFittedTemp = xFittedTemp;
handles.Data.paramsFittedTemp = paramsFittedTemp;
handles.Data.xToFitTemp = xToFitTemp;


% For Slider
handles.methodTypes = {'Max. Perp. Error','Integral Error','Angular Error'};
set(handles.popupmenu2,'String',handles.methodTypes)
set(handles.popupmenu2,'Value',1)
% STATISTICAL TEST (Z-score, p-value)
% Max Perp Error
set(handles.edit1,'String',0.05);
handles.Data.cutoff = 0.05;
set(handles.units,'String','p-value');
% From Data Analysis, Young Adults (for now), LN2 STD
maxPerpSTD = 0.0138;
% Find which Values Are Statistically Different
for a = 1:length(handles.Data.alphaVals) 
    eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
    eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
    for b = 1:length(handles.Data.alphaFineVals)
        eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
            '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
        eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
            '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
%         zScore = (maxPerpFitted - maxPerpToFit)/maxPerpSTD;
%         handles.Data.measure(b,a) = 2*normcdf(-abs(zScore),maxPerpToFit,maxPerpSTD);
%         handles.Data.measure(b,a) = 2*normcdf(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);
        [~,handles.Data.measure(b,a)] = ztest(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);
    end
end


% Plot Xs over Values, For now, only mark good trials with green Xs
axes(handles.RMSgrid_axes)
hold on;
plot(Data.alphaValsGrid,Data.alphaFineValsGrid,'rx','MarkerSize',20);
[ii,jj] = find(handles.Data.measure>handles.Data.cutoff);
plot(Data.alphaVals(jj),Data.alphaFineVals(ii),'gx','MarkerSize',20)
hold off;



% Update handles structure
guidata(hObject, handles);

%-------------END OF INITIALIZATION------------------------%
%----------------------------------------------------------%
%----------------------------------------------------------%
%----------------------------------------------------------%


% --- Outputs from this function are returned to the command line.
function varargout = displayAlphaFitResults_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function vert_slider_Callback(hObject, eventdata, handles)
% hObject    handle to vert_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Add Update Alpha Value, Plot New Highlighter on Grid [4 plot lines], Remove Old  
% Change Parameter Values, Plot New Trajectories

% Change this to change substructure string 'alphaFitted,num2str(get...) 
%alphaFitted = alphaFineVals(get(hObject,'Value'));

%vertVal = round(get(hObject,'Value')*10)/10;
% [~,alphaFittedIndex] = min(abs(handles.Data.alphaVals - get(hObject,'Value')));
% vertVal = handles.Data.alphaVals(alphaFittedIndex);
[~,alphaFittedIndex] = min(abs(handles.Data.alphaFineVals - get(hObject,'Value')));
vertVal = handles.Data.alphaFineVals(alphaFittedIndex);
set(hObject,'Value',vertVal);
set(handles.gridMarker,'XData',get(handles.horiz_slider,'Value'),...
    'YData',vertVal);

%Get p-value
pVal = str2num(get(handles.edit1,'String'));
zVal = -sqrt(2)*erfcinv(2-pVal);

drawnow

% Get Indices for Alphas, Create Temp Variables from Structure
%alphaFittedIndex = vertVal*10 + 1;
%alphaToFitIndex = get(handles.horiz_slider,'Value')*10 + 1;
%alphaToFitIndex = find(handles.Data.alphaFineVals == get(handles.horiz_slider,'Value'));
alphaToFitIndex = find(handles.Data.alphaVals == get(handles.horiz_slider,'Value'));
eval(['xToFitTemp = handles.Data.toFitTraj.alpha',...
    num2str(alphaToFitIndex),'.x;']);
eval(['xFittedTemp = handles.Data.alphaFitted',num2str(alphaFittedIndex),...
    '.alphaToFit',num2str(alphaToFitIndex),...
    '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.',...
    'Phi5566_1.x;'])
eval(['paramsFittedTemp = handles.Data.alphaFitted',num2str(alphaFittedIndex),...
    '.alphaToFit',num2str(alphaToFitIndex),...
    '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.',...
    'Phi5566_1.params;'])
% Update Parameter Values
set(handles.fittedAlpha,'String',num2str(vertVal));
set(handles.fittedQ1177,'String',num2str(paramsFittedTemp.Q(1,1)));
set(handles.fittedQ2288,'String',num2str(paramsFittedTemp.Q(2,2)));
set(handles.fittedQ33,'String',num2str(paramsFittedTemp.Q(3,3)));
set(handles.fittedQ5566,'String',num2str(paramsFittedTemp.Q(5,5)));
set(handles.fittedR1122,'String',num2str(paramsFittedTemp.R(1,1)));
set(handles.fittedPhi1122,'String',num2str(paramsFittedTemp.Phi(1,1),'%5.4g'));
set(handles.fittedPhi3344,'String',num2str(paramsFittedTemp.Phi(3,3),'%5.4g'));
set(handles.fittedPhi5566,'String',num2str(paramsFittedTemp.Phi(5,5),'%5.4g'));

% Plot New Trajectories
axes(handles.XYTraj_axes)
axis equal

if get(handles.popupmenu2,'Value') == 1
    ciplotY(xToFitTemp(:,1)-zVal*handles.maxPerpSTD ,xToFitTemp(:,1)+zVal*handles.maxPerpSTD,xToFitTemp(:,2),[200/255 200/255 255/255]);
    [~,maxPerpToFitInd] = max(abs(xToFitTemp(:,1)));
    hold on;
    plot(xToFitTemp(maxPerpToFitInd,1),xToFitTemp(maxPerpToFitInd,2),'mo')
    [~,maxPerpFittedInd] = max(abs(xFittedTemp(:,1)));
    plot(xFittedTemp(maxPerpFittedInd,1),xFittedTemp(maxPerpFittedInd,2),'mo')
elseif get(handles.popupmenu2,'Value') == 3
    [~,maxVelToFitInd] = max(xToFitTemp(:,3).^2 + xToFitTemp(:,4).^2);
    vectorToFit = [xToFitTemp(maxVelToFitInd,1) - 0; xToFitTemp(maxVelToFitInd,2) + 0.1];
    angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));    
    ciplotY(sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit+zVal*handles.angErrSTD),...
        sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit-zVal*handles.angErrSTD),...
        xToFitTemp(:,2),[200/255 200/255 255/255]);
    hold on;
    plot(xToFitTemp(maxVelToFitInd,1),xToFitTemp(maxVelToFitInd,2),'mo')
    [~,maxVelFittedInd] = max(xFittedTemp(:,3).^2 + xFittedTemp(:,4).^2);
    plot(xFittedTemp(maxVelFittedInd,1),xFittedTemp(maxVelFittedInd,2),'mo')    
end
plot(xToFitTemp(:,1),xToFitTemp(:,2))
plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
plot(0,-0.1,'gx')
plot(0,0.1,'rx')
hold off;
grid on
axis([-0.09 0.09 -.12 .12])
title('XY-Trajectory')
set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
xlabel('(m)'),ylabel('(m)')

axes(handles.XVel_axes)
plot(handles.Data.time,xToFitTemp(:,3))
hold on;
plot(handles.Data.time,xFittedTemp(:,3),'r')
hold off;
title('Velocity and Force Profiles')
y1 = ylabel('X-Velocity (m/s)');
posy1 = get(y1, 'position');
set(handles.XVel_axes,'YAxisLocation','right')
set(y1, 'position', posy1);
xlim([handles.Data.time(1) handles.Data.time(end)])
set(handles.XVel_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.YVel_axes)
plot(handles.Data.time,xToFitTemp(:,4))
hold on;
plot(handles.Data.time,xFittedTemp(:,4),'r')
hold off;
set(gca,'Xgrid','on')
y2 = ylabel('Y-Velocity (m/s)');
posy2 = get(y2, 'position');
set(handles.YVel_axes,'YAxisLocation','right')
set(y2, 'position', posy2);
xlim([handles.Data.time(1) handles.Data.time(end)])
set(handles.YVel_axes,'XTickLabel',[])

axes(handles.XForce_axes)
plot(handles.Data.time,xToFitTemp(:,5))
hold on;
plot(handles.Data.time,xFittedTemp(:,5),'r')
hold off;
set(gca,'Xgrid','on')
y3 = ylabel('X-Force (m/s)');
posy3 = get(y3,'position');
set(handles.XForce_axes,'YAxisLocation','right')
set(y3, 'position', posy3);
xlim([handles.Data.time(1) handles.Data.time(end)])
set(handles.XForce_axes,'XTickLabel',[])

axes(handles.YForce_axes)
plot(handles.Data.time,xToFitTemp(:,6))
hold on;
plot(handles.Data.time,xFittedTemp(:,6),'r')
hold off;
set(gca,'Xgrid','on')
y4 = ylabel('Y-Force (m/s)');
posy4 = get(y4, 'position');
set(handles.YForce_axes,'YAxisLocation','right')
set(y4, 'position',posy4);
xlabel('Time (ms)')
xlim([handles.Data.time(1) handles.Data.time(end)])

%handles.Data.xFittedTemp = xFittedTemp;
%handles.Data.paramsFittedTemp = paramsFittedTemp;

% --- Executes on slider movement.
function horiz_slider_Callback(hObject, eventdata, handles)
% hObject    handle to horiz_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% TO DO:
% Add Update Alpha Value, Change Position of Highlighter on Grid, 
% Change Parameter Values, Plot New Trajectories

%horizVal = round(get(hObject,'Value')*10)/10;
% [~,alphaToFitIndex] = min(abs(handles.Data.alphaFineVals - get(hObject,'Value')));
% horizVal = handles.Data.alphaFineVals(alphaToFitIndex);
[~,alphaToFitIndex] = min(abs(handles.Data.alphaVals - get(hObject,'Value')));
horizVal = handles.Data.alphaVals(alphaToFitIndex);
set(hObject,'Value',horizVal);
set(handles.gridMarker,'XData',horizVal,...
    'YData',get(handles.vert_slider,'Value'));

%Get p-value
pVal = str2num(get(handles.edit1,'String'));
zVal = -sqrt(2)*erfcinv(2-pVal);

drawnow


% Get Indices for Alphas, Create Temp Variables from Structure
%alphaToFitIndex = horizVal*10 + 1;
vertVal = get(handles.vert_slider,'Value');
%alphaFittedIndex = vertVal*10 + 1;
%alphaFittedIndex = find(handles.Data.alphaFineVals == vertVal);
alphaFittedIndex = find(handles.Data.alphaFineVals == vertVal);
eval(['xToFitTemp = handles.Data.toFitTraj.alpha',...
    num2str(alphaToFitIndex),'.x;']);
eval(['xFittedTemp = handles.Data.alphaFitted',num2str(alphaFittedIndex),...
    '.alphaToFit',num2str(alphaToFitIndex),...
    '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.',...
    'Phi5566_1.x;'])
eval(['paramsFittedTemp = handles.Data.alphaFitted',num2str(alphaFittedIndex),...
    '.alphaToFit',num2str(alphaToFitIndex),...
    '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.',...
    'Phi5566_1.params;'])
% Update Parameter Values
set(handles.fittedAlpha,'String',num2str(vertVal));
set(handles.fittedQ1177,'String',num2str(paramsFittedTemp.Q(1,1)));
set(handles.fittedQ2288,'String',num2str(paramsFittedTemp.Q(2,2)));
set(handles.fittedQ33,'String',num2str(paramsFittedTemp.Q(3,3)));
set(handles.fittedQ5566,'String',num2str(paramsFittedTemp.Q(5,5)));
set(handles.fittedR1122,'String',num2str(paramsFittedTemp.R(1,1)));
set(handles.fittedPhi1122,'String',num2str(paramsFittedTemp.Phi(1,1),'%5.4g'));
set(handles.fittedPhi3344,'String',num2str(paramsFittedTemp.Phi(3,3),'%5.4g'));
set(handles.fittedPhi5566,'String',num2str(paramsFittedTemp.Phi(5,5),'%5.4g'));

% Update Parameter Values
set(handles.initAlpha,'String',num2str(horizVal));

% Plot New Trajectories
axes(handles.XYTraj_axes)
axis equal

if get(handles.popupmenu2,'Value') == 1
    ciplotY(xToFitTemp(:,1)-zVal*handles.maxPerpSTD ,xToFitTemp(:,1)+zVal*handles.maxPerpSTD,xToFitTemp(:,2),[200/255 200/255 255/255]);
    [~,maxPerpToFitInd] = max(abs(xToFitTemp(:,1)));
    hold on;
    plot(xToFitTemp(maxPerpToFitInd,1),xToFitTemp(maxPerpToFitInd,2),'mo')
    [~,maxPerpFittedInd] = max(abs(xFittedTemp(:,1)));
    plot(xFittedTemp(maxPerpFittedInd,1),xFittedTemp(maxPerpFittedInd,2),'mo')
elseif get(handles.popupmenu2,'Value') == 3
    [~,maxVelToFitInd] = max(xToFitTemp(:,3).^2 + xToFitTemp(:,4).^2);
    vectorToFit = [xToFitTemp(maxVelToFitInd,1) - 0; xToFitTemp(maxVelToFitInd,2) + 0.1];
    angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));    
    ciplotY(sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit+zVal*handles.angErrSTD),...
        sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit-zVal*handles.angErrSTD),...
        xToFitTemp(:,2),[200/255 200/255 255/255]);
    hold on;
    plot(xToFitTemp(maxVelToFitInd,1),xToFitTemp(maxVelToFitInd,2),'mo')
    [~,maxVelFittedInd] = max(xFittedTemp(:,3).^2 + xFittedTemp(:,4).^2);
    plot(xFittedTemp(maxVelFittedInd,1),xFittedTemp(maxVelFittedInd,2),'mo')    
end
plot(xToFitTemp(:,1),xToFitTemp(:,2))
plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
plot(0,-0.1,'gx')
plot(0,0.1,'rx')
hold off;
axis([-0.09 0.09 -.12 .12])
title('XY-Trajectory')
set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
xlabel('(m)'),ylabel('(m)')
grid on

axes(handles.XVel_axes)
plot(handles.Data.time,xToFitTemp(:,3))
hold on;
plot(handles.Data.time,xFittedTemp(:,3),'r')
hold off;
title('Velocity and Force Profiles')
y1 = ylabel('X-Velocity (m/s)');
posy1 = get(y1, 'position');
set(handles.XVel_axes,'YAxisLocation','right')
set(y1, 'position', posy1);
xlim([handles.Data.time(1) handles.Data.time(end)])
set(handles.XVel_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.YVel_axes)
plot(handles.Data.time,xToFitTemp(:,4))
hold on;
plot(handles.Data.time,xFittedTemp(:,4),'r')
hold off;
y2 = ylabel('Y-Velocity (m/s)');
posy2 = get(y2, 'position');
set(handles.YVel_axes,'YAxisLocation','right')
set(y2, 'position', posy2);
xlim([handles.Data.time(1) handles.Data.time(end)])
set(handles.YVel_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.XForce_axes)
plot(handles.Data.time,xToFitTemp(:,5))
hold on;
plot(handles.Data.time,xFittedTemp(:,5),'r')
hold off;
y3 = ylabel('X-Force (m/s)');
posy3 = get(y3,'position');
set(handles.XForce_axes,'YAxisLocation','right')
set(y3, 'position', posy3);
xlim([handles.Data.time(1) handles.Data.time(end)])
set(handles.XForce_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.YForce_axes)
plot(handles.Data.time,xToFitTemp(:,6))
hold on;
plot(handles.Data.time,xFittedTemp(:,6),'r')
hold off;
y4 = ylabel('Y-Force (m/s)');
posy4 = get(y4, 'position');
set(handles.YForce_axes,'YAxisLocation','right')
set(y4, 'position',posy4);
xlabel('Time (ms)')
xlim([handles.Data.time(1) handles.Data.time(end)])
set(gca,'Xgrid','on')

%handles.Data.xToFitTemp = xToFitTemp;
%handles.Data.xFittedTemp = xFittedTemp;
%handles.Data.paramsFittedTemp = paramsFittedTemp;

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1
ind = get(handles.popupmenu1,'Value');
file = handles.fileNames{ind};
load(['DataFiles/',num2str(file)]);
handles.Data = Data;

%Set back to RMS
set(handles.popupmenu3,'Value',1)


% Re-draw All Plots
axes(handles.RMSgrid_axes)
colormap('hot');
% pcolor(Data.alphaFineValsGrid,Data.alphaValsGrid,Data.gridAlpha)
pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridAlpha);
handles.cb = colorbar;
shading interp
hold on
% plot(Data.alphaFineValsGrid,Data.alphaValsGrid,'co','MarkerSize',4,...
%     'MarkerFaceColor','none')
plot(Data.alphaValsGrid,Data.alphaFineValsGrid,'co','MarkerSize',4,...
    'MarkerFaceColor','none')
if isfield(Data,'gridFlags')
    [ii,jj] = find(Data.gridFlags~=2);
%    plot(Data.alphaFineValsGrid(ii),Data.alphaValsGrid(jj),'mo','MarkerSize',4)
    plot(Data.alphaVals(jj),Data.alphaFineVals(ii),'mo','MarkerSize',4)
end
hold off
% set(gca,'XTick', 0:0.1:1)
% set(gca,'YTick', 0:0.1:1)
% set(gca,'XTick', Data.alphaFineVals)
% set(gca,'YTick', Data.alphaVals)
set(gca,'XTick', Data.alphaVals)
set(gca,'YTick', Data.alphaFineVals)
% title('RMS Error')
xlabel('Alpha Values for Trajectory to Be Fit')
ylabel('Alpha Values for Resulting Fitted Trajectory')

hold on;
handles.gridMarker = plot(handles.RMSgrid_axes,Data.alphaVals(1),Data.alphaFineVals(1),'o','markerfacecolor','b','markersize',10);
hold off;
%set(handles.gridMarker,'XData',Data.alphaVals(1),'YData',Data.alphaFineVals(1))


%Re-scale sliders
% set(handles.horiz_slider,'Min',Data.alphaFineVals(1))
% set(handles.horiz_slider,'Max',Data.alphaFineVals(end))
% % Assumes constant step size...
% set(handles.horiz_slider,'SliderStep',...
%     [Data.alphaFineVals(2) - Data.alphaFineVals(1), ...
%     Data.alphaFineVals(2) - Data.alphaFineVals(1)])
% set(handles.horiz_slider,'Value',Data.alphaFineVals(1))
% 
% set(handles.vert_slider,'Min',Data.alphaVals(1))
% set(handles.vert_slider,'Max',Data.alphaVals(end))
% set(handles.vert_slider,'SliderStep',...
%     [Data.alphaVals(2) - Data.alphaVals(1), ...
%     Data.alphaVals(2) - Data.alphaVals(1)])
% set(handles.vert_slider,'Value',Data.alphaVals(1))
set(handles.horiz_slider,'Min',Data.alphaVals(1))
set(handles.horiz_slider,'Max',Data.alphaVals(end))
% Assumes constant step size...
% set(handles.horiz_slider,'SliderStep',...
%     [Data.alphaVals(2) - Data.alphaVals(1), ...
%     Data.alphaVals(2) - Data.alphaVals(1)])
set(handles.horiz_slider,'SliderStep',...
    [1/(length(Data.alphaVals)-1), 1/(length(Data.alphaVals)-1)])
set(handles.horiz_slider,'Value',Data.alphaVals(1))

set(handles.vert_slider,'Min',Data.alphaFineVals(1))
set(handles.vert_slider,'Max',Data.alphaFineVals(end))
% set(handles.vert_slider,'SliderStep',...
%     [Data.alphaFineVals(2) - Data.alphaFineVals(1), ...
%     Data.alphaFineVals(2) - Data.alphaFineVals(1)])
set(handles.vert_slider,'SliderStep',...
    [1/(length(Data.alphaFineVals)-1), 1/(length(Data.alphaFineVals)-1)])
set(handles.vert_slider,'Value',Data.alphaFineVals(1))
drawnow

% Update To Fit Parameter Values
set(handles.initAlpha,'String',num2str(Data.alphaVals(1)));
set(handles.initQ1177,'String',num2str(Data.toFitParams.Q(1,1)));
set(handles.initQ2288,'String',num2str(Data.toFitParams.Q(2,2)));
set(handles.initQ33,'String',num2str(Data.toFitParams.Q(3,3)));
set(handles.initQ5566,'String',num2str(Data.toFitParams.Q(5,5)));
set(handles.initR1122,'String',num2str(Data.toFitParams.R(1,1)));
set(handles.initPhi1122,'String',num2str(Data.toFitParams.Phi(1,1),'%5.4g'));
set(handles.initPhi3344,'String',num2str(Data.toFitParams.Phi(3,3),'%5.4g'));
set(handles.initPhi5566,'String',num2str(Data.toFitParams.Phi(5,5),'%5.4g'));

% Update Fitted Parameter Values
paramsFittedTemp = handles.Data.alphaFitted1.alphaToFit1.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.params;
set(handles.fittedAlpha,'String',num2str(paramsFittedTemp.alpha));
set(handles.fittedQ1177,'String',num2str(paramsFittedTemp.Q(1,1)));
set(handles.fittedQ2288,'String',num2str(paramsFittedTemp.Q(2,2)));
set(handles.fittedQ33,'String',num2str(paramsFittedTemp.Q(3,3)));
set(handles.fittedQ5566,'String',num2str(paramsFittedTemp.Q(5,5)));
set(handles.fittedR1122,'String',num2str(paramsFittedTemp.R(1,1)));
set(handles.fittedPhi1122,'String',num2str(paramsFittedTemp.Phi(1,1),'%5.4g'));
set(handles.fittedPhi3344,'String',num2str(paramsFittedTemp.Phi(3,3),'%5.4g'));
set(handles.fittedPhi5566,'String',num2str(paramsFittedTemp.Phi(5,5),'%5.4g'));


xFittedTemp = handles.Data.alphaFitted1.alphaToFit1.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x;
xToFitTemp = handles.Data.toFitTraj.alpha1.x;
axes(handles.XYTraj_axes)
axis equal
ciplotY(xToFitTemp(:,1)-1.96*handles.maxPerpSTD ,xToFitTemp(:,1)+1.96*handles.maxPerpSTD,xToFitTemp(:,2),[200/255 200/255 255/255]);
hold on;
plot(xToFitTemp(:,1),xToFitTemp(:,2))
plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
plot(0,-0.1,'gx')
plot(0,0.1,'rx')
[~,maxPerpToFitInd] = max(abs(xToFitTemp(:,1)));
plot(xToFitTemp(maxPerpToFitInd,1),xToFitTemp(maxPerpToFitInd,2),'mo')
[~,maxPerpFittedInd] = max(abs(xFittedTemp(:,1)));
plot(xFittedTemp(maxPerpFittedInd,1),xFittedTemp(maxPerpFittedInd,2),'mo')
hold off;
axis([-0.09 0.09 -.12 .12])
title('XY-Trajectory')
set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
xlabel('(m)'),ylabel('(m)')
grid on

axes(handles.XVel_axes)
plot(Data.time,xToFitTemp(:,3))
hold on;
plot(Data.time,xFittedTemp(:,3),'r')
hold off;
title('Velocity and Force Profiles')
y1 = ylabel('X-Velocity (m/s)');
posy1 = get(y1, 'position');
set(handles.XVel_axes,'YAxisLocation','right')
set(y1, 'position', posy1);
xlim([Data.time(1) Data.time(end)])
set(handles.XVel_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.YVel_axes)
plot(Data.time,xToFitTemp(:,4))
hold on;
plot(Data.time,xFittedTemp(:,4),'r')
hold off;
y2 = ylabel('Y-Velocity (m/s)');
posy2 = get(y2, 'position');
set(handles.YVel_axes,'YAxisLocation','right')
set(y2, 'position', posy2);
xlim([Data.time(1) Data.time(end)])
set(handles.YVel_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.XForce_axes)
plot(Data.time,xToFitTemp(:,5))
hold on;
plot(Data.time,xFittedTemp(:,5),'r')
hold off;
y3 = ylabel('X-Force (m/s)');
posy3 = get(y3,'position');
set(handles.XForce_axes,'YAxisLocation','right')
set(y3, 'position', posy3);
xlim([Data.time(1) Data.time(end)])
set(handles.XForce_axes,'XTickLabel',[])
set(gca,'Xgrid','on')

axes(handles.YForce_axes)
plot(Data.time,xToFitTemp(:,6))
hold on;
plot(Data.time,xFittedTemp(:,6),'r')
hold off;
y4 = ylabel('Y-Force (m/s)');
posy4 = get(y4, 'position');
set(handles.YForce_axes,'YAxisLocation','right')
set(y4, 'position',posy4);
xlabel('Time (ms)')
xlim([Data.time(1) Data.time(end)])
set(gca,'Xgrid','on')

handles.Data.xFittedTemp = xFittedTemp;
handles.Data.paramsFittedTemp = paramsFittedTemp;
handles.Data.xToFitTemp = xToFitTemp;

% For Slider
handles.methodTypes = {'Max. Perp. Error','Integral Error','Angular Error'};
set(handles.popupmenu2,'String',handles.methodTypes)
set(handles.popupmenu2,'Value',1)
% Max Perp Error
set(handles.edit1,'String',0.05);
handles.Data.cutoff = 0.05;
set(handles.units,'String','p-value');
% From Data Analysis, Young Adults (for now), LN2 STD
%maxPerpSTD = 0.0138;
% Find which Values Are Statistically Different
for a = 1:length(handles.Data.alphaVals) 
    eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
    eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
    for b = 1:length(handles.Data.alphaFineVals)
        eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
            '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
        eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
            '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
%         zScore = (maxPerpFitted - maxPerpToFit)/handles.maxPerpSTD;
%         handles.Data.measure(b,a) = 2*normcdf(-abs(zScore),maxPerpToFit,handles.maxPerpSTD);
%         handles.Data.measure(b,a) = 2*normcdf(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);        
        [~,handles.Data.measure(b,a)] = ztest(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);
    end
end

% Plot Xs over Values, For now, only mark good trials with green Xs
axes(handles.RMSgrid_axes)
hold on;
plot(Data.alphaValsGrid,Data.alphaFineValsGrid,'rx','MarkerSize',20);
[ii,jj] = find(handles.Data.measure>handles.Data.cutoff);
plot(Data.alphaVals(jj),Data.alphaFineVals(ii),'gx','MarkerSize',20)
hold off;




% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2

ind = get(handles.popupmenu2,'Value');
horiz = find(handles.Data.alphaVals == get(handles.horiz_slider,'Value'));
vert = find(handles.Data.alphaFineVals == get(handles.vert_slider,'Value'));

% THRESHOLD VALUES (OLD)
% if ind == 1
%     % Max Perp Error
%     set(handles.edit1,'String',0.01);
%     handles.Data.cutoff = 0.01;
%     set(handles.units,'String','meters');
%     % Find which Values Are Above Threshold
%     for a = 1:length(handles.Data.alphaVals) 
%         eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
%         eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
%         for b = 1:length(handles.Data.alphaFineVals)
%             eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
%             eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
%             handles.Data.measure(b,a) = abs(maxPerpToFit - maxPerpFitted);
%         end
%     end
% elseif ind == 2
%     % Integral Error
%     set(handles.edit1,'String',0.002);
%     handles.Data.cutoff = 0.01;
%     set(handles.units,'String','meters');
%     for a = 1:length(handles.Data.alphaVals) 
%         for b = 1:length(handles.Data.alphaFineVals)
%             eval(['handles.Data.measure(b,a) = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.RMSError;'])
%         end
%     end
%     
% elseif ind == 3
%     % Angular Error
%     set(handles.edit1,'String',5');
%     handles.Data.cutoff = 5;
%     set(handles.units,'String','degrees')
%     for a = 1:length(handles.Data.alphaVals) 
%         eval(['[~,maxVelToFitInd] = max(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,3).^2 + handles.Data.toFitTraj.alpha',num2str(a),'.x(:,4).^2);'])
%         eval(['vectorToFit = [handles.Data.toFitTraj.alpha',num2str(a),...
%             '.x(maxVelToFitInd,1) - 0; handles.Data.toFitTraj.alpha',num2str(a),...
%             '.x(maxVelToFitInd,2) + 0.1];'])
%         for b = 1:length(handles.Data.alphaFineVals)
%             eval(['[~,maxVelFittedInd] = max(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,3));'])
%             eval(['vectorFitted = [handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,1) - 0;',...
%                 'handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,2) + 0.1];'])            
%             handles.Data.measure(b,a) = acosd(dot(vectorToFit,vectorFitted)/(norm(vectorToFit)*norm(vectorFitted)));
%         end
%     end    
% end

% STATISTICAL TEST (Z-score, p-value)
if ind == 1
    % Max Perp Error
    set(handles.edit1,'String',0.05);
    handles.Data.cutoff = 0.05;
    set(handles.units,'String','p-value');
    % From Data Analysis, Young Adults (for now), LN2 STD
    % maxPerpSTD = 0.0138;
    % Find which Values Are Statistically Different
    for a = 1:length(handles.Data.alphaVals) 
        eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
        eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
            eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
%             zScore = (maxPerpFitted - maxPerpToFit)/handles.maxPerpSTD;
%             handles.Data.measure(b,a) = 2*normcdf(-abs(zScore),maxPerpToFit,handles.maxPerpSTD);
%             handles.Data.measure(b,a) = 2*normcdf(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);        
            [~,handles.Data.measure(b,a)] = ztest(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);
        end
    end
    eval(['xFittedTemp = handles.Data.alphaFitted',num2str(vert),'.alphaToFit',num2str(horiz),...
        '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x;']);
    eval(['xToFitTemp = handles.Data.toFitTraj.alpha',num2str(horiz),'.x;']);
    axes(handles.XYTraj_axes)
    axis equal
    ciplotY(xToFitTemp(:,1)-1.96*handles.maxPerpSTD ,xToFitTemp(:,1)+1.96*handles.maxPerpSTD,xToFitTemp(:,2),[200/255 200/255 255/255]);
    hold on;
    plot(xToFitTemp(:,1),xToFitTemp(:,2))
    plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
    plot(0,-0.1,'gx')
    plot(0,0.1,'rx')
    [~,maxPerpToFitInd] = max(abs(xToFitTemp(:,1)));
    plot(xToFitTemp(maxPerpToFitInd,1),xToFitTemp(maxPerpToFitInd,2),'mo')
    [~,maxPerpFittedInd] = max(abs(xFittedTemp(:,1)));
    plot(xFittedTemp(maxPerpFittedInd,1),xFittedTemp(maxPerpFittedInd,2),'mo')
    hold off;
    axis([-0.09 0.09 -.12 .12])
    title('XY-Trajectory')
    set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
    xlabel('(m)'),ylabel('(m)')
    grid on
elseif ind == 2
    % Integral Error
    set(handles.edit1,'String',0.002);
    handles.Data.cutoff = 0.01;
    set(handles.units,'String','meters');
    for a = 1:length(handles.Data.alphaVals) 
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['handles.Data.measure(b,a) = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.RMSError;'])
        end
    end
    eval(['xFittedTemp = handles.Data.alphaFitted',num2str(vert),'.alphaToFit',num2str(horiz),...
        '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x;']);
    eval(['xToFitTemp = handles.Data.toFitTraj.alpha',num2str(horiz),'.x;']);
    axes(handles.XYTraj_axes)
    axis equal
    plot(xToFitTemp(:,1),xToFitTemp(:,2))
    hold on;
    plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
    plot(0,-0.1,'gx')
    plot(0,0.1,'rx')
    hold off;
    axis([-0.09 0.09 -.12 .12])
    title('XY-Trajectory')
    set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
    xlabel('(m)'),ylabel('(m)')
    grid on    
elseif ind == 3
    % Angular Error
    set(handles.edit1,'String',0.05');
    handles.Data.cutoff = 0.05;
    set(handles.units,'String','p-value')
    % From Data Analysis, Young Adults (for now), LN2 STD
    %angErrSTD = 2.5419;
    for a = 1:length(handles.Data.alphaVals) 
        eval(['[~,maxVelToFitInd] = max(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,3).^2 + handles.Data.toFitTraj.alpha',num2str(a),'.x(:,4).^2);'])
        eval(['vectorToFit = [handles.Data.toFitTraj.alpha',num2str(a),...
            '.x(maxVelToFitInd,1) - 0; handles.Data.toFitTraj.alpha',num2str(a),...
            '.x(maxVelToFitInd,2) + 0.1];'])
        angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['[~,maxVelFittedInd] = max(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,3).^2 + handles.Data.alphaFitted',num2str(b),'.alphaToFit',...
                num2str(a),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,4).^2);'])
            eval(['vectorFitted = [handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,1) - 0;',...
                'handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,2) + 0.1];'])
            angErrFitted = acosd(dot(vectorFitted,[0;1])/(norm(vectorFitted)*norm([0;1])));
%             zScore = (angErrFitted - angErrToFit)/handles.angErrSTD;
%             handles.Data.measure(b,a) = 2*normcdf(-abs(zScore),angErrToFit,handles.angErrSTD);
            %zScore = (angErrFitted - angErrToFit)/handles.angErrSTD;
%            handles.Data.measure(b,a) = 2*normcdf(angErrFitted,angErrToFit,handles.angErrSTD);
            [~,handles.Data.measure(b,a)] = ztest(angErrFitted,angErrToFit,handles.angErrSTD);
        end
    end
    eval(['xFittedTemp = handles.Data.alphaFitted',num2str(vert),'.alphaToFit',num2str(horiz),...
        '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x;']);
    eval(['xToFitTemp = handles.Data.toFitTraj.alpha',num2str(horiz),'.x;']);
    [~,maxVelToFitInd] = max(xToFitTemp(:,3).^2 + xToFitTemp(:,4).^2);
    vectorToFit = [xToFitTemp(maxVelToFitInd,1) - 0; xToFitTemp(maxVelToFitInd,2) + 0.1];
    angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));    
    axes(handles.XYTraj_axes)
    axis equal
    ciplotY(sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit+1.96*handles.angErrSTD),...
        sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit-1.96*handles.angErrSTD),...
        xToFitTemp(:,2),[200/255 200/255 255/255]);
    hold on;
    plot(xToFitTemp(:,1),xToFitTemp(:,2))
    plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
    plot(0,-0.1,'gx')
    plot(0,0.1,'rx')
    [~,maxVelToFitInd] = max(xToFitTemp(:,3).^2 + xToFitTemp(:,4).^2);
    plot(xToFitTemp(maxVelToFitInd,1),xToFitTemp(maxVelToFitInd,2),'mo')
    [~,maxVelFittedInd] = max(xFittedTemp(:,3).^2 + xFittedTemp(:,4).^2);
    plot(xFittedTemp(maxVelFittedInd,1),xFittedTemp(maxVelFittedInd,2),'mo')    
    hold off;
    axis([-0.09 0.09 -.12 .12])
    title('XY-Trajectory')
    set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
    xlabel('(m)'),ylabel('(m)')
    grid on
end

% Plot Xs over Values, For now, only mark good trials with green Xs
axes(handles.RMSgrid_axes)
hold on;
plot(handles.Data.alphaValsGrid,handles.Data.alphaFineValsGrid,'rx','MarkerSize',20);
[ii,jj] = find(handles.Data.measure>handles.Data.cutoff);
plot(handles.Data.alphaVals(jj),handles.Data.alphaFineVals(ii),'gx','MarkerSize',20)
hold off;



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double

ind = get(handles.popupmenu2,'Value');
handles.Data.cutoff = str2num(get(handles.edit1,'String'));
horiz = find(handles.Data.alphaVals == get(handles.horiz_slider,'Value'));
vert = find(handles.Data.alphaFineVals == get(handles.vert_slider,'Value'));

%Get p-value
pVal = str2num(get(handles.edit1,'String'));
zVal = -sqrt(2)*erfcinv(2-pVal);

% if ind == 1
%     % Find which Values Are Above Threshold
%     for a = 1:length(handles.Data.alphaVals) 
%         eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
%         eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
%         for b = 1:length(handles.Data.alphaFineVals)
%             eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
%             eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
%             handles.Data.measure(b,a) = abs(maxPerpToFit - maxPerpFitted);
%         end
%     end
% elseif ind == 2
%     for a = 1:length(handles.Data.alphaVals) 
%         for b = 1:length(handles.Data.alphaFineVals)
%             eval(['handles.Data.measure(b,a) = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.RMSError;'])
%         end
%     end
% elseif ind == 3
%     for a = 1:length(handles.Data.alphaVals) 
%         eval(['[~,maxVelToFitInd] = max(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,3).^2 + handles.Data.toFitTraj.alpha',num2str(a),'.x(:,4).^2);'])
%         eval(['vectorToFit = [handles.Data.toFitTraj.alpha',num2str(a),...
%             '.x(maxVelToFitInd,1) - 0; handles.Data.toFitTraj.alpha',num2str(a),...
%             '.x(maxVelToFitInd,2) + 0.1];'])
%         for b = 1:length(handles.Data.alphaFineVals)
%             eval(['[~,maxVelFittedInd] = max(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,3));'])
%             eval(['vectorFitted = [handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,1) - 0;',...
%                 'handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
%                 '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,2) + 0.1];'])            
%             handles.Data.measure(b,a) = acosd(dot(vectorToFit,vectorFitted)/(norm(vectorToFit)*norm(vectorFitted)));
%         end
%     end
% 
% end

eval(['xFittedTemp = handles.Data.alphaFitted',num2str(vert),'.alphaToFit',num2str(horiz),...
    '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x;']);
eval(['xToFitTemp = handles.Data.toFitTraj.alpha',num2str(horiz),'.x;']);


% STATISTICAL TEST (Z-score, p-value)
if ind == 1
    % Max Perp Error
    % From Data Analysis, Young Adults (for now), LN2 STD
    %maxPerpSTD = 0.0138;
    % Find which Values Are Statistically Different
    for a = 1:length(handles.Data.alphaVals) 
        eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
        eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
            eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
%             zScore = (maxPerpFitted - maxPerpToFit)/handles.maxPerpSTD;
%             handles.Data.measure(b,a) = 2*normcdf(-abs(zScore),maxPerpToFit,handles.maxPerpSTD);
%            handles.Data.measure(b,a) = 2*normcdf(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);        
            [~,handles.Data.measure(b,a)] = ztest(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);
        end
    end
elseif ind == 2
    % Integral Error
    set(handles.units,'String','meters');
    for a = 1:length(handles.Data.alphaVals) 
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['handles.Data.measure(b,a) = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.RMSError;'])
        end
    end
    
elseif ind == 3
    % Angular Error
    % From Data Analysis, Young Adults (for now), LN2 STD
    %angErrSTD = 2.5419;
    for a = 1:length(handles.Data.alphaVals) 
        eval(['[~,maxVelToFitInd] = max(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,3).^2 + handles.Data.toFitTraj.alpha',num2str(a),'.x(:,4).^2);'])
        eval(['vectorToFit = [handles.Data.toFitTraj.alpha',num2str(a),...
            '.x(maxVelToFitInd,1) - 0; handles.Data.toFitTraj.alpha',num2str(a),...
            '.x(maxVelToFitInd,2) + 0.1];'])
        angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['[~,maxVelFittedInd] = max(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,3).^2 + handles.Data.alphaFitted',num2str(b),'.alphaToFit',...
                num2str(a),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,4).^2);'])
            eval(['vectorFitted = [handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,1) - 0;',...
                'handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,2) + 0.1];'])
            angErrFitted = acosd(dot(vectorFitted,[0;1])/(norm(vectorFitted)*norm([0;1])));
%             zScore = (angErrFitted - angErrToFit)/handles.angErrSTD;
%             handles.Data.measure(b,a) = 2*normcdf(-abs(zScore),angErrToFit,handles.angErrSTD);
%            handles.Data.measure(b,a) = 2*normcdf(angErrFitted,angErrToFit,handles.angErrSTD);
        [~,handles.Data.measure(b,a)] = ztest(angErrFitted,angErrToFit,handles.angErrSTD);
        end
    end    
end

% Plot Xs over Values, For now, only mark good trials with green Xs
axes(handles.RMSgrid_axes)
hold on;
plot(handles.Data.alphaValsGrid,handles.Data.alphaFineValsGrid,'rx','MarkerSize',20);
[ii,jj] = find(handles.Data.measure>handles.Data.cutoff);
plot(handles.Data.alphaVals(jj),handles.Data.alphaFineVals(ii),'gx','MarkerSize',20)
hold off;


% Plot New Trajectories
axes(handles.XYTraj_axes)
axis equal

if ind == 1
    ciplotY(xToFitTemp(:,1)-zVal*handles.maxPerpSTD ,xToFitTemp(:,1)+zVal*handles.maxPerpSTD,xToFitTemp(:,2),[200/255 200/255 255/255]);
    [~,maxPerpToFitInd] = max(abs(xToFitTemp(:,1)));
    plot(xToFitTemp(maxPerpToFitInd,1),xToFitTemp(maxPerpToFitInd,2),'mo')
    [~,maxPerpFittedInd] = max(abs(xFittedTemp(:,1)));
    plot(xFittedTemp(maxPerpFittedInd,1),xFittedTemp(maxPerpFittedInd,2),'mo')
elseif ind == 3
    [~,maxVelToFitInd] = max(xToFitTemp(:,3).^2 + xToFitTemp(:,4).^2);
    vectorToFit = [xToFitTemp(maxVelToFitInd,1) - 0; xToFitTemp(maxVelToFitInd,2) + 0.1];
    angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));    
    ciplotY(sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit+zVal*handles.angErrSTD),...
        sign(vectorToFit(1))*(xToFitTemp(:,2)+0.1)*tand(angErrToFit-zVal*handles.angErrSTD),...
        xToFitTemp(:,2),[200/255 200/255 255/255]);
    plot(xToFitTemp(maxVelToFitInd,1),xToFitTemp(maxVelToFitInd,2),'mo')
    [~,maxVelFittedInd] = max(xFittedTemp(:,3).^2 + xFittedTemp(:,4).^2);
    plot(xFittedTemp(maxVelFittedInd,1),xFittedTemp(maxVelFittedInd,2),'mo')    
end
hold on;
plot(xToFitTemp(:,1),xToFitTemp(:,2))
plot(xFittedTemp(:,1),xFittedTemp(:,2),'r')
plot(0,-0.1,'gx')
plot(0,0.1,'rx')
hold off;
axis([-0.09 0.09 -.12 .12])
title('XY-Trajectory')
set(handles.XYTraj_axes,'YTick',[-0.1 0 0.1])
xlabel('(m)'),ylabel('(m)')
grid on


% --- Executes on selection change in popupmenu3.
function popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu3


% Changes the interpolated background plot
% Can be:
% RMS Error (default)
% Q(1,1) & Q(7,7) & -Q(1,7) & -Q(7,1)
% Q(2,2) & Q(8,8) & -Q(2,8) & -Q(8,2)
% Q(3,3)
% Q(5,5) & Q(6,6)
% R(1,1) & R(2,2)
% Phi(1,1) & Phi(2,2) 
% Phi(3,3) & Phi(4,4)
% Phi(5,5) & Phi(6,6)

%ind = get(handles.popupmenu3,'Value');
ind = get(hObject,'Value');
Data = handles.Data;
horiz = find(Data.alphaVals == get(handles.horiz_slider,'Value'));
vert = find(Data.alphaFineVals == get(handles.vert_slider,'Value'));
c = 1; d = 1; f = 1; g = 1; h = 1; k = 1; l = 1;


% Re-draw All Plots
cla(handles.RMSgrid_axes)
axes(handles.RMSgrid_axes)
hold on
if ind == 1
%     colormap('hot');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridAlpha);
%     title('RMS Error')
    handles.cb = colorbar;
elseif ind == 2
    Data.gridQ1177 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridQ1177(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Q(1,1));'])
        end
    end
%     colormap('cool');
    pcolor(handles.RMSgrid_axes,Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridQ1177)
%     title('Q(1,1) & Q(7,7)')
    handles.cb = colorbar;
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
%    set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
elseif ind == 3
    handles.gridQ2288 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridQ2288(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Q(2,2));'])
        end
    end
%     colormap('spring');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridQ2288)
%     title('Q(2,2) & Q(8,8)')
    handles.cb = colorbar;
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
    %    set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
elseif ind == 4
    handles.gridQ33 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridQ33(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Q(3,3));'])
        end
    end    
%     colormap('summer');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridQ33)
%     title('Q(3,3)')
    handles.cb = colorbar;
%     set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
elseif ind == 5
    handles.gridQ5566 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridQ5566(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Q(5,5));'])
        end
    end      
%     colormap('autumn');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridQ5566)    
%     title('Q(5,5) & Q(6,6)')  
    handles.cb = colorbar;
%     set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
elseif ind == 6
    handles.gridR1122 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridR1122(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.R(1,1));'])
        end
    end      
%     colormap('winter');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridR1122)
%     title('R(1,1) & R(2,2)')
    handles.cb = colorbar;
%     set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
elseif ind == 7
    handles.gridPhi1122 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridPhi1122(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Phi(1,1));'])
        end
    end  
%     colormap('bone');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridPhi1122)    
%     title('Phi(1,1) & Phi(2,2)')
    handles.cb = colorbar;
%     set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
elseif ind == 8
    Data.gridPhi3344 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridPhi3344(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Phi(3,3));'])
        end
    end      
%     colormap('copper');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridPhi3344)
%     title('Phi(3,3) & Phi(4,4)')
    handles.cb = colorbar;
%     set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
elseif ind == 9
    handles.gridPhi5566 = zeros(length(Data.alphaVals),length(Data.alphaFineVals));
    for a = 1:length(Data.alphaVals) 
        for b = 1:length(Data.alphaFineVals)
            eval(['Data.gridPhi5566(a,b) = log10(Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                '.params.Phi(5,5));'])
        end
    end      
%     colormap('jet');
    pcolor(Data.alphaValsGrid,Data.alphaFineValsGrid,Data.gridPhi5566)    
%     title('Phi(5,5) & Phi(6,6)')
    handles.cb = colorbar;
%     set(handles.cb,'YTickLabel',10.^(str2num(get(handles.cb,'YTickLabel'))));
    set(handles.cb,'YTickLabel',sprintf('%0.3g|',10.^(str2num(get(handles.cb,'YTickLabel')))));
end
% handles.cb = colorbar;
shading interp
hold on
plot(Data.alphaValsGrid,Data.alphaFineValsGrid,'co','MarkerSize',4,...
    'MarkerFaceColor','none')
if isfield(Data,'gridFlags')
    [ii,jj] = find(Data.gridFlags~=2);
    plot(Data.alphaVals(jj),Data.alphaFineVals(ii),'mo','MarkerSize',4)
end

hold off

set(gca,'XTick', Data.alphaVals)
set(gca,'YTick', Data.alphaFineVals)
xlabel('Alpha Values for Trajectory to Be Fit')
ylabel('Alpha Values for Resulting Fitted Trajectory')



horizVal = get(handles.horiz_slider,'Value');
vertVal = get(handles.vert_slider,'Value');

%set(handles.gridMarker,'XData',horizVal,'YData',vertVal);
hold on;
handles.gridMarker = plot(handles.RMSgrid_axes,horizVal,vertVal,'o','markerfacecolor','b','markersize',10);
hold off;

handles.Data = Data;

% Get Current Measure of Fit
ind2 = get(handles.popupmenu2,'Value');
handles.Data.cutoff = str2num(get(handles.edit1,'String'));
% STATISTICAL TEST (Z-score, p-value)
if ind2 == 1
    % Max Perp Error
    set(handles.edit1,'String',0.05);
    handles.Data.cutoff = 0.05;
    set(handles.units,'String','p-value');
    for a = 1:length(handles.Data.alphaVals) 
        eval(['[~,maxPerpToFitInd] = max(abs(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,1)));'])
        eval(['maxPerpToFit = handles.Data.toFitTraj.alpha',num2str(a),'.x(maxPerpToFitInd,1);'])
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['[~,maxPerpFittedInd] = max(abs(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,1)));'])
            eval(['maxPerpFitted = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxPerpFittedInd,1);'])
            [~,handles.Data.measure(b,a)] = ztest(maxPerpFitted,maxPerpToFit,handles.maxPerpSTD);
        end
    end
elseif ind2 == 2
    % Integral Error
    set(handles.edit1,'String',0.002);
    handles.Data.cutoff = 0.01;
    set(handles.units,'String','meters');
    for a = 1:length(handles.Data.alphaVals) 
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['handles.Data.measure(b,a) = handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.RMSError;'])
        end
    end 
elseif ind2 == 3
    % Angular Error
    set(handles.edit1,'String',0.05');
    handles.Data.cutoff = 0.05;
    set(handles.units,'String','p-value')
    for a = 1:length(handles.Data.alphaVals) 
        eval(['[~,maxVelToFitInd] = max(handles.Data.toFitTraj.alpha',num2str(a),'.x(:,3).^2 + handles.Data.toFitTraj.alpha',num2str(a),'.x(:,4).^2);'])
        eval(['vectorToFit = [handles.Data.toFitTraj.alpha',num2str(a),...
            '.x(maxVelToFitInd,1) - 0; handles.Data.toFitTraj.alpha',num2str(a),...
            '.x(maxVelToFitInd,2) + 0.1];'])
        angErrToFit = acosd(dot(vectorToFit,[0;1])/(norm(vectorToFit)*norm([0;1])));
        for b = 1:length(handles.Data.alphaFineVals)
            eval(['[~,maxVelFittedInd] = max(handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,3).^2 + handles.Data.alphaFitted',num2str(b),'.alphaToFit',...
                num2str(a),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(:,4).^2);'])
            eval(['vectorFitted = [handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,1) - 0;',...
                'handles.Data.alphaFitted',num2str(b),'.alphaToFit',num2str(a),...
                '.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.x(maxVelFittedInd,2) + 0.1];'])
            angErrFitted = acosd(dot(vectorFitted,[0;1])/(norm(vectorFitted)*norm([0;1])));
            [~,handles.Data.measure(b,a)] = ztest(angErrFitted,angErrToFit,handles.angErrSTD);
        end
    end
end

% Plot Xs over Values, For now, only mark good trials with green Xs
axes(handles.RMSgrid_axes)
hold on;
plot(handles.Data.alphaValsGrid,handles.Data.alphaFineValsGrid,'rx','MarkerSize',20);
[ii,jj] = find(handles.Data.measure>handles.Data.cutoff);
plot(handles.Data.alphaVals(jj),handles.Data.alphaFineVals(ii),'gx','MarkerSize',20)
hold off;

guidata(hObject, handles);


%---------------CREATION FUNCTIONS-----------------------%


% --- Executes during object creation, after setting all properties.
function vert_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to vert_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function horiz_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to horiz_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
