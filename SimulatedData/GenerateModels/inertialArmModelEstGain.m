function xPlotting = inertialArmModelEstGain(estGain,curlGain,plotflag)

% ISSUE: This model forces the terminal control to 0, when it doesnt need
% to be.  Max's model does not... what is his doing that this is not?

% For "PUSH" Task
Px0 = 0;%m
Py0 = -.10;%m 
Vx0 = 0;
Vy0 = 0;
Fx0 = 0;
Fy0 = 0;
Tx0 = 0;
Ty0 = .1;
x0= [Px0; Py0; Vx0; Vy0; Fx0; Fy0; Tx0; Ty0];

rate = 1000;%Hz
dt = 1/rate;
% t = 0;%sec
% T = 4;%sec
% time = t:1/rate:T;
%n = 120;
n = 0.6*rate;

% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;
Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];
% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);
% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));
% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));
% inertia matrix
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;
Inertia = [H11 H12; H12 H22];
Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);

% Curl Field
%curlGain = -20;%Ns/m
%curlGain = 0;%Ns/m

% Initialize Cost Array
J = zeros(n,length(estGain));


Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
Fsys(3:4,3:4) = curlGain*invMass*[0 1;-1 0];
FsysEst = zeros(8);


% Controller Matrix
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;

for i = 1:length(estGain)

FsysEst(3:4,3:4) = estGain(i)*invMass*[0 1;-1 0];


% Make discrete Time
%AdisNull = eye(8) + Asys*dt;
%AdisCurl = eye(8) + Asys*dt + Fsys*dt;
A = eye(8) + Asys*dt + Fsys*dt;
Aest = eye(8) + Asys*dt + FsysEst*dt;

B = Bsys*dt;


if i == 1;
% For Plotting
xPlotting = zeros(n,8,length(estGain));
colorArray = hsv(length(estGain));
%colorMatrix = permute(reshape(colorArray.', 3,length(Q_11),[]),[2 3 1]);
colorMatrix = colorArray;
end

%Cost Function Matrices
% Q = [.0001 0 0 0 0 0 -.0001 0; 
%      0 0 0 0 0 0 0 0;
%      0 0 0 0 0 0 0 0;
%      0 0 0 0 0 0 0 0;
%      0 0 0 0 0 0 0 0;
%      0 0 0 0 0 0 0 0;
%      -.0001 0 0 0 0 0 .0001 0;
%      0 0 0 0 0 0 0 0];%1e2 [1e3]
Q = [0 0 0 0 0 0 0 0; 
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0;
     0 0 0 0 0 0 0 0];%1e2 [1e3]
R = [1 0;
     0 1];
F = [1e8 0 0 0 0 0 -1e8 0; 
     0 1e8 0 0 0 0 0 -1e8;
     0 0 1e6 0 0 0 0 0;%6
     0 0 0 1e6 0 0 0 0;
     0 0 0 0 1e4 0 0 0;%4
     0 0 0 0 0 1e4 0 0;
     -1e8 0 0 0 0 0 1e8 0;
     0 -1e8 0 0 0 0 0 1e8];

%Terminal Conditions
S = zeros(8,8,n);
S(:,:,n) = F;
% Qhat = zeros(8,8,n);
% Qhat(:,:,n) = 0.5*
% Qt = repmat(Q,[1,1,n]);
% Qt(:,:,n) = F;
% Rt = repmat(R,[1,1,n]);
L = zeros(2,8,n-1);

% Iterate Backwards
for k = n-1:-1:1
    
    % Ricatti Equation
%     S(:,:,k) = Q + Aest'*S(:,:,k+1)*Aest - ...
%          Aest'*S(:,:,k+1)*B*inv(R + B'*S(:,:,k+1)*B)*B'*S(:,:,k+1)*Aest;
    S(:,:,k) = Q + Aest'*S(:,:,k+1)*Aest - ...
        (Aest'*S(:,:,k+1)*B)*inv(R + B'*S(:,:,k+1)*B)*(B'*S(:,:,k+1)*Aest);
    % Feedback Gain Matrix
    L(:,:,k) = inv(B'*S(:,:,k+1)*B + R)*B'*S(:,:,k+1)*Aest;

end

% Iterate Forward, Find Trajectory
x = zeros(n,8);
x(1,:) = x0';
u = zeros(n-1,2);
P = zeros(8,8,n);

for k = 2:n
    
    u(k-1,:) = -L(:,:,k-1)*x(k-1,:)';

    x(k,:) = ( A*x(k-1,:)' + B*u(k-1,:)' )';
    
    P(:,:,k) = A*P(:,:,k-1)*A';
    
end

%    [~,J(:,i)] = trajectoryCost(Q,R,F,x,u); 
    
    xPlotting(:,:,i) = x;
end


if plotflag == 1
% Plot trajectory
figure
subplot(1,2,1)
hold on
for i = 1:length(estGain)
p(i) = plot(xPlotting(:,1,i),xPlotting(:,2,i),'col',colorMatrix(i,:));
plot(x0(1),x0(2),'gx')
plot(x0(7),x0(8),'rx')
end
hold off
axis equal
%ylim([-.22,.02]),xlim([-0.05 0.05])
%ylim([-.22,.02]),xlim([-0.1 0.1])
title('X-Y Trajectory')
xlabel('(meters)'),ylabel('(meters)')
%set(gca,'YTick', -0.2:0.05:0)
%set(gca,'YTickLabel', -0.1:0.05:0.1)

%legend(p,'1e-2','1','1e2','1e4','1e6','location','West')
%hLeg = legend(p,'0','0.2','0.4','0.6','0.8','1.0','location','West');

% hLeg = legend(p,['0, J=',num2str(J(end,1))],['0.2, J=',num2str(J(end,2))],...
%     ['0.4, J=',num2str(J(end,3))],['0.6, J=',num2str(J(end,4))],...
%     ['0.8, J=',num2str(J(end,5))],['1.0, J=',num2str(J(end,6))],...
%     'location','West');
% hTitle = get(hLeg,'Title');
% set(hTitle,'String','Alpha')

subplot(5,2,2)
hold on
for i = 1:length(estGain)
    plot(xPlotting(:,3,i),'col',colorMatrix(i,:))
end
hold off
xlim([0 n]),title('X-Velocity')
ylabel('(m/s)')
%set(gca,'XTick', 0:40:120)
%set(gca,'XTickLabel',0:200:600)

subplot(5,2,4)
hold on
for i = 1:length(estGain)
plot(xPlotting(:,4,i),'col',colorMatrix(i,:))
end
hold off
xlim([0 n]),title('Y-Velocity')
ylabel('(m/s)')
%set(gca,'XTick', 0:40:120)
%set(gca,'XTickLabel',0:200:600)

subplot(5,2,6)
hold on
for i = 1:length(estGain)
    plot(xPlotting(:,5,i),'col',colorMatrix(i,:));
end
hold off
xlim([0 n]),title('X-Force')
ylabel('(N)')
%set(gca,'XTick', 0:40:120)
%set(gca,'XTickLabel',0:200:600)

subplot(5,2,8)
hold on
for i = 1:length(estGain)
    plot(xPlotting(:,6,i),'col',colorMatrix(i,:));
end
hold off
xlim([0 n]),title('Y-Force')
ylabel('(N)')
%set(gca,'XTick', 0:40:120)
%set(gca,'XTickLabel',0:200:600)

subplot(5,2,10)
hold on
for i = 1:length(estGain)
    %plot(J(1:end-1,i),'col',colorMatrix(i,:));
end
hold off
xlim([0 n]),title('Cost')
ylabel('(N)')
%set(gca,'XTick', 0:40:120)
%set(gca,'XTickLabel',0:200:600),xlabel('Time (ms)')

suptitle('Arm Reach Model - Varying Percentage Learned of Curl Field')
%legend(p,'X','Y')
end