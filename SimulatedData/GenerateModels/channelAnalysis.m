% Run Channel Trial, Fit Gain

alpha = 0:0.1:1;
curlGain = 20;
for i = 1:length(alpha)
    x1 = channelModel(alpha(i),1,0);
    g1(i) = fitgain(x1(:,5),x1(:,4));
    x2 = channelModel(alpha(i),2,0);
    g2(i) = fitgain(x2(:,5),x2(:,4));
end

figure
plot(alpha*curlGain,g1,'bo')
hold on
plot(alpha*curlGain,g2,'go')
xlabel('Internal Model of Gain')
ylabel('Estimated Value of Gain Using Fit Function')
axis square;grid on;
legend('W/ Vert. Curl','W/O Vert. Curl','Location','NorthWest')


% %Fit only middle of trajectory...
% for i = 1:length(alpha)
%     x1 = channelModel(alpha(i),1,0);
%     g1(i) = fitgain(x1(200:end-200,5),x1(200:end-200,4));
%     x2 = channelModel(alpha(i),2,0);
%     g2(i) = fitgain(x2(200:end-200,5),x2(200:end-200,4));
% end
% 
% figure
% plot(alpha*curlGain,g1,'bo')
% hold on
% plot(alpha*curlGain,g2,'ro')
% xlabel('Internal Model of Gain')
% ylabel('Estimated Value of Gain Using Fit Function')
% axis square;grid on;
% legend('W/ Vert. Curl','W/O Vert. Curl')