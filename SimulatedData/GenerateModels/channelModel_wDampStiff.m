%% Channel Trial Model w/ Inertial Arm Model

% Initial Parameters

%%% INPUT THESE VALUES AS FUNCTION - BASED ON SUBJECT DATA AND PHASE
load('YoungData-ToFit-WAccel-20140905.mat')
estGain = -14;
curlGain = -20;
trialLength = 139;
%%% 

% x0 = [0;-0.1;0;0;0;0;0;0.1];
dt = 1/200;%sec
t0 = 0;

%% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22];

Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);

%Set initial parameters
forceInit = Mass*[(yData.LF2.VxMean(2) - yData.LF2.VxMean(1))/dt;
                  (yData.LF2.VyMean(2) - yData.LF2.VyMean(1))/dt];
x0 = [yData.LF2.PxMean(1);
      yData.LF2.PyMean(1);
      yData.LF2.VxMean(1);
      yData.LF2.VyMean(1);
      forceInit(1);
      forceInit(2); 
      0; 
      0.1];

%% Dynamics Matrices

% Estimated A matrix
% Subject expects a curl field with an estimated gain
Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
Fsys(3:4,3:4) = invMass*[0 1;-1 0];

% Controller Matrix
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;

% Find Estimated Dynamics
Aest = eye(8) + Asys*dt + estGain*Fsys*dt;
Aact = eye(8) + Asys*dt + curlGain*Fsys*dt;
B = dt*Bsys;


% Channel Trial Dynamics
stiffConst = 2000;
dampConst = 50;

Fsys_Chan = zeros(8);
Fsys_Chan(3:4,1:4) = invMass*[-1*stiffConst 0 -1*dampConst 0;
                               0            0  0           0];
A_Chan = eye(8) + Asys*dt + Fsys_Chan*dt;

%% Cost Parameters

Q = zeros(8);
R = zeros(2);
Phi = zeros(8);

% For now, using parameters fit to young data, phase LF2... 
% [REMEMBER TO UPDATE estGain ABOVE]
Q11Vals = [1.1160e7];
Q22Vals = [0];
Q33Vals = [0];
Q5566Vals = [0];
R1122Vals = [4.1716e9];
Phi1122Vals = [5.8324e7];
Phi3344Vals = [4.4536e5];
Phi5566Vals = [1.0952e3];

% Fill in Q Matrix
Q(1,1) = Q11Vals;
Q(7,7) = Q11Vals;
Q(1,7) = -1*Q11Vals;
Q(7,1) = -1*Q11Vals;

Q(2,2) = Q22Vals;
Q(8,8) = Q22Vals;
Q(2,8) = -1*Q22Vals;
Q(8,2) = -1*Q22Vals;

Q(3,3) = Q33Vals;

Q(5,5) = Q5566Vals;
Q(6,6) = Q5566Vals;

% Fill in R Matrix
R(1,1) = R1122Vals;
R(2,2) = R1122Vals;

% Fill in Phi Matrix
Phi(1,1) = Phi1122Vals;
Phi(7,7) = Phi1122Vals;
Phi(1,7) = -1*Phi1122Vals;
Phi(7,1) = -1*Phi1122Vals;
Phi(2,2) = Phi1122Vals;
Phi(8,8) = Phi1122Vals;
Phi(2,8) = -1*Phi1122Vals;
Phi(8,2) = -1*Phi1122Vals;

Phi(3,3) = Phi3344Vals;
Phi(4,4) = Phi3344Vals;

Phi(5,5) = Phi5566Vals;
Phi(6,6) = Phi5566Vals;

%% Solve for Trajectory
%Terminal Conditions
S = zeros(size(Asys,1),size(Asys,2),trialLength);
S(:,:,trialLength) = Phi;
L = zeros(size(B,2),size(Asys,1),trialLength-1);

% Iterate Backwards
for k = trialLength-1:-1:1
    
    % Ricatti Equation
    S(:,:,k) = Q + Aest'*S(:,:,k+1)*Aest - ...
        (Aest'*S(:,:,k+1)*B)*inv(R + B'*S(:,:,k+1)*B)*(B'*S(:,:,k+1)*Aest);
    % Feedback Gain Matrix
    L(:,:,k) = inv(B'*S(:,:,k+1)*B + R)*B'*S(:,:,k+1)*Aest;

end

% Iterate Forward, Find Trajectory
x = zeros(trialLength,size(Asys,1));
x(1,:) = x0';
u = zeros(trialLength-1,2);
P = zeros(size(Asys,1),size(Asys,2),trialLength);

xCurl = x;
uCurl = u;
PCurl = P;

for k = 2:trialLength
    
    u(k-1,:) = -L(:,:,k-1)*x(k-1,:)';
    uCurl(k-1,:) = -L(:,:,k-1)*xCurl(k-1,:)';
    
    x(k,:) = ( A_Chan*x(k-1,:)' + B*u(k-1,:)' )';
    xCurl(k,:) = ( Aact*xCurl(k-1,:)' + B*uCurl(k-1,:)' )';
    
    P(:,:,k) = A_Chan*P(:,:,k-1)*A_Chan';
    P(:,:,k) = Aact*PCurl(:,:,k-1)*Aact';

end

SOL.x = x;
SOL.u = u;
SOL.time = [0:dt:(trialLength-1)*dt]';

SOL_Curl.x = xCurl;
SOL_Curl.u = uCurl;
SOL_Curl.time = [0:dt:(trialLength-1)*dt]';

%% Plot Resulting Trajectory

figure
subplot(1,2,1)
h(1) = plot(SOL.x(:,1),SOL.x(:,2));
hold on
h(2) = plot(SOL_Curl.x(:,1),SOL_Curl.x(:,2),'r');
h(3) = plot(yData.LF2.PxMean,yData.LF2.PyMean,'m');
ciplotY((yData.LF2.PxMean - yData.LF2.PxSE),...
        (yData.LF2.PxMean + yData.LF2.PxSE),...
        yData.LF2.PyMean,'m',0.5);
axis equal
title('XY-Trajectory')

% Eventually plot averaged curl trial for phase of subject data, and
% averaged channel trials to compare with model

subplot(6,2,2)
plot(SOL.time,SOL.x(:,1))
hold on
plot(SOL_Curl.time,SOL_Curl.x(:,1),'r')
xlim([SOL.time(1) SOL.time(end)])
title('X-Pos')

subplot(6,2,4)
plot(SOL.time,SOL.x(:,2))
hold on
plot(SOL_Curl.time,SOL_Curl.x(:,2),'r')
xlim([SOL.time(1) SOL.time(end)])
title('Y-Pos')

subplot(6,2,6)
plot(SOL.time,SOL.x(:,3))
hold on
plot(SOL_Curl.time,SOL_Curl.x(:,3),'r')
xlim([SOL.time(1) SOL.time(end)])
title('X-Vel')

subplot(6,2,8)
plot(SOL.time,SOL.x(:,4))
hold on
plot(SOL_Curl.time,SOL_Curl.x(:,4),'r')
xlim([SOL.time(1) SOL.time(end)])
title('Y-Vel')

subplot(6,2,10)
plot(SOL.time,SOL.x(:,5))
hold on
plot(SOL_Curl.time,SOL_Curl.x(:,5),'r')
xlim([SOL.time(1) SOL.time(end)])
title('X-Force')

subplot(6,2,12)
plot(SOL.time,SOL.x(:,6))
hold on
plot(SOL_Curl.time,SOL_Curl.x(:,6),'r')
xlim([SOL.time(1) SOL.time(end)])
title('Y-Force')

legend(h,'Channel Trial','Curl Field','LF2 Data','Location','SouthWest')


%% Compare Model Fit to True Estimated Gain

measuredEstGain = -1*fitgain(SOL.x(:,5),SOL.x(:,4)); % Multiply by -1 because the force is opposite the curl gain magnitude

% Display Results
disp(['Actual Gain: -20, True Estimate: ',num2str(estGain),' Estimate From Model: ',num2str(measuredEstGain)]) 

