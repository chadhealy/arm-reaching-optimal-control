%%% SWEEP OVER ALL ALPHAS, PARAMETERS TO FIT
%
%
% QUESTION: Fit Q22 or Q44 (vertical position, velocity)???
%

tic
% Values to be Fit Over
%alphaVals = 0:.1:1;


% Dynamics
x0 = [0;-0.1;0;0;0;0;0;0.1];
curlGain = -20;%Ns/m (curl gain)
% From experimental Data
dt = 1/500;%sec
tf = 0.6;%sec
t0 = 0;
trialLength = length(t0:dt:tf);

% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);

% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22];

Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);


t0 = 0;
%trialLength = length(t0:dt:tf);
Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
% Fsys(3:4,3:4) = curlGain/mass*[0 1;-1 0];
Fsys(3:4,3:4) = curlGain*invMass*[0 1;-1 0];


% Controller Matrix
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;

% Make discrete Time
AdisNull = eye(8) + Asys*dt;
AdisCurl = eye(8) + Asys*dt + Fsys*dt;
Bdis = dt*Bsys;

%Could be Adis instead of AdisNull and Curl (not used in code, so not a
%big deal)


% % Small Vectors for Initial Run (~65,000 iterations... 7.5 years to run)
% alphaVals = 0:.2:1;
% Q11Vals = [0 10.^(-4:2:0)];
% Q33Vals = [0 10.^(-6:3:0)];
% Q5566Vals = [0 10.^(-14:6:2)];
% R1122Vals = [10.^(-12:4:-4)];
% Phi1122Vals = [0 10.^(0:4:8)];
% Phi3344Vals = [0 10.^(0:3:6)];
% Phi5566Vals = [0 10.^(0:2:4)];
% alphaFineVals = 0:0.5:1;
% %

% Test Vectors (Single Values)
% alphaVals = 0:0.1:1;
% alphaVals = 0.8:0.1:1;
% alphaVals = 0.9;


%alphaVals = 0:0.1:1;

%alphaVals = 0.5:0.1:0.7;
%alphaVals = 1;
% % % First Run
% % % Q11Vals = [1202];
% % Q11Vals = [3.10e4];
% % % Second Run
% % %Q11Vals = [1e3];
% % %Q22Vals = [189];
% % Q22Vals = [474];
% % Q33Vals = [0];
% % Q5566Vals = [.0270];
% % %R1122Vals = [1e-8];
% % R1122Vals = [8.60e6];
% % Phi1122Vals = [5.17e4];
% % Phi3344Vals = [3.32e4];
% % Phi5566Vals = [4.31e4];
%alphaFineVals = 0:0.1:1;
%alphaFineVals = 0.8:0.1:1;
%alphaFineVals = 0.3;
%alphaFineVals = 0:0.1:1;
%alphaFineVals = 0.2:0.1:0.4;
%alphaFineVals = 1;

alphaVals = 0:0.1:1;
alphaFineVals = 0:0.1:1;
% alphaVals = 0:0.1:0.2;
% alphaFineVals = 0:0.1:0.2;
Q11Vals = [3.097e4];
Q22Vals = [473.5694];
Q33Vals = [0];
Q5566Vals = [.0270];
R1122Vals = [8.6032e6];
Phi1122Vals = [5.1670e4];
Phi3344Vals = [3.3167e4];
Phi5566Vals = [4.3100e4];


% Initiate To-Fit Parameter Structure
toFitParams.x0 = x0;
toFitParams.dt = dt;
toFitParams.Asys = Asys;
toFitParams.B = Bdis;
toFitParams.Fsys = Fsys;
toFitParams.Q = zeros(8);
toFitParams.R = zeros(2);
toFitParams.Phi = zeros(8);
toFitParams.trialLength = trialLength;

% Assign Fit Indices
FitIndices = [1, ... % R(1,1) = R(2,2) (control effort)
              1, ... % Q(5,5) = Q(6,6) (force penalty)
              1, ... % Q(1,1) = Q(7,7) = -Q(1,7) = -Q(7,1) (horizontal tracking)             
              1, ... % Q(2,2) = Q(8,8) = -Q(2,8) = -Q(8,2) (vertical dist. penalty)             
              0, ... % Q(3,3) % Horizontal Velocity
              1, ... % Phi(1,1) = Phi(2,2) = Phi(7,7) = Phi(8,8) = -Phi(1,7)... (terminal position)
              1, ... % Phi(3,3) = Phi(4,4) (terminal velocity)
              1, ... % Phi(5,5) = Phi(6,6) (terminal force)
              0];    % alpha (proportional estimate of curl field)
FitIndices = logical(FitIndices);
numX = sum(FitIndices);


%%% Number of solutions
%
numsols = 10;
%
%%%

%initialize To Fit Trajectory Params
c=1;d=1;f=1;g=1;h=1;k=1;l=1;o=1;
toFitParams.Q(1,1) = Q11Vals(c);
toFitParams.Q(7,7) = Q11Vals(c);
toFitParams.Q(1,7) = -1*Q11Vals(c);
toFitParams.Q(7,1) = -1*Q11Vals(c);
toFitParams.Q(3,3) = Q33Vals(d);
toFitParams.Q(5,5) = Q5566Vals(f);
toFitParams.Q(6,6) = Q5566Vals(f);
toFitParams.R(1,1) = R1122Vals(g);
toFitParams.R(2,2) = R1122Vals(g);
toFitParams.Phi(1,1) = Phi1122Vals(h);
toFitParams.Phi(7,7) = Phi1122Vals(h);
toFitParams.Phi(1,7) = -1*Phi1122Vals(h);
toFitParams.Phi(7,1) = -1*Phi1122Vals(h);
toFitParams.Phi(2,2) = Phi1122Vals(h);
toFitParams.Phi(8,8) = Phi1122Vals(h);
toFitParams.Phi(2,8) = -1*Phi1122Vals(h);
toFitParams.Phi(8,2) = -1*Phi1122Vals(h);
toFitParams.Phi(3,3) = Phi3344Vals(k);
toFitParams.Phi(4,4) = Phi3344Vals(k);
toFitParams.Phi(5,5) = Phi5566Vals(l);
toFitParams.Phi(6,6) = Phi5566Vals(l);
toFitParams.Q(2,2) = Q22Vals(o);
toFitParams.Q(8,8) = Q22Vals(o);
toFitParams.Q(2,8) = -1*Q22Vals(o);
toFitParams.Q(8,2) = -1*Q22Vals(o);

toFitParamsTemp = toFitParams;
clear toFitParams

[alphaFineValsGrid,alphaValsGrid] = meshgrid(alphaFineVals,alphaVals);
combos = [alphaFineValsGrid(:) alphaValsGrid(:)];

parfor a = 1:size(combos,1)
%     Params.alpha = combos(a,1);
%     bestFitParams.alpha = combos(a,1);

    toFitParams = toFitParamsTemp;
    toFitParams.alpha = combos(a,2);
    toFitTraj = OptimalReach(toFitParams);

    % Fit Trajectory
    % Re-initialize for each loop
    %       [R(1,1), Q(5,5), Q(1,1), Q(2,2), Q(3,3), Phi(1,1), Phi(3,3), Phi(5,5), alpha]
    BaseX = [1e5;   0;      0;      100;   0;      1e5;      1e5;      1e5;      .5];


    AA = [];
    bb = [];
    Aeq = [];
    beq = [];
    lb = zeros(numX,1);
    % lb(1) = 1e-9; % This is why it went to this value...
    % Lowering the lower bound (upperbound) of R(1,1),
    lb(1) = 1e-15;
    % Currently R(1,1) sometimes reaches a value of zero... was 1e-15. 
    %lb(1) = 1;


    %lb(numX) = 0;  %only if fitting alpha
    ub = 1e20*ones(numX,1);
    %ub(numX) = 2;  %only if fitting alpha
    % Lowering upper bound of R(1,1)
    %ub(1) = 1;
    nonlcon = [];

    Params = toFitParams;
    Params.alpha = combos(a,1);
    bestFitParams = Params;


    Values = zeros(numsols,1);
    Results = zeros(numsols,numX);
    Flags = zeros(numsols,1);

    % Initialize Search Parameters
     options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-14,...
         'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');

    for m = 1:numsols 
        disp(['minimization loop: ',num2str(m),' [',num2str(combos(a,1)),',',num2str(combos(a,2)),']'])

        Xo = BaseX(FitIndices);
        % If fitting over alpha, do not change by same scale.
        if FitIndices(9)
            Xo(1:end-1) = Xo(1:end-1) + 1000*rand(numX,1).*Xo(1:end-1);
            Xo(end) = Xo(end) + rand;
        else
            Xo = Xo + 1000*rand(numX,1).*Xo;
        end

        [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly_NoReg(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
        %flag

        Results(m,:) = X';
        Values(m) = fval;
        Flags(m) = flag;
    end

    % Refined Search
    options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-16,...
        'TolX',1E-16,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');

    for m = numsols+1:2*numsols
        disp(['minimization loop: ',num2str(m),' [',num2str(combos(a,1)),',',num2str(combos(a,2)),']'])

        Xo = Results(m-numsols,:)';
        [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly_NoReg(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);

        Results(m,:) = X';
        Values(m) = fval;
        Flags(m) = flag;
    end


    Results(2*numsols+1,:) = lb';
    Values(2*numsols+1) = RMSfitVector_noGlobal_XYOnly_NoReg(lb,toFitTraj,FitIndices,Params);

    [val, in] = min(Values(Flags==2));
    validResults = Results(Flags==2,:);
    BestX = validResults(in,:);


    count = 1;
    if FitIndices(1)
        bestFitParams.R(1,1) = BestX(count);
        bestFitParams.R(2,2) = BestX(count);
        count = count + 1;
    end
    if FitIndices(2)
        bestFitParams.Q(5,5) = BestX(count);
        bestFitParams.Q(6,6) = BestX(count);
        count = count + 1;
    end
    if FitIndices(3)
        bestFitParams.Q(1,1) = BestX(count);
        bestFitParams.Q(7,7) = BestX(count);
        bestFitParams.Q(1,7) = -1*BestX(count);
        bestFitParams.Q(7,1) = -1*BestX(count);
        count = count + 1;
    end
    if FitIndices(4) 
        bestFitParams.Q(2,2) = BestX(count);
        bestFitParams.Q(8,8) = BestX(count);
        bestFitParams.Q(2,8) = -1*BestX(count);
        bestFitParams.Q(8,2) = -1*BestX(count);
        count = count + 1;
    end
    if FitIndices(5)
       bestFitParams.Q(3,3) = BestX(count); 
       count = count + 1;
    end
    if FitIndices(6)
        bestFitParams.Phi(1,1) = BestX(count);
        bestFitParams.Phi(7,7) = BestX(count);
        bestFitParams.Phi(1,7) = -1*BestX(count);
        bestFitParams.Phi(7,1) = -1*BestX(count);
        bestFitParams.Phi(2,2) = BestX(count);
        bestFitParams.Phi(8,8) = BestX(count);
        bestFitParams.Phi(2,8) = -1*BestX(count);
        bestFitParams.Phi(8,2) = -1*BestX(count);
        count = count + 1;
    end
    if FitIndices(7)
        bestFitParams.Phi(3,3) = BestX(count);
        bestFitParams.Phi(4,4) = BestX(count);
        count = count + 1;
    end
    if FitIndices(8)
        bestFitParams.Phi(5,5) = BestX(count);
        bestFitParams.Phi(6,6) = BestX(count);
        count = count + 1;
    end
    if FitIndices(9)
        bestFitParams.alpha = BestX(count);
    end

    %Find Indices for Variable Naming
%     alphaFineValsInd = find(alphaFineVals==combos(a,1));
%     alphaValsInd = find(alphaVals==combos(a,2));
    % Save Trajectory
%     eval(['alphaFitted',num2str(alphaFineValsInd),'.alphaToFit',num2str(alphaValsInd),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1 = OptimalReach(bestFitParams);']) 
%     % Save Parameters
%     eval(['alphaFitted',num2str(alphaFineValsInd),'.alphaToFit',num2str(alphaValsInd),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_.params = bestFitParams;']) 
%     % Save RMS Value
%     eval(['alphaFitted',num2str(alphaFineValsInd),'.alphaToFit',num2str(alphaValsInd),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.RMSError = val;']) 
%     % Save Flag Value
%     eval(['alphaFitted',num2str(alphaFineValsInd),'.alphaToFit',num2str(alphaValsInd),'.Q11_1.Q33_1.Q5566_1.R1122_1.Phi1122_1.Phi3344_1.Phi5566_1.flag = Flags(in);']) 

    
    alphas{a} = OptimalReach(bestFitParams); 
    alphas{a}.params = bestFitParams;
    alphas{a}.RMSError = val;
    alphas{a}.flag = Flags(in);
end

% Get total time
time2run = toc

%%% Create Data Structure to Save
% Create and Group Trajectories that were Fitted
for a = 1:length(alphaVals)
    toFitParams.alpha = alphaVals(a);
    eval(['toFitTraj.alpha',num2str(a),' = OptimalReach(toFitParams);'])
end
% % Create RMS Grid
% gridAlpha = zeros(length(alphaVals),length(alphaFineVals));
% for a = 1:length(alphaVals) 
%     for b = 1:length(alphaFineVals)
%         eval(['gridAlpha(a,b) = alphaFitted{1,',num2str(a),'}{1,',num2str(b),...
%             '}.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
%             num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
%             '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
%             '.RMSError;'])
%     end
% end
% [alphaValsGrid,alphaFineValsGrid] = meshgrid(alphaVals,alphaFineVals);
% % Fit Results
% for a = 1:length(alphaVals) 
%     for b = 1:length(alphaFineVals)
%         eval(['Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
%             ' = alphaFitted{1,',num2str(a),'}{1,',num2str(b),...
%             '};'])
%     end
% end
% % How Fit Ended
% gridFlags = zeros(length(alphaVals),length(alphaFineVals));
% for a = 1:length(alphaVals) 
%     for b = 1:length(alphaFineVals)
%         eval(['gridFlags(a,b) = alphaFitted{1,',num2str(a),'}{1,',num2str(b),...
%             '}.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
%             num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
%             '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
%             '.flag;'])
%     end
% end
% Create RMS Grid
gridAlpha = zeros(length(alphaVals),length(alphaFineVals));
for a = 1:length(alphaVals) 
    for b = 1:length(alphaFineVals)
        eval(['gridAlpha(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
            '.RMSError;'])
    end
end
[alphaValsGrid,alphaFineValsGrid] = meshgrid(alphaVals,alphaFineVals);
% Fit Results
for a = 1:length(alphaVals) 
    for b = 1:length(alphaFineVals)
        eval(['Data.alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
            ' = alphaFitted',num2str(a),'.alphaToFit',num2str(b),';'])
    end
end
% How Fit Ended
gridFlags = zeros(length(alphaVals),length(alphaFineVals));
for a = 1:length(alphaVals) 
    for b = 1:length(alphaFineVals)
        eval(['gridFlags(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
            '.flag;'])
    end
end


Data.time = t0:dt:tf;
Data.toFitTraj = toFitTraj;
Data.toFitParams = toFitParams;
Data.alphaVals = alphaVals;
Data.alphaFineVals = alphaFineVals;
Data.gridAlpha = gridAlpha;
Data.alphaFineValsGrid = alphaFineValsGrid;
Data.alphaValsGrid = alphaValsGrid;
Data.gridFlags = gridFlags;

