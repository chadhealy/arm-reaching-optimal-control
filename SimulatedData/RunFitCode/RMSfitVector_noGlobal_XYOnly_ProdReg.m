function RMS = RMSfitVector_noGlobal_XYOnly_ProdReg(X,toFitTraj,FitIndices,Params)

%global toFitTraj Params FitIndices



Px = toFitTraj.x(:,1);
Py = toFitTraj.x(:,2);
% Vx = toFitTraj.x(:,3);
% Vy = toFitTraj.x(:,4);
% Fx = toFit.X(:,5);
% Fy = toFit.X(:,6);

N = length(Px);

count = 1;
if FitIndices(1)
    Params.R(1,1) = X(count);
    Params.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.Q(5,5) = X(count);
    Params.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.Q(1,1) = X(count);
    Params.Q(7,7) = X(count);
    Params.Q(1,7) = -1*X(count);
    Params.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.Q(2,2) = X(count);
    Params.Q(8,8) = X(count);
    Params.Q(2,8) = -1*X(count);
    Params.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.Phi(1,1) = X(count);
    Params.Phi(7,7) = X(count);
    Params.Phi(1,7) = -1*X(count);
    Params.Phi(7,1) = -1*X(count);
    Params.Phi(2,2) = X(count);
    Params.Phi(8,8) = X(count);
    Params.Phi(2,8) = -1*X(count);
    Params.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.Phi(3,3) = X(count);
    Params.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.Phi(5,5) = X(count);
    Params.Phi(6,6) = X(count);
    count = count + 1;
end
if FitIndices(9)
%    Params.estGain = X(count);
    Params.alpha = X(count);
end

SOL = OptimalReach(Params);

% % For experimental Data
% RMS = sum((Px' - SOL.X(:,1)).^2);
% RMS = RMS + sum((Py' - SOL.X(:,2)).^2);
% RMS = RMS + sum((Vx' - SOL.X(:,3)).^2);
% RMS = RMS + sum((Vy' - SOL.X(:,4)).^2);
%
% RMS = sqrt(RMS/4/N);

% For Theoretical Data...... TO DO: Make uniform
RMS = sum((Px - SOL.x(:,1)).^2);
RMS = RMS + sum((Py - SOL.x(:,2)).^2);
%RMS = RMS + sum((Vx - SOL.x(:,3)).^2);
%RMS = RMS + sum((Vy - SOL.x(:,4)).^2);
%RMS = RMS + sum((Fx - SOL.X(:,5)).^2);
%RMS = RMS + sum((Fy - SOL.X(:,6)).^2);

%RMS = sqrt(RMS/6/N);
%RMS = sqrt(RMS/4/N);
RMS = sqrt(RMS/2/N);

%Regularization
nonZeroX = X(find(X~=0));
RMS = RMS + prod(nonZeroX)*1e-26;