% After the fact data processing

% Save Time Vector
Data.time = t0:dt:tf;


% Create and Group Trajectories that were Fitted
for a = 1:length(alphaVals)
    toFitParams.alpha = alphaVals(a);
    eval(['toFitTraj.alpha',num2str(a),' = OptimalReach(toFitParams);'])
end

% % Create a grid of Alpha Values
% gridAlpha = zeros(length(alphaFineVals),length(alphaVals));
% for a = 1:length(alphaFineVals) 
%     for b = 1:length(alphaVals)
%         eval(['gridAlpha(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
%             '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
%             num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
%             '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
%             '.RMSError;'])
%     end
% end
% [alphaFineValsGrid,alphaValsGrid] = meshgrid(alphaFineVals,alphaVals);
% Create a grid of Alpha Values
gridAlpha = zeros(length(alphaVals),length(alphaFineVals));
for a = 1:length(alphaVals) 
    for b = 1:length(alphaFineVals)
        eval(['gridAlpha(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
            '.RMSError;'])
    end
end
[alphaValsGrid,alphaFineValsGrid] = meshgrid(alphaVals,alphaFineVals);

% Create a Giant Data Structure
% Trajectories to be Fit
Data.toFitTraj = toFitTraj;
Data.toFitParams = toFitParams;
% Fit Results
Data.alphaFitted1 = alphaFitted1;
Data.alphaFitted2 = alphaFitted2;
Data.alphaFitted3 = alphaFitted3;
Data.alphaFitted4 = alphaFitted4;
Data.alphaFitted5 = alphaFitted5;
Data.alphaFitted6 = alphaFitted6;
Data.alphaFitted7 = alphaFitted7;
Data.alphaFitted8 = alphaFitted8;
Data.alphaFitted9 = alphaFitted9;
Data.alphaFitted10 = alphaFitted10;
Data.alphaFitted11 = alphaFitted11;
%Alpha Parameters
Data.alphaVals = alphaVals;
Data.alphaFineVals = alphaFineVals;
%RMS Error Plot
Data.gridAlpha = gridAlpha;
Data.alphaFineValsGrid = alphaFineValsGrid;
Data.alphaValsGrid = alphaValsGrid;

% Create Grid of Flag Values
% gridFlags = zeros(length(alphaFineVals),length(alphaVals));
% for a = 1:length(alphaFineVals) 
%     for b = 1:length(alphaVals)
%         eval(['gridFlags(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
%             '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
%             num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
%             '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
%             '.flag;'])
%     end
% end
gridFlags = zeros(length(alphaVals),length(alphaFineVals));
for a = 1:length(alphaVals) 
    for b = 1:length(alphaFineVals)
        eval(['gridFlags(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
            '.flag;'])
    end
end
Data.gridFlags = gridFlags;
