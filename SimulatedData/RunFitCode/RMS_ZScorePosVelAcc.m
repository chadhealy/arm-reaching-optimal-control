% RMS Fit Code for Young Subject

% Looks at Individual Phase

function RMS = RMS_ZScorePosVelAcc(X,toFitTraj,FitIndices,Params,subjData,phase)

count = 1;
if FitIndices(1)
    Params.R(1,1) = X(count);
    Params.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.Q(5,5) = X(count);
    Params.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.Q(1,1) = X(count);
    Params.Q(7,7) = X(count);
    Params.Q(1,7) = -1*X(count);
    Params.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.Q(2,2) = X(count);
    Params.Q(8,8) = X(count);
    Params.Q(2,8) = -1*X(count);
    Params.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.Phi(1,1) = X(count);
    Params.Phi(7,7) = X(count);
    Params.Phi(1,7) = -1*X(count);
    Params.Phi(7,1) = -1*X(count);
    Params.Phi(2,2) = X(count);
    Params.Phi(8,8) = X(count);
    Params.Phi(2,8) = -1*X(count);
    Params.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.Phi(3,3) = X(count);
    Params.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.Phi(5,5) = X(count);
    Params.Phi(6,6) = X(count);
    count = count + 1;
end
if FitIndices(9)
%    Params.estGain = X(count);
    Params.alpha = X(count);
end

% Px = toFitTraj.x(:,1);
% Py = toFitTraj.x(:,2);
% Vx = toFitTraj.x(:,3);
% Vy = toFitTraj.x(:,4);
% Fx = toFitTraj.x(:,5);
% Fy = toFitTraj.x(:,6);

n = length(toFitTraj.x(:,1));


% eval(['Params.curlGain = subjData.',Params.phase,'.curlGain;'])
% eval(['n = length(subjData.',Params.phase,'.PxMean);'])
% Params.trialLength = n;
% 
% eval(['forceInit = Params.Mass*[(subjData.',Params.phase,...
%         '.VxMean(2) - subjData.',Params.phase,...
%         '.VxMean(1))/Params.dt; (subjData.',Params.phase,...
%         '.VyMean(2) - subjData.',Params.phase,...
%         '.VyMean(1))/Params.dt];']);
% 
% eval(['Params.x0 = [subjData.',Params.phase,...
%     '.PxMean(1);subjData.',Params.phase,...
%     '.PyMean(1);subjData.',Params.phase,...
%     '.VxMean(1);subjData.',Params.phase,...
%     '.VyMean(1);forceInit(1);forceInit(2); 0; 0.1];'])

SOL = OptimalReach(Params);


% Calculate Error
% Normalized by trial length, number of arrays compared, and by STD so 
% it is essentially comparing z-scores.
    
eval(['RMS = sum(abs((toFitTraj.x(:,1) - SOL.x(:,1))./subjData.',phase,...
    '.PxSE'')) + sum(abs((toFitTraj.x(:,2) - SOL.x(:,2))./subjData.',phase,...
    '.PySE'')) + sum(abs((toFitTraj.x(:,3) - SOL.x(:,3))./subjData.',phase,...
    '.VxSE'')) + sum(abs((toFitTraj.x(:,4) - SOL.x(:,4))./subjData.',phase,...
    '.VySE'')) + sum(abs((toFitTraj.x(:,5) - SOL.x(:,5))./subjData.',phase,...
    '.FxSE'')) + sum(abs((toFitTraj.x(:,6) - SOL.x(:,6))./subjData.',phase,...
    '.FySE''));'])

RMS = sqrt(RMS/6/n);

%Regularization
%RMS = RMS + sum(X*1e-13);