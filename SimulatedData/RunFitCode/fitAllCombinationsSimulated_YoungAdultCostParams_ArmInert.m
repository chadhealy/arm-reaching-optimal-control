%%% SWEEP OVER ALL ALPHAS, PARAMETERS TO FIT
%
%
% QUESTION: Fit Q22 or Q44 (vertical position, velocity)???
%


% Values to be Fit Over
%alphaVals = 0:.1:1;

load('youngData_wForceSE.mat')
phase = 'LF2';


% Dynamics
x0 = [0;-0.1;0;0;0;0;0;0.1];
curlGain = -20;%Ns/m (curl gain)
% From experimental Data
% dt = 1/500;%sec
% tf = 0.6;%sec
% t0 = 0;
% trialLength = length(t0:dt:tf);

dt = 1/200;
eval(['trialLength = length(yData.',phase,'.PxMean);'])


% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);

% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22];

Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);


t0 = 0;
%trialLength = length(t0:dt:tf);
Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
% Fsys(3:4,3:4) = curlGain/mass*[0 1;-1 0];
Fsys(3:4,3:4) = curlGain*invMass*[0 1;-1 0];


% Controller Matrix
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;

% Make discrete Time
AdisNull = eye(8) + Asys*dt;
AdisCurl = eye(8) + Asys*dt + Fsys*dt;
Bdis = dt*Bsys;

%Could be Adis instead of AdisNull and Curl (not used in code, so not a
%big deal)


% % Small Vectors for Initial Run (~65,000 iterations... 7.5 years to run)
% alphaVals = 0:.2:1;
% Q11Vals = [0 10.^(-4:2:0)];
% Q33Vals = [0 10.^(-6:3:0)];
% Q5566Vals = [0 10.^(-14:6:2)];
% R1122Vals = [10.^(-12:4:-4)];
% Phi1122Vals = [0 10.^(0:4:8)];
% Phi3344Vals = [0 10.^(0:3:6)];
% Phi5566Vals = [0 10.^(0:2:4)];
% alphaFineVals = 0:0.5:1;
% %

% Test Vectors (Single Values)
% alphaVals = 0:0.1:1;
% alphaVals = 0.8:0.1:1;
% alphaVals = 0.9;


% alphaVals = 0:0.1:1;
% %alphaVals = 0.5:0.1:0.7;
% %alphaVals = 1;
% % First Run
% % Q11Vals = [1202];
% Q11Vals = [3.10e4];
% % Second Run
% %Q11Vals = [1e3];
% %Q22Vals = [189];
% Q22Vals = [474];
% Q33Vals = [0];
% Q5566Vals = [.0270];
% %R1122Vals = [1e-8];
% R1122Vals = [8.60e6];
% Phi1122Vals = [5.17e4];
% Phi3344Vals = [3.32e4];
% Phi5566Vals = [4.31e4];
% alphaFineVals = 0:0.1:1;
%alphaFineVals = 0.8:0.1:1;
%alphaFineVals = 0.3;
%alphaFineVals = 0:0.1:1;
%alphaFineVals = 0.2:0.1:0.4;
%alphaFineVals = 1;
%
alphaVals = 0:0.2:0.4;
alphaFineVals = 0:0.2:0.4;
% alphaVals = 0:0.1:0.2;
% alphaFineVals = 0:0.1:0.2;
% Q11Vals = [3.097e4];
% Q22Vals = [473.5694];
% Q33Vals = [0];
% Q5566Vals = [.0270];
% R1122Vals = [8.6032e6];
% Phi1122Vals = [5.1670e4];
% Phi3344Vals = [3.3167e4];
% Phi5566Vals = [4.3100e4];
Q11Vals = [1.1160e7];
Q22Vals = [0];
Q33Vals = [0];
Q5566Vals = [0];
R1122Vals = [4.1716e9];
Phi1122Vals = [5.8324e7];
Phi3344Vals = [4.4536e5];
Phi5566Vals = [1.0952e3];

% Initiate To-Fit Parameter Structure
toFitParams.x0 = x0;
toFitParams.dt = dt;
toFitParams.Asys = Asys;
toFitParams.B = Bdis;
toFitParams.Fsys = Fsys;
toFitParams.Q = zeros(8);
toFitParams.R = zeros(2);
toFitParams.Phi = zeros(8);
toFitParams.trialLength = trialLength;

% Assign Fit Indices
FitIndices = [1, ... % R(1,1) = R(2,2) (control effort)
              1, ... % Q(5,5) = Q(6,6) (force penalty)
              1, ... % Q(1,1) = Q(7,7) = -Q(1,7) = -Q(7,1) (horizontal tracking)             
              1, ... % Q(2,2) = Q(8,8) = -Q(2,8) = -Q(8,2) (vertical dist. penalty)             
              0, ... % Q(3,3) % Horizontal Velocity
              1, ... % Phi(1,1) = Phi(2,2) = Phi(7,7) = Phi(8,8) = -Phi(1,7)... (terminal position)
              1, ... % Phi(3,3) = Phi(4,4) (terminal velocity)
              1, ... % Phi(5,5) = Phi(6,6) (terminal force)
              0];    % alpha (proportional estimate of curl field)
FitIndices = logical(FitIndices);
numX = sum(FitIndices);


%%% Number of solutions
%
numsols = 10;
%
%%%



for a = 1:length(alphaFineVals) 
    Params.alpha = alphaFineVals(a);
    bestFitParams.alpha = alphaFineVals(a);
    
    for b = 1:length(alphaVals)
        toFitParams.alpha = alphaVals(b);

        for c = 1:length(Q11Vals)
            toFitParams.Q(1,1) = Q11Vals(c);
            toFitParams.Q(7,7) = Q11Vals(c);
            toFitParams.Q(1,7) = -1*Q11Vals(c);
            toFitParams.Q(7,1) = -1*Q11Vals(c);

            for d = 1:length(Q33Vals)
                toFitParams.Q(3,3) = Q33Vals(d);

                for f = 1:length(Q5566Vals)
                    toFitParams.Q(5,5) = Q5566Vals(f);
                    toFitParams.Q(6,6) = Q5566Vals(f);

                    for g = 1:length(R1122Vals)
                        toFitParams.R(1,1) = R1122Vals(g);
                        toFitParams.R(2,2) = R1122Vals(g);

                        for h = 1:length(Phi1122Vals)
                            toFitParams.Phi(1,1) = Phi1122Vals(h);
                            toFitParams.Phi(7,7) = Phi1122Vals(h);
                            toFitParams.Phi(1,7) = -1*Phi1122Vals(h);
                            toFitParams.Phi(7,1) = -1*Phi1122Vals(h);
                            toFitParams.Phi(2,2) = Phi1122Vals(h);
                            toFitParams.Phi(8,8) = Phi1122Vals(h);
                            toFitParams.Phi(2,8) = -1*Phi1122Vals(h);
                            toFitParams.Phi(8,2) = -1*Phi1122Vals(h);

                            for k = 1:length(Phi3344Vals)
                                toFitParams.Phi(3,3) = Phi3344Vals(k);
                                toFitParams.Phi(4,4) = Phi3344Vals(k);

                                for l = 1:length(Phi5566Vals)
                                    toFitParams.Phi(5,5) = Phi5566Vals(l);
                                    toFitParams.Phi(6,6) = Phi5566Vals(l);
                                    
                                    for o = 1:length(Q22Vals)
                                        toFitParams.Q(2,2) = Q22Vals(o);
                                        toFitParams.Q(8,8) = Q22Vals(o);
                                        toFitParams.Q(2,8) = -1*Q22Vals(o);
                                        toFitParams.Q(8,2) = -1*Q22Vals(o);

                                        % Get Trajectory to Fit
                                        toFitTraj = OptimalReach(toFitParams);

                                        % Fit Trajectory

                                        % Re-initialize for each loop
                                        %       [R(1,1), Q(5,5), Q(1,1), Q(2,2), Q(3,3), Phi(1,1), Phi(3,3), Phi(5,5), alpha]
                                        % BaseX = [1e-8;   0;      0;      1e-8;   0;      1e4;      1e4;      1e2;      .5];
                                        % For new motorGain value (try starting at high motor costs...)
                                        BaseX = [1e5;   0;      0;      100;   0;      1e5;      1e5;      1e5;      .5];


                                        AA = [];
                                        bb = [];
                                        Aeq = [];
                                        beq = [];
                                        lb = zeros(numX,1);
                                        % lb(1) = 1e-9; % This is why it went to this value...
                                        % Lowering the lower bound (upperbound) of R(1,1),
                                        lb(1) = 1e-15;
                                        % Currently R(1,1) sometimes reaches a value of zero... was 1e-15. 
                                        %lb(1) = 1;


                                        %lb(numX) = 0;  %only if fitting alpha
                                        ub = 1e20*ones(numX,1);
                                        %ub(numX) = 2;  %only if fitting alpha
                                        % Lowering upper bound of R(1,1)
                                        %ub(1) = 1;
                                        nonlcon = [];

                                        Params = toFitParams;
                                        Params.alpha = alphaFineVals(a);
                                        bestFitParams = Params;


                                        Values = zeros(numsols,1);
                                        Results = zeros(numsols,numX);
                                        Flags = zeros(numsols,1);

                                        % Initialize Search Parameters
    %                                     options = optimset('MaxFunEval',1E4,'MaxIter',1E4,'TolFun',1E-12,...
    %                                         'TolX',1E-12,'TolCon',1E-12,'LargeScale','off','Algorithm','sqp');
                                        % Trying new parameters
                                         options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-14,...
                                             'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');
%                                         options = optimset('MaxFunEval',1E6,'MaxIter',1E6,'TolFun',1E-16,...
%                                             'TolX',1E-16,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');

                                        for m = 1:numsols 
                                            disp(['minimization loop: ',num2str(m)])

                                            Xo = BaseX(FitIndices);
                                            % If fitting over alpha, do not change by same scale.
                                            if FitIndices(9)
                                                Xo(1:end-1) = Xo(1:end-1) + 1000*rand(numX,1).*Xo(1:end-1);
                                                Xo(end) = Xo(end) + rand;
                                            else
                                                Xo = Xo + 1000*rand(numX,1).*Xo;
                                            end

%                                             [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly_ProdReg(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
                                            [X, fval, flag] = fmincon(@(X) RMS_ZScorePosVelAcc(X,toFitTraj,FitIndices,Params,yData,phase),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
                                            %flag

                                            Results(m,:) = X';
                                            Values(m) = fval;
                                            Flags(m) = flag;
                                        end

                                        % Refined Search
    %                                     options = optimset('MaxFunEval',1E3,'MaxIter',1E3,'TolFun',1E-14,...
    %                                         'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');
                                        options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-16,...
                                            'TolX',1E-16,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');

                                        for m = numsols+1:2*numsols
                                            disp(['refining minimization loop: ',num2str(m)])

                                            Xo = Results(m-numsols,:)';
%                                             [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly_ProdReg(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
                                            [X, fval, flag] = fmincon(@(X) RMS_ZScorePosVelAcc(X,toFitTraj,FitIndices,Params,yData,phase),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
                                            %flag

                                            Results(m,:) = X';
                                            Values(m) = fval;
                                            Flags(m) = flag;
                                        end


                                        Results(2*numsols+1,:) = lb';
%                                         Values(2*numsols+1) = RMSfitVector_noGlobal_XYOnly_ProdReg(lb,toFitTraj,FitIndices,Params);
                                        Values(2*numsols+1) = RMS_ZScorePosVelAcc(lb,toFitTraj,FitIndices,Params,yData,phase);

%                                         [val, in] = min(Values);
%                                         BestX = Results(in,:);
                                        [val, in] = min(Values(Flags==2));
                                        validResults = Results(Flags==2,:);
                                        BestX = validResults(in,:);

%                                         nLoops = 2;
%                                         while Flags(in) ~= 2
%                                             % Change step size to 1e-18
%                                             options = optimset('MaxFunEval',1E4,'MaxIter',1E4,'TolFun',1E-16,...
%                                                 'TolX',1E-18,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');
% 
%                                             % Re-run for better solution
% 
%                                             % Set initial point at best
%                                             % solution that met fmincon
%                                             % conditions
%                                             [~,newInd] = min(Values(find(Flags==2)));
% 
%                                             for m = nLoops*numsols+1:(nLoops+1)*numsols
%                                                 disp(['refining minimization loop ',num2str(nLoops),': ',num2str(m)])
% 
%                                                 Xo = Results(newInd,:)';
%                                                 [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
% 
%                                                 Results(m,:) = X';
%                                                 Values(m) = fval;
%                                                 Flags(m) = flag;
%                                             end
%                                         Results((nLoops+1)*numsols+1,:) = lb';
%                                         Values((nLoops+1)*numsols+1) = RMSfitVector_noGlobal_XYOnly(lb,toFitTraj,FitIndices,Params);
% 
%                                         [val, in] = min(Values(nLoops*numsols+1:end));
%                                         BestX = Results(in,:);
% 
%                                         nLoops = nLoops + 1;
%                                         end

                                        count = 1;
                                        if FitIndices(1)
                                            bestFitParams.R(1,1) = BestX(count);
                                            bestFitParams.R(2,2) = BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(2)
                                            bestFitParams.Q(5,5) = BestX(count);
                                            bestFitParams.Q(6,6) = BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(3)
                                            bestFitParams.Q(1,1) = BestX(count);
                                            bestFitParams.Q(7,7) = BestX(count);
                                            bestFitParams.Q(1,7) = -1*BestX(count);
                                            bestFitParams.Q(7,1) = -1*BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(4) 
                                            bestFitParams.Q(2,2) = BestX(count);
                                            bestFitParams.Q(8,8) = BestX(count);
                                            bestFitParams.Q(2,8) = -1*BestX(count);
                                            bestFitParams.Q(8,2) = -1*BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(5)
                                           bestFitParams.Q(3,3) = BestX(count); 
                                           count = count + 1;
                                        end
                                        if FitIndices(6)
                                            bestFitParams.Phi(1,1) = BestX(count);
                                            bestFitParams.Phi(7,7) = BestX(count);
                                            bestFitParams.Phi(1,7) = -1*BestX(count);
                                            bestFitParams.Phi(7,1) = -1*BestX(count);
                                            bestFitParams.Phi(2,2) = BestX(count);
                                            bestFitParams.Phi(8,8) = BestX(count);
                                            bestFitParams.Phi(2,8) = -1*BestX(count);
                                            bestFitParams.Phi(8,2) = -1*BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(7)
                                            bestFitParams.Phi(3,3) = BestX(count);
                                            bestFitParams.Phi(4,4) = BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(8)
                                            bestFitParams.Phi(5,5) = BestX(count);
                                            bestFitParams.Phi(6,6) = BestX(count);
                                            count = count + 1;
                                        end
                                        if FitIndices(9)
                                            bestFitParams.alpha = BestX(count);
                                        end

                                        % Save Trajectory
                                        eval(['alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                                            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                                            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                                            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                                            ' = OptimalReach(bestFitParams);']) 
                                        % Save Parameters
                                        eval(['alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                                            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                                            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                                            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                                            '.params = bestFitParams;'])
                                        % Save RMS Value
                                        eval(['alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                                            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                                            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                                            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                                            '.RMSError = val;'])
                                        % Save Flag Value
                                        eval(['alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
                                            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
                                            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
                                            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
                                            '.flag = Flags(in);'])
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

% % Plot Heat Map
gridAlpha = zeros(length(alphaFineVals),length(alphaVals));
for a = 1:length(alphaFineVals) 
    for b = 1:length(alphaVals)
        eval(['gridAlpha(a,b) = alphaFitted',num2str(a),'.alphaToFit',num2str(b),...
            '.Q11_',num2str(c),'.Q33_',num2str(d),'.Q5566_',...
            num2str(f),'.R1122_',num2str(g),'.Phi1122_',num2str(h)...
            '.Phi3344_',num2str(k),'.Phi5566_',num2str(l),...
            '.RMSError;'])
    end
end
