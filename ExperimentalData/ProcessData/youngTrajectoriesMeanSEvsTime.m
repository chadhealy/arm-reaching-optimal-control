% Pre Load Old Subect Data or run importSubjGroupDataWInterp


figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.Px,2)
    plot(yData.EN1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PxMean)-1,yData.EN1.PxMean)
ciplot(yData.EN1.PxMean-yData.EN1.PxSE,...
    yData.EN1.PxMean+yData.EN1.PxSE,...
    0:length(yData.EN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.Px,2)
    plot(yData.LN1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PxMean)-1,yData.LN1.PxMean)
ciplot(yData.LN1.PxMean-yData.LN1.PxSE,...
    yData.LN1.PxMean+yData.LN1.PxSE,...
    0:length(yData.LN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.Px,2)
    plot(yData.EF1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PxMean)-1,yData.EF1.PxMean)
ciplot(yData.EF1.PxMean-yData.EF1.PxSE,...
    yData.EF1.PxMean+yData.EF1.PxSE,...
    0:length(yData.EF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.Px,2)
    plot(yData.LF1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PxMean)-1,yData.LF1.PxMean)
ciplot(yData.LF1.PxMean-yData.LF1.PxSE,...
    yData.LF1.PxMean+yData.LF1.PxSE,...
    0:length(yData.LF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.Px,2)
    plot(yData.EF2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PxMean)-1,yData.EF2.PxMean)
ciplot(yData.EF2.PxMean-yData.EF2.PxSE,...
    yData.EF2.PxMean+yData.EF2.PxSE,...
    0:length(yData.EF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.Px,2)
    plot(yData.LF2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PxMean)-1,yData.LF2.PxMean)
ciplot(yData.LF2.PxMean-yData.LF2.PxSE,...
    yData.LF2.PxMean+yData.LF2.PxSE,...
    0:length(yData.LF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.Px,2)
    plot(yData.EN2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PxMean)-1,yData.EN2.PxMean)
ciplot(yData.EN2.PxMean-yData.EN2.PxSE,...
    yData.EN2.PxMean+yData.EN2.PxSE,...
    0:length(yData.EN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.Px,2)
    plot(yData.LN2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PxMean)-1,yData.LN2.PxMean)
ciplot(yData.LN2.PxMean-yData.LN2.PxSE,...
    yData.LN2.PxMean+yData.LN2.PxSE,...
    0:length(yData.LN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.Py,2)
    plot(yData.EN1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PyMean)-1,yData.EN1.PyMean)
ciplot(yData.EN1.PyMean-yData.EN1.PySE,...
    yData.EN1.PyMean+yData.EN1.PySE,...
    0:length(yData.EN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.Py,2)
    plot(yData.LN1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PyMean)-1,yData.LN1.PyMean)
ciplot(yData.LN1.PyMean-yData.LN1.PySE,...
    yData.LN1.PyMean+yData.LN1.PySE,...
    0:length(yData.LN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.Py,2)
    plot(yData.EF1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PyMean)-1,yData.EF1.PyMean)
ciplot(yData.EF1.PyMean-yData.EF1.PySE,...
    yData.EF1.PyMean+yData.EF1.PySE,...
    0:length(yData.EF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.Py,2)
    plot(yData.LF1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PyMean)-1,yData.LF1.PyMean)
ciplot(yData.LF1.PyMean-yData.LF1.PySE,...
    yData.LF1.PyMean+yData.LF1.PySE,...
    0:length(yData.LF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.Py,2)
    plot(yData.EF2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PyMean)-1,yData.EF2.PyMean)
ciplot(yData.EF2.PyMean-yData.EF2.PySE,...
    yData.EF2.PyMean+yData.EF2.PySE,...
    0:length(yData.EF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.Py,2)
    plot(yData.LF2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PyMean)-1,yData.LF2.PyMean)
ciplot(yData.LF2.PyMean-yData.LF2.PySE,...
    yData.LF2.PyMean+yData.LF2.PySE,...
    0:length(yData.LF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.Py,2)
    plot(yData.EN2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PyMean)-1,yData.EN2.PyMean)
ciplot(yData.EN2.PyMean-yData.EN2.PySE,...
    yData.EN2.PyMean+yData.EN2.PySE,...
    0:length(yData.EN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.Py,2)
    plot(yData.LN2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PyMean)-1,yData.LN2.PyMean)
ciplot(yData.LN2.PyMean-yData.LN2.PySE,...
    yData.LN2.PyMean+yData.LN2.PySE,...
    0:length(yData.LN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN2')

suptitle('Young Subject Data - Average Trajectories by Phase')

%% Plot with Time-Normalized Trajectories
% Pre Load Old Subect Data or run importSubjGroupDataWInterp

figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.normPx,2)
    plot(yData.EN1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PxMean)-1,yData.EN1.PxMean)
ciplot(yData.EN1.PxMean-yData.EN1.PxSE,...
    yData.EN1.PxMean+yData.EN1.PxSE,...
    0:length(yData.EN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.normPx,2)
    plot(yData.LN1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PxMean)-1,yData.LN1.PxMean)
ciplot(yData.LN1.PxMean-yData.LN1.PxSE,...
    yData.LN1.PxMean+yData.LN1.PxSE,...
    0:length(yData.LN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.normPx,2)
    plot(yData.EF1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PxMean)-1,yData.EF1.PxMean)
ciplot(yData.EF1.PxMean-yData.EF1.PxSE,...
    yData.EF1.PxMean+yData.EF1.PxSE,...
    0:length(yData.EF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.normPx,2)
    plot(yData.LF1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PxMean)-1,yData.LF1.PxMean)
ciplot(yData.LF1.PxMean-yData.LF1.PxSE,...
    yData.LF1.PxMean+yData.LF1.PxSE,...
    0:length(yData.LF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.normPx,2)
    plot(yData.EF2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PxMean)-1,yData.EF2.PxMean)
ciplot(yData.EF2.PxMean-yData.EF2.PxSE,...
    yData.EF2.PxMean+yData.EF2.PxSE,...
    0:length(yData.EF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.normPx,2)
    plot(yData.LF2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PxMean)-1,yData.LF2.PxMean)
ciplot(yData.LF2.PxMean-yData.LF2.PxSE,...
    yData.LF2.PxMean+yData.LF2.PxSE,...
    0:length(yData.LF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.normPx,2)
    plot(yData.EN2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PxMean)-1,yData.EN2.PxMean)
ciplot(yData.EN2.PxMean-yData.EN2.PxSE,...
    yData.EN2.PxMean+yData.EN2.PxSE,...
    0:length(yData.EN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.normPx,2)
    plot(yData.LN2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PxMean)-1,yData.LN2.PxMean)
ciplot(yData.LN2.PxMean-yData.LN2.PxSE,...
    yData.LN2.PxMean+yData.LN2.PxSE,...
    0:length(yData.LN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.normPx,2)
    plot(yData.EN1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PyMean)-1,yData.EN1.PyMean)
ciplot(yData.EN1.PyMean-yData.EN1.PySE,...
    yData.EN1.PyMean+yData.EN1.PySE,...
    0:length(yData.EN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.normPx,2)
    plot(yData.LN1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PyMean)-1,yData.LN1.PyMean)
ciplot(yData.LN1.PyMean-yData.LN1.PySE,...
    yData.LN1.PyMean+yData.LN1.PySE,...
    0:length(yData.LN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.normPx,2)
    plot(yData.EF1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PyMean)-1,yData.EF1.PyMean)
ciplot(yData.EF1.PyMean-yData.EF1.PySE,...
    yData.EF1.PyMean+yData.EF1.PySE,...
    0:length(yData.EF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.normPx,2)
    plot(yData.LF1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PyMean)-1,yData.LF1.PyMean)
ciplot(yData.LF1.PyMean-yData.LF1.PySE,...
    yData.LF1.PyMean+yData.LF1.PySE,...
    0:length(yData.LF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.normPx,2)
    plot(yData.EF2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PyMean)-1,yData.EF2.PyMean)
ciplot(yData.EF2.PyMean-yData.EF2.PySE,...
    yData.EF2.PyMean+yData.EF2.PySE,...
    0:length(yData.EF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.normPx,2)
    plot(yData.LF2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PyMean)-1,yData.LF2.PyMean)
ciplot(yData.LF2.PyMean-yData.LF2.PySE,...
    yData.LF2.PyMean+yData.LF2.PySE,...
    0:length(yData.LF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.normPx,2)
    plot(yData.EN2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PyMean)-1,yData.EN2.PyMean)
ciplot(yData.EN2.PyMean-yData.EN2.PySE,...
    yData.EN2.PyMean+yData.EN2.PySE,...
    0:length(yData.EN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.normPx,2)
    plot(yData.LN2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PyMean)-1,yData.LN2.PyMean)
ciplot(yData.LN2.PyMean-yData.LN2.PySE,...
    yData.LN2.PyMean+yData.LN2.PySE,...
    0:length(yData.LN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN2')

suptitle('Young Subject Data - Average Trajectories by Phase - Time-Normalized')