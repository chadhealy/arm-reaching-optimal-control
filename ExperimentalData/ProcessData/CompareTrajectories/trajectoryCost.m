function [J,Jcum] = trajectoryCost(Q,R,F,x,u)

% Q - tracking cost matrix
% R - control effort cost matrix
% F - terminal cost matrix
% x - trajectory (state) for size N x T
%     where N is the number of states, and T is the amount of time steps

% J - cost at each time step
% Jcum - cumulative cost of a trajectory

% Cost function
% J = 0.5*xf'*F*xf + 0.5*sum(x'*Q*x + u'*R*u)

% if find(size(x)==size(Q,1)) == 2
%     x = x';
% end

J = zeros(size(x,1),1);
for i = 1:size(x,1)-1
    J(i) = x(i,:)*Q*x(i,:)' + u(i,:)*R*u(i,:)';
end
J = 0.5*J;
J(end) = J(end) + 0.5*x(end,:)*F*x(end,:)';

Jcum = zeros(size(x,1),1);
Jcum = cumsum(J);