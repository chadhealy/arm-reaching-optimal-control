function SOL = OptimalReachSubjData(params)

% % Try to Stop for Warning
% lastwarn('');

x0 = params.x0;
trialLength = params.trialLength;
%dt = params.dt;
%A = params.A;
%Aest = params.Aest;
A = eye(8) + params.Asys*params.dt + params.curlGain*params.Fsys*params.dt;
Aest = eye(8) + params.Asys*params.dt + params.estGain*params.Fsys*params.dt;
B = params.B;
Q = params.Q;
R = params.R;
Phi = params.Phi;
%Po = params.Po;
 

%Terminal Conditions
S = zeros(8,8,trialLength);
S(:,:,trialLength) = Phi;
L = zeros(2,8,trialLength-1);

% Iterate Backwards
for k = trialLength-1:-1:1
    
    % Ricatti Equation
    S(:,:,k) = Q + Aest'*S(:,:,k+1)*Aest - ...
        (Aest'*S(:,:,k+1)*B)*inv(R + B'*S(:,:,k+1)*B)*(B'*S(:,:,k+1)*Aest);
    % Feedback Gain Matrix
    L(:,:,k) = inv(B'*S(:,:,k+1)*B + R)*B'*S(:,:,k+1)*Aest;

end

% Iterate Forward, Find Trajectory
x = zeros(trialLength,8);
x(1,:) = x0';
u = zeros(trialLength-1,2);
P = zeros(8,8,trialLength);

for k = 2:trialLength
    
    u(k-1,:) = -L(:,:,k-1)*x(k-1,:)';

    x(k,:) = ( A*x(k-1,:)' + B*u(k-1,:)' )';
    
    P(:,:,k) = A*P(:,:,k-1)*A';
    
end


SOL.x = x;
SOL.u = u;

% if ~isempty(lastwarn)
%     pause
% end


