%% Compare Trajectories
% - EF1 and LF1 are different trial lengths. Although the RMS error is normalized
% for the number of points, cost currently isnt. Additionally, this messes
% up the time scale comparison thing... I could resample the LF1 trajectory
% at the rate of EF1 [SEE PT 2]
%
% For now, I do not inlcude the min-jerk (perfectly straight trajectory),
% because I am unsure what cost parameters to use (Q,R,Phi...).  Instead, I
% ran the model with a set of cost parameters which produce a perfectly
% straight trajectory.  This is purely qualitative. 
% 
% With re-sampled LF1, we can compare RMS error values for both
% trajectories...

%% Calculated Necessary Data

% load('Young-AllPhasesIndivid-EarlyPts-ConstrGain0_25-20140722.mat')

% Calculate Arm Inertia
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;
Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];
% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));
% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));
% inertia matrix
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;
Inertia = [H11 H12; H12 H22];
Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);
%Estimate Acceleration
Young.EF1.Traj.accel = Young.EF1.Traj.x(:,5:6)*invMass;
Young.LF1.Traj.accel = Young.LF1.Traj.x(:,5:6)*invMass;

% Find Stright Trajectory
Params.x0 = [0;-0.1;0;0;0;0;0;0.1];
Params.trialLength = size(Young.EF1.Traj.x,1);
Params.dt = 1/200;
Params.curlGain = -20;
Params.estGain = -20;
Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
% Fsys(3:4,3:4) = curlGain/mass*[0 1;-1 0];
Fsys(3:4,3:4) = invMass*[0 1;-1 0];
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;
Params.Asys = Asys;
Params.B = Bsys*dt;
Params.Fsys = Fsys;
Params.Q = zeros(8);
Params.R = zeros(2);
Params.Phi = zeros(8);
%   R1122 Q5566 Q1177 Q2288 Q33 Phi1122 Phi3344 Phi5566
X = [1  0     1e2   0     0   1e8     1e8     1e4    ]; 
Params.R(1,1) = X(1);
Params.R(2,2) = X(1);
Params.Q(5,5) = X(2);
Params.Q(6,6) = X(2);
Params.Q(1,1) = X(3);
Params.Q(7,7) = X(3);
Params.Q(1,7) = -1*X(3);
Params.Q(7,1) = -1*X(3);
Params.Q(2,2) = X(4);
Params.Q(8,8) = X(4);
Params.Q(2,8) = -1*X(4);
Params.Q(8,2) = -1*X(4);
Params.Q(3,3) = X(5);
Params.Phi(1,1) = X(6);
Params.Phi(7,7) = X(6);
Params.Phi(1,7) = -1*X(6);
Params.Phi(7,1) = -1*X(6);
Params.Phi(2,2) = X(6);
Params.Phi(8,8) = X(6);
Params.Phi(2,8) = -1*X(6);
Params.Phi(8,2) = -1*X(6);
Params.Phi(3,3) = X(7);
Params.Phi(4,4) = X(7);
Params.Phi(5,5) = X(8);
Params.Phi(6,6) = X(8);
strReach.Params = Params;
strReach.Traj = OptimalReachSubjData(strReach.Params);

% Calculate Costs
[~,Jcum.EF1] = trajectoryCost(Young.EF1.Params.Q,Young.EF1.Params.R,Young.EF1.Params.Phi,Young.EF1.Traj.x,Young.EF1.Traj.u);
[~,Jcum.LF1] = trajectoryCost(Young.LF1.Params.Q,Young.LF1.Params.R,Young.LF1.Params.Phi,Young.LF1.Traj.x,Young.LF1.Traj.u);
[~,Jcum.strReach] = trajectoryCost(strReach.Params.Q,strReach.Params.R,strReach.Params.Phi,strReach.Traj.x,strReach.Traj.u);

% Calculate Error
for i = 1:length(yData.EF1.PxMean)
    RMS.EF1(i) = sum((yData.EF1.PxMean(1:i)' - Young.EF1.Traj.x(1:i,1)).^2) + ...
            sum((yData.EF1.PyMean(1:i)' - Young.EF1.Traj.x(1:i,2)).^2);
    RMScum.EF1(i) = sqrt(RMS.EF1(i)/2/i);
    
    RMS.strReach(i) = sum((yData.EF1.PxMean(1:i)' - strReach.Traj.x(1:i,1)).^2) + ...
            sum((yData.EF1.PyMean(1:i)' - strReach.Traj.x(1:i,2)).^2);
    RMScum.strReach(i) = sqrt(RMS.strReach(i)/2/i);
end



%% Build Figure
figure
% XY Trajectory
subplot(1.5,2,1)
hold on
h(1) = plot(yData.EF1.PxMean,yData.EF1.PyMean);
ciplotY(yData.EF1.PxMean - yData.EF1.PxSE,...
    yData.EF1.PxMean + yData.EF1.PxSE,yData.EF1.PyMean,'b',0.5)
h(2) = plot(Young.EF1.Traj.x(:,1),Young.EF1.Traj.x(:,2),'c');
h(3) = plot(Young.LF1.Traj.x(:,1),Young.LF1.Traj.x(:,2),'m');
h(4) = plot(strReach.Traj.x(:,1),strReach.Traj.x(:,2),'g');
axis equal
axis([-0.08 0.08 -.15 .15])
legend(h,'Data','EF1 Fit','LF1 Fit','Straight','Location','EastOutside')
title('XY-Trajectory')

% X-Pos vs. Time
subplot(6,2,2)
hold on
plot(0:dt:(length(yData.EF1.PxMean)-1)*dt,yData.EF1.PxMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,1))-1)*dt,Young.EF1.Traj.x(:,1),'c')
plot(0:dt:(length(Young.LF1.Traj.x(:,1))-1)*dt,Young.LF1.Traj.x(:,1),'m')
plot(0:dt:(length(strReach.Traj.x(:,1))-1)*dt,strReach.Traj.x(:,1),'g')
xlim([0 (length(yData.EF1.PxMean)-1)*dt])
set(gca,'XTickLabel',[])
title('X-Position')

% Y-Pos vs. Time
subplot(6,2,4)
hold on
plot(0:dt:(length(yData.EF1.PyMean)-1)*dt,yData.EF1.PyMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,2))-1)*dt,Young.EF1.Traj.x(:,2),'c')
plot(0:dt:(length(Young.LF1.Traj.x(:,2))-1)*dt,Young.LF1.Traj.x(:,2),'m')
plot(0:dt:(length(strReach.Traj.x(:,2))-1)*dt,strReach.Traj.x(:,2),'g')
xlim([0 (length(yData.EF1.PyMean)-1)*dt])
set(gca,'XTickLabel',[])
title('Y-Position')

% X-Vel vs. Time
subplot(6,2,6)
hold on
plot(0:dt:(length(yData.EF1.VxMean)-1)*dt,yData.EF1.VxMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,3))-1)*dt,Young.EF1.Traj.x(:,3),'c')
plot(0:dt:(length(Young.LF1.Traj.x(:,3))-1)*dt,Young.LF1.Traj.x(:,3),'m')
plot(0:dt:(length(strReach.Traj.x(:,3))-1)*dt,strReach.Traj.x(:,3),'g')
xlim([0 (length(yData.EF1.VxMean)-1)*dt])
set(gca,'XTickLabel',[])
title('X-Velocity')

% Y-Vel vs. Time
subplot(6,2,8)
hold on
plot(0:dt:(length(yData.EF1.VyMean)-1)*dt,yData.EF1.VyMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,4))-1)*dt,Young.EF1.Traj.x(:,4),'c')
plot(0:dt:(length(Young.LF1.Traj.x(:,4))-1)*dt,Young.LF1.Traj.x(:,4),'m')
plot(0:dt:(length(strReach.Traj.x(:,4))-1)*dt,strReach.Traj.x(:,4),'g')
xlim([0 (length(yData.EF1.VyMean)-1)*dt])
set(gca,'XTickLabel',[])
title('Y-Velocity')


% X-Acceleration vs. Time
subplot(6,2,10)
hold on
plot(dt:dt:(length(yData.EF1.VxMean)-1)*dt,(yData.EF1.VxMean(2:end)-yData.EF1.VxMean(1:end-1))/dt)
plot(0:dt:(length(Young.EF1.Traj.accel(:,1))-1)*dt,Young.EF1.Traj.accel(:,1),'c')
plot(0:dt:(length(Young.LF1.Traj.accel(:,1))-1)*dt,Young.LF1.Traj.accel(:,1),'m')
xlim([0 (length(yData.EF1.VxMean)-1)*dt])
set(gca,'XTickLabel',[])
title('X-Acceration')

% Y-Acceleration vs. Time
subplot(6,2,12)
hold on
plot(dt:dt:(length(yData.EF1.VyMean)-1)*dt,(yData.EF1.VyMean(2:end)-yData.EF1.VyMean(1:end-1))/dt)
% plot(0:dt:(length(Young.EF1.Traj.accel(:,2))-1)*dt,Young.EF1.Traj.accel(:,2),'c')
% plot(0:dt:(length(Young.LF1.Traj.accel(:,2))-1)*dt,Young.LF1.Traj.accel(:,2),'m')
plot(dt:dt:(length(Young.EF1.Traj.x(:,4))-1)*dt,(Young.EF1.Traj.x(2:end,4)-Young.EF1.Traj.x(1:end-1,4))/dt,'c')
plot(dt:dt:(length(Young.LF1.Traj.x(:,4))-1)*dt,(Young.LF1.Traj.x(2:end,4)-Young.LF1.Traj.x(1:end-1,4))/dt,'m')
xlim([0 (length(yData.EF1.VyMean)-1)*dt])
title('Y-Acceration')

% Cost vs. Time [Green cost is too low to see... ~1e-8]
subplot(6,2,9)
hold on
plot(0:dt:(length(Jcum.EF1)-1)*dt,Jcum.EF1,'c')
plot(0:dt:(length(Jcum.LF1)-1)*dt,Jcum.LF1,'m')
plot(0:dt:(length(Jcum.strReach)-1)*dt,Jcum.strReach,'g')
xlim([0 (length(Jcum.EF1)-1)*dt])
set(gca,'XTickLabel',[])
title('Cost')

%RMS Error vs. Time (Cumulative)
subplot(6,2,11)
hold on
plot(0:dt:(length(Young.EF1.Traj.x(:,1))-1)*dt,RMScum.EF1,'c')
% plot(0:dt:(length(Young.LF1.Traj.x(:,1))-1)*dt,RMS.LF1,'m')
plot(0:dt:(length(strReach.x(:,1))-1)*dt,RMScum.strReach,'g')
title('RMS Error')

%% Resample LF1
% So the data vs. time is more comprable, and an RMS value can be
% calculated
%
% Includes Straight Trajectory
% Plot New Figure

Young.LF1.Traj.xReSampled = zeros(size(Young.EF1.Traj.x,1),8);
for i = 1:8
    Young.LF1.Traj.xReSampled(:,i) = resample(Young.LF1.Traj.x(:,i),size(Young.EF1.Traj.x,1),size(Young.LF1.Traj.x,1),0);
end
for i = 1:2
    Young.LF1.Traj.uReSampled(:,i) = resample(Young.LF1.Traj.u(:,i),size(Young.EF1.Traj.u,1),size(Young.LF1.Traj.u,1),0);
end
%Calculate Error
for i = 1:length(yData.EF1.PxMean)
    RMS.LF1(i) = sum((yData.EF1.PxMean(1:i)' - Young.LF1.Traj.xReSampled(1:i,1)).^2) + ...
            sum((yData.EF1.PyMean(1:i)' - Young.LF1.Traj.xReSampled(1:i,2)).^2);
    RMScum.LF1(i) = sqrt(RMS.LF1(i)/2/i);
end

% Re-Calculate Acceleration
Young.LF1.Traj.accelReSamp = Young.LF1.Traj.xReSampled(:,5:6)*invMass;
% Re-Calculate Cost
[~,Jcum.LF1ReSamp] = trajectoryCost(Young.LF1.Params.Q,Young.LF1.Params.R,Young.LF1.Params.Phi,Young.LF1.Traj.xReSampled,Young.LF1.Traj.uReSampled);

%Replot Figure
 figure
% XY Trajectory
subplot(1.5,2,1)
hold on
h(1) = plot(yData.EF1.PxMean,yData.EF1.PyMean);
ciplotY(yData.EF1.PxMean - yData.EF1.PxSE,...
    yData.EF1.PxMean + yData.EF1.PxSE,yData.EF1.PyMean,'b',0.5)
h(2) = plot(Young.EF1.Traj.x(:,1),Young.EF1.Traj.x(:,2),'c');
h(3) = plot(Young.LF1.Traj.xReSampled(:,1),Young.LF1.Traj.xReSampled(:,2),'m');
h(4) = plot(strReach.Traj.x(:,1),strReach.Traj.x(:,2),'g');
axis equal
axis([-0.08 0.08 -.15 .15])
legend(h,'Data','EF1 Fit','LF1 Fit','Straight','Location','EastOutside')
title('XY-Trajectory')

% X-Pos vs. Time
subplot(6,2,2)
hold on
plot(0:dt:(length(yData.EF1.PxMean)-1)*dt,yData.EF1.PxMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,1))-1)*dt,Young.EF1.Traj.x(:,1),'c')
plot(0:dt:(length(Young.LF1.Traj.xReSampled(:,1))-1)*dt,Young.LF1.Traj.xReSampled(:,1),'m')
plot(0:dt:(length(strReach.Traj.x(:,1))-1)*dt,strReach.Traj.x(:,1),'g')
xlim([0 (length(yData.EF1.PxMean)-1)*dt])
set(gca,'XTickLabel',[])
title('X-Position')

% Y-Pos vs. Time
subplot(6,2,4)
hold on
plot(0:dt:(length(yData.EF1.PyMean)-1)*dt,yData.EF1.PyMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,2))-1)*dt,Young.EF1.Traj.x(:,2),'c')
plot(0:dt:(length(Young.LF1.Traj.xReSampled(:,2))-1)*dt,Young.LF1.Traj.xReSampled(:,2),'m')
plot(0:dt:(length(strReach.Traj.x(:,2))-1)*dt,strReach.Traj.x(:,2),'g')
xlim([0 (length(yData.EF1.PyMean)-1)*dt])
set(gca,'XTickLabel',[])
title('Y-Position')

% X-Vel vs. Time
subplot(6,2,6)
hold on
plot(0:dt:(length(yData.EF1.VxMean)-1)*dt,yData.EF1.VxMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,3))-1)*dt,Young.EF1.Traj.x(:,3),'c')
plot(0:dt:(length(Young.LF1.Traj.xReSampled(:,3))-1)*dt,Young.LF1.Traj.xReSampled(:,3),'m')
plot(0:dt:(length(strReach.Traj.x(:,3))-1)*dt,strReach.Traj.x(:,3),'g')
xlim([0 (length(yData.EF1.VxMean)-1)*dt])
set(gca,'XTickLabel',[])
title('X-Velocity')

% Y-Vel vs. Time
subplot(6,2,8)
hold on
plot(0:dt:(length(yData.EF1.VyMean)-1)*dt,yData.EF1.VyMean)
plot(0:dt:(length(Young.EF1.Traj.x(:,4))-1)*dt,Young.EF1.Traj.x(:,4),'c')
plot(0:dt:(length(Young.LF1.Traj.xReSampled(:,4))-1)*dt,Young.LF1.Traj.xReSampled(:,4),'m')
plot(0:dt:(length(strReach.Traj.x(:,4))-1)*dt,strReach.Traj.x(:,4),'g')
xlim([0 (length(yData.EF1.VyMean)-1)*dt])
set(gca,'XTickLabel',[])
title('Y-Velocity')


% X-Acceleration vs. Time
subplot(6,2,10)
hold on
plot(dt:dt:(length(yData.EF1.VxMean)-1)*dt,(yData.EF1.VxMean(2:end)-yData.EF1.VxMean(1:end-1))/dt)
plot(0:dt:(length(Young.EF1.Traj.accel(:,1))-1)*dt,Young.EF1.Traj.accel(:,1),'c')
plot(0:dt:(length(Young.LF1.Traj.accelReSamp(:,1))-1)*dt,Young.LF1.Traj.accelReSamp(:,1),'m')
xlim([0 (length(yData.EF1.VxMean)-1)*dt])
set(gca,'XTickLabel',[])
title('X-Acceration')

% Y-Acceleration vs. Time
subplot(6,2,12)
hold on
plot(dt:dt:(length(yData.EF1.VyMean)-1)*dt,(yData.EF1.VyMean(2:end)-yData.EF1.VyMean(1:end-1))/dt)
% plot(0:dt:(length(Young.EF1.Traj.accel(:,2))-1)*dt,Young.EF1.Traj.accel(:,2),'c')
% plot(0:dt:(length(Young.LF1.Traj.accelReSamp(:,2))-1)*dt,Young.LF1.Traj.accel(:,2),'m')
plot(dt:dt:(length(Young.EF1.Traj.x(:,4))-1)*dt,(Young.EF1.Traj.x(2:end,4)-Young.EF1.Traj.x(1:end-1,4))/dt,'c')
plot(dt:dt:(length(Young.LF1.Traj.xReSampled(:,4))-1)*dt,(Young.LF1.Traj.xReSampled(2:end,4)-Young.LF1.Traj.xReSampled(1:end-1,4))/dt,'m')
xlim([0 (length(yData.EF1.VyMean)-1)*dt])
title('Y-Acceration')

% Cost vs. Time [Green cost is too low to see... ~1e-8]
subplot(6,2,9)
hold on
plot(0:dt:(length(Jcum.EF1)-1)*dt,Jcum.EF1,'c')
plot(0:dt:(length(Jcum.LF1ReSamp)-1)*dt,Jcum.LF1ReSamp,'m')
plot(0:dt:(length(Jcum.strReach)-1)*dt,Jcum.strReach,'g')
xlim([0 (length(Jcum.EF1)-1)*dt])
set(gca,'XTickLabel',[])
title('Cost')

%RMS Error vs. Time (Cumulative)
subplot(6,2,11)
hold on
plot(0:dt:(length(Young.EF1.Traj.x(:,1))-1)*dt,RMScum.EF1,'c')
plot(0:dt:(length(Young.LF1.Traj.xReSampled(:,1))-1)*dt,RMScum.LF1,'m')
plot(0:dt:(length(strReach.x(:,1))-1)*dt,RMScum.strReach,'g')
title('RMS Error')

suptitle('Normalized Comparison of EN1 Solution, LN1 Solution and Straight Trajectory')

%% Conclusions
% So the RMS Error for LN1 is less than the solution found for EN1. 
% BUT! This EN1 is constrained to zero gain, and LN1 is not. So is this bit
% of realism causing the soltuion to be poor? Does this mean that the model
% sucks??
