%% Calculate Non-Time Normalized Mean Trajectories
% (from previously processed data)

load('YoungData-ToFit-WAccel-20140905.mat')

phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};

for ii = 1:length(phaseNames)
    eval(['maxTrialLength = size(yData.',phaseNames{ii},'.Px,1);']);
    eval(['nTrials = size(yData.',phaseNames{ii},'.Px,2);'])
    eval(['yData.',phaseNames{ii},'.PxMeanNonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.PyMeanNonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.VxMeanNonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.VyMeanNonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.AxMeanNonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.AyMeanNonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.PxSENonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.PySENonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.VxSENonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.VySENonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.AxSENonNorm = zeros(1,maxTrialLength);']);
    eval(['yData.',phaseNames{ii},'.AySENonNorm = zeros(1,maxTrialLength);']);
    for jj = 1:maxTrialLength
        eval(['yData.',phaseNames{ii},'.PxMeanNonNorm(jj) = nanmean(yData.',phaseNames{ii},'.Px(jj,:));'])
        eval(['yData.',phaseNames{ii},'.PyMeanNonNorm(jj) = nanmean(yData.',phaseNames{ii},'.Py(jj,:));'])
        eval(['yData.',phaseNames{ii},'.VxMeanNonNorm(jj) = nanmean(yData.',phaseNames{ii},'.Vx(jj,:));'])
        eval(['yData.',phaseNames{ii},'.VyMeanNonNorm(jj) = nanmean(yData.',phaseNames{ii},'.Vy(jj,:));'])
        eval(['yData.',phaseNames{ii},'.AxMeanNonNorm(jj) = nanmean(yData.',phaseNames{ii},'.Ax(jj,:));'])
        eval(['yData.',phaseNames{ii},'.AyMeanNonNorm(jj) = nanmean(yData.',phaseNames{ii},'.Ay(jj,:));'])
        
        % Commented Out to Inspect STD instead of SE
        eval(['yData.',phaseNames{ii},'.PxSENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Px(jj,:))/sqrt(nTrials);'])
        eval(['yData.',phaseNames{ii},'.PySENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Py(jj,:))/sqrt(nTrials);'])
        eval(['yData.',phaseNames{ii},'.VxSENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Vx(jj,:))/sqrt(nTrials);'])
        eval(['yData.',phaseNames{ii},'.VySENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Vy(jj,:))/sqrt(nTrials);'])
        eval(['yData.',phaseNames{ii},'.AxSENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Ax(jj,:))/sqrt(nTrials);'])
        eval(['yData.',phaseNames{ii},'.AySENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Ay(jj,:))/sqrt(nTrials);'])
%         eval(['yData.',phaseNames{ii},'.PxSENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Px(jj,:));'])
%         eval(['yData.',phaseNames{ii},'.PySENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Py(jj,:));'])
%         eval(['yData.',phaseNames{ii},'.VxSENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Vx(jj,:));'])
%         eval(['yData.',phaseNames{ii},'.VySENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Vy(jj,:));'])
%         eval(['yData.',phaseNames{ii},'.AxSENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Ax(jj,:));'])
%         eval(['yData.',phaseNames{ii},'.AySENonNorm(jj) = nanstd(yData.',phaseNames{ii},'.Ay(jj,:));'])
    end
    
    % EDIT 09/18/2014, truncate to mean length.
    eval(['meanLength =  length(yData.',phaseNames{ii},'.PxMean);']);
    eval(['yData.',phaseNames{ii},'.PxMeanNonNorm = yData.',phaseNames{ii},'.PxMeanNonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.PyMeanNonNorm = yData.',phaseNames{ii},'.PyMeanNonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.VxMeanNonNorm = yData.',phaseNames{ii},'.VxMeanNonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.VyMeanNonNorm = yData.',phaseNames{ii},'.VyMeanNonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.AxMeanNonNorm = yData.',phaseNames{ii},'.AxMeanNonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.AyMeanNonNorm = yData.',phaseNames{ii},'.AyMeanNonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.PxSENonNorm = yData.',phaseNames{ii},'.PxSENonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.PySENonNorm = yData.',phaseNames{ii},'.PySENonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.VxSENonNorm = yData.',phaseNames{ii},'.VxSENonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.VySENonNorm = yData.',phaseNames{ii},'.VySENonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.AxSENonNorm = yData.',phaseNames{ii},'.AxSENonNorm(1:meanLength);']);
    eval(['yData.',phaseNames{ii},'.AySENonNorm = yData.',phaseNames{ii},'.AySENonNorm(1:meanLength);']);
    
end

% Plot XY-Trajectories, by Phase
figure
subplot(2,4,1)
hold on
for ii = 1:length(yData.EN1.PxMeanNonNorm)
    xSE_Sq = [yData.EN1.PxMeanNonNorm(ii) - yData.EN1.PxSENonNorm(ii);
              yData.EN1.PxMeanNonNorm(ii) + yData.EN1.PxSENonNorm(ii);
              yData.EN1.PxMeanNonNorm(ii) + yData.EN1.PxSENonNorm(ii);
              yData.EN1.PxMeanNonNorm(ii) - yData.EN1.PxSENonNorm(ii);
              yData.EN1.PxMeanNonNorm(ii) - yData.EN1.PxSENonNorm(ii)];
    ySE_Sq = [yData.EN1.PyMeanNonNorm(ii) - yData.EN1.PySENonNorm(ii);
              yData.EN1.PyMeanNonNorm(ii) - yData.EN1.PySENonNorm(ii);
              yData.EN1.PyMeanNonNorm(ii) + yData.EN1.PySENonNorm(ii);
              yData.EN1.PyMeanNonNorm(ii) + yData.EN1.PySENonNorm(ii);
              yData.EN1.PyMeanNonNorm(ii) - yData.EN1.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EN1.PxMeanNonNorm,yData.EN1.PyMeanNonNorm)
% ciplotY((yData.EN1.PxMeanNonNorm - yData.EN1.PxSENonNorm),...
%         (yData.EN1.PxMeanNonNorm + yData.EN1.PxSENonNorm),...    
%         yData.EN1.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('EN1')

subplot(2,4,5)
hold on
for ii = 1:length(yData.LN1.PxMeanNonNorm)
    xSE_Sq = [yData.LN1.PxMeanNonNorm(ii) - yData.LN1.PxSENonNorm(ii);
              yData.LN1.PxMeanNonNorm(ii) + yData.LN1.PxSENonNorm(ii);
              yData.LN1.PxMeanNonNorm(ii) + yData.LN1.PxSENonNorm(ii);
              yData.LN1.PxMeanNonNorm(ii) - yData.LN1.PxSENonNorm(ii);
              yData.LN1.PxMeanNonNorm(ii) - yData.LN1.PxSENonNorm(ii)];
    ySE_Sq = [yData.LN1.PyMeanNonNorm(ii) - yData.LN1.PySENonNorm(ii);
              yData.LN1.PyMeanNonNorm(ii) - yData.LN1.PySENonNorm(ii);
              yData.LN1.PyMeanNonNorm(ii) + yData.LN1.PySENonNorm(ii);
              yData.LN1.PyMeanNonNorm(ii) + yData.LN1.PySENonNorm(ii);
              yData.LN1.PyMeanNonNorm(ii) - yData.LN1.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LN1.PxMeanNonNorm,yData.LN1.PyMeanNonNorm)
% ciplotY((yData.LN1.PxMeanNonNorm - yData.LN1.PxSENonNorm),...
%         (yData.LN1.PxMeanNonNorm + yData.LN1.PxSENonNorm),...    
%         yData.LN1.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('LN1')

subplot(2,4,2)
hold on
for ii = 1:length(yData.EF1.PxMeanNonNorm)
    xSE_Sq = [yData.EF1.PxMeanNonNorm(ii) - yData.EF1.PxSENonNorm(ii);
              yData.EF1.PxMeanNonNorm(ii) + yData.EF1.PxSENonNorm(ii);
              yData.EF1.PxMeanNonNorm(ii) + yData.EF1.PxSENonNorm(ii);
              yData.EF1.PxMeanNonNorm(ii) - yData.EF1.PxSENonNorm(ii);
              yData.EF1.PxMeanNonNorm(ii) - yData.EF1.PxSENonNorm(ii)];
    ySE_Sq = [yData.EF1.PyMeanNonNorm(ii) - yData.EF1.PySENonNorm(ii);
              yData.EF1.PyMeanNonNorm(ii) - yData.EF1.PySENonNorm(ii);
              yData.EF1.PyMeanNonNorm(ii) + yData.EF1.PySENonNorm(ii);
              yData.EF1.PyMeanNonNorm(ii) + yData.EF1.PySENonNorm(ii);
              yData.EF1.PyMeanNonNorm(ii) - yData.EF1.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EF1.PxMeanNonNorm,yData.EF1.PyMeanNonNorm)
% ciplotY((yData.EF1.PxMeanNonNorm - yData.EF1.PxSENonNorm),...
%         (yData.EF1.PxMeanNonNorm + yData.EF1.PxSENonNorm),...    
%         yData.EF1.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('EF1')

subplot(2,4,6)
hold on
for ii = 1:length(yData.LF1.PxMeanNonNorm)
    xSE_Sq = [yData.LF1.PxMeanNonNorm(ii) - yData.LF1.PxSENonNorm(ii);
              yData.LF1.PxMeanNonNorm(ii) + yData.LF1.PxSENonNorm(ii);
              yData.LF1.PxMeanNonNorm(ii) + yData.LF1.PxSENonNorm(ii);
              yData.LF1.PxMeanNonNorm(ii) - yData.LF1.PxSENonNorm(ii);
              yData.LF1.PxMeanNonNorm(ii) - yData.LF1.PxSENonNorm(ii)];
    ySE_Sq = [yData.LF1.PyMeanNonNorm(ii) - yData.LF1.PySENonNorm(ii);
              yData.LF1.PyMeanNonNorm(ii) - yData.LF1.PySENonNorm(ii);
              yData.LF1.PyMeanNonNorm(ii) + yData.LF1.PySENonNorm(ii);
              yData.LF1.PyMeanNonNorm(ii) + yData.LF1.PySENonNorm(ii);
              yData.LF1.PyMeanNonNorm(ii) - yData.LF1.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LF1.PxMeanNonNorm,yData.LF1.PyMeanNonNorm)
% ciplotY((yData.LF1.PxMeanNonNorm - yData.LF1.PxSENonNorm),...
%         (yData.LF1.PxMeanNonNorm + yData.LF1.PxSENonNorm),...    
%         yData.LF1.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('LF1')

subplot(2,4,3)
hold on
for ii = 1:length(yData.EF2.PxMeanNonNorm)
    xSE_Sq = [yData.EF2.PxMeanNonNorm(ii) - yData.EF2.PxSENonNorm(ii);
              yData.EF2.PxMeanNonNorm(ii) + yData.EF2.PxSENonNorm(ii);
              yData.EF2.PxMeanNonNorm(ii) + yData.EF2.PxSENonNorm(ii);
              yData.EF2.PxMeanNonNorm(ii) - yData.EF2.PxSENonNorm(ii);
              yData.EF2.PxMeanNonNorm(ii) - yData.EF2.PxSENonNorm(ii)];
    ySE_Sq = [yData.EF2.PyMeanNonNorm(ii) - yData.EF2.PySENonNorm(ii);
              yData.EF2.PyMeanNonNorm(ii) - yData.EF2.PySENonNorm(ii);
              yData.EF2.PyMeanNonNorm(ii) + yData.EF2.PySENonNorm(ii);
              yData.EF2.PyMeanNonNorm(ii) + yData.EF2.PySENonNorm(ii);
              yData.EF2.PyMeanNonNorm(ii) - yData.EF2.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EF2.PxMeanNonNorm,yData.EF2.PyMeanNonNorm)
% ciplotY((yData.EF2.PxMeanNonNorm - yData.EF2.PxSENonNorm),...
%         (yData.EF2.PxMeanNonNorm + yData.EF2.PxSENonNorm),...    
%         yData.EF2.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('EF2')

subplot(2,4,7)
hold on
for ii = 1:length(yData.LF2.PxMeanNonNorm)
    xSE_Sq = [yData.LF2.PxMeanNonNorm(ii) - yData.LF2.PxSENonNorm(ii);
              yData.LF2.PxMeanNonNorm(ii) + yData.LF2.PxSENonNorm(ii);
              yData.LF2.PxMeanNonNorm(ii) + yData.LF2.PxSENonNorm(ii);
              yData.LF2.PxMeanNonNorm(ii) - yData.LF2.PxSENonNorm(ii);
              yData.LF2.PxMeanNonNorm(ii) - yData.LF2.PxSENonNorm(ii)];
    ySE_Sq = [yData.LF2.PyMeanNonNorm(ii) - yData.LF2.PySENonNorm(ii);
              yData.LF2.PyMeanNonNorm(ii) - yData.LF2.PySENonNorm(ii);
              yData.LF2.PyMeanNonNorm(ii) + yData.LF2.PySENonNorm(ii);
              yData.LF2.PyMeanNonNorm(ii) + yData.LF2.PySENonNorm(ii);
              yData.LF2.PyMeanNonNorm(ii) - yData.LF2.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LF2.PxMeanNonNorm,yData.LF2.PyMeanNonNorm)
% ciplotY((yData.LF2.PxMeanNonNorm - yData.LF2.PxSENonNorm),...
%         (yData.LF2.PxMeanNonNorm + yData.LF2.PxSENonNorm),...    
%         yData.LF2.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('LF2')

subplot(2,4,4)
hold on
for ii = 1:length(yData.EN2.PxMeanNonNorm)
    xSE_Sq = [yData.EN2.PxMeanNonNorm(ii) - yData.EN2.PxSENonNorm(ii);
              yData.EN2.PxMeanNonNorm(ii) + yData.EN2.PxSENonNorm(ii);
              yData.EN2.PxMeanNonNorm(ii) + yData.EN2.PxSENonNorm(ii);
              yData.EN2.PxMeanNonNorm(ii) - yData.EN2.PxSENonNorm(ii);
              yData.EN2.PxMeanNonNorm(ii) - yData.EN2.PxSENonNorm(ii)];
    ySE_Sq = [yData.EN2.PyMeanNonNorm(ii) - yData.EN2.PySENonNorm(ii);
              yData.EN2.PyMeanNonNorm(ii) - yData.EN2.PySENonNorm(ii);
              yData.EN2.PyMeanNonNorm(ii) + yData.EN2.PySENonNorm(ii);
              yData.EN2.PyMeanNonNorm(ii) + yData.EN2.PySENonNorm(ii);
              yData.EN2.PyMeanNonNorm(ii) - yData.EN2.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EN2.PxMeanNonNorm,yData.EN2.PyMeanNonNorm)
% ciplotY((yData.EN2.PxMeanNonNorm - yData.EN2.PxSENonNorm),...
%         (yData.EN2.PxMeanNonNorm + yData.EN2.PxSENonNorm),...    
%         yData.EN2.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('EN2')

subplot(2,4,8)
hold on
for ii = 1:length(yData.LN2.PxMeanNonNorm)
    xSE_Sq = [yData.LN2.PxMeanNonNorm(ii) - yData.LN2.PxSENonNorm(ii);
              yData.LN2.PxMeanNonNorm(ii) + yData.LN2.PxSENonNorm(ii);
              yData.LN2.PxMeanNonNorm(ii) + yData.LN2.PxSENonNorm(ii);
              yData.LN2.PxMeanNonNorm(ii) - yData.LN2.PxSENonNorm(ii);
              yData.LN2.PxMeanNonNorm(ii) - yData.LN2.PxSENonNorm(ii)];
    ySE_Sq = [yData.LN2.PyMeanNonNorm(ii) - yData.LN2.PySENonNorm(ii);
              yData.LN2.PyMeanNonNorm(ii) - yData.LN2.PySENonNorm(ii);
              yData.LN2.PyMeanNonNorm(ii) + yData.LN2.PySENonNorm(ii);
              yData.LN2.PyMeanNonNorm(ii) + yData.LN2.PySENonNorm(ii);
              yData.LN2.PyMeanNonNorm(ii) - yData.LN2.PySENonNorm(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LN2.PxMeanNonNorm,yData.LN2.PyMeanNonNorm)
% ciplotY((yData.LN2.PxMeanNonNorm - yData.LN2.PxSENonNorm),...
%         (yData.LN2.PxMeanNonNorm + yData.LN2.PxSENonNorm),...    
%         yData.LN2.PyMeanNonNorm,'b',0.5)
xlim([-0.08 0.08]),ylim([-0.15 0.15])
axis equal
title('LN2')

%% Plot NonTime Normalized Position VS Time

figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.Px,2)
    plot(yData.EN1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PxMeanNonNorm)-1,yData.EN1.PxMeanNonNorm)
ciplot(yData.EN1.PxMeanNonNorm-yData.EN1.PxSE,...
    yData.EN1.PxMeanNonNorm+yData.EN1.PxSE,...
    0:length(yData.EN1.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.Px,2)
    plot(yData.LN1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PxMeanNonNorm)-1,yData.LN1.PxMeanNonNorm)
ciplot(yData.LN1.PxMeanNonNorm-yData.LN1.PxSE,...
    yData.LN1.PxMeanNonNorm+yData.LN1.PxSE,...
    0:length(yData.LN1.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.Px,2)
    plot(yData.EF1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PxMeanNonNorm)-1,yData.EF1.PxMeanNonNorm)
ciplot(yData.EF1.PxMeanNonNorm-yData.EF1.PxSE,...
    yData.EF1.PxMeanNonNorm+yData.EF1.PxSE,...
    0:length(yData.EF1.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.Px,2)
    plot(yData.LF1.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PxMeanNonNorm)-1,yData.LF1.PxMeanNonNorm)
ciplot(yData.LF1.PxMeanNonNorm-yData.LF1.PxSE,...
    yData.LF1.PxMeanNonNorm+yData.LF1.PxSE,...
    0:length(yData.LF1.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.Px,2)
    plot(yData.EF2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PxMeanNonNorm)-1,yData.EF2.PxMeanNonNorm)
ciplot(yData.EF2.PxMeanNonNorm-yData.EF2.PxSE,...
    yData.EF2.PxMeanNonNorm+yData.EF2.PxSE,...
    0:length(yData.EF2.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.Px,2)
    plot(yData.LF2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PxMeanNonNorm)-1,yData.LF2.PxMeanNonNorm)
ciplot(yData.LF2.PxMeanNonNorm-yData.LF2.PxSE,...
    yData.LF2.PxMeanNonNorm+yData.LF2.PxSE,...
    0:length(yData.LF2.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.Px,2)
    plot(yData.EN2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PxMeanNonNorm)-1,yData.EN2.PxMeanNonNorm)
ciplot(yData.EN2.PxMeanNonNorm-yData.EN2.PxSE,...
    yData.EN2.PxMeanNonNorm+yData.EN2.PxSE,...
    0:length(yData.EN2.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.Px,2)
    plot(yData.LN2.Px(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PxMeanNonNorm)-1,yData.LN2.PxMeanNonNorm)
ciplot(yData.LN2.PxMeanNonNorm-yData.LN2.PxSE,...
    yData.LN2.PxMeanNonNorm+yData.LN2.PxSE,...
    0:length(yData.LN2.PxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.Py,2)
    plot(yData.EN1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PyMeanNonNorm)-1,yData.EN1.PyMeanNonNorm)
ciplot(yData.EN1.PyMeanNonNorm-yData.EN1.PySE,...
    yData.EN1.PyMeanNonNorm+yData.EN1.PySE,...
    0:length(yData.EN1.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.Py,2)
    plot(yData.LN1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PyMeanNonNorm)-1,yData.LN1.PyMeanNonNorm)
ciplot(yData.LN1.PyMeanNonNorm-yData.LN1.PySE,...
    yData.LN1.PyMeanNonNorm+yData.LN1.PySE,...
    0:length(yData.LN1.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.Py,2)
    plot(yData.EF1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PyMeanNonNorm)-1,yData.EF1.PyMeanNonNorm)
ciplot(yData.EF1.PyMeanNonNorm-yData.EF1.PySE,...
    yData.EF1.PyMeanNonNorm+yData.EF1.PySE,...
    0:length(yData.EF1.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.Py,2)
    plot(yData.LF1.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PyMeanNonNorm)-1,yData.LF1.PyMeanNonNorm)
ciplot(yData.LF1.PyMeanNonNorm-yData.LF1.PySE,...
    yData.LF1.PyMeanNonNorm+yData.LF1.PySE,...
    0:length(yData.LF1.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.Py,2)
    plot(yData.EF2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PyMeanNonNorm)-1,yData.EF2.PyMeanNonNorm)
ciplot(yData.EF2.PyMeanNonNorm-yData.EF2.PySE,...
    yData.EF2.PyMeanNonNorm+yData.EF2.PySE,...
    0:length(yData.EF2.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.Py,2)
    plot(yData.LF2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PyMeanNonNorm)-1,yData.LF2.PyMeanNonNorm)
ciplot(yData.LF2.PyMeanNonNorm-yData.LF2.PySE,...
    yData.LF2.PyMeanNonNorm+yData.LF2.PySE,...
    0:length(yData.LF2.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.Py,2)
    plot(yData.EN2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PyMeanNonNorm)-1,yData.EN2.PyMeanNonNorm)
ciplot(yData.EN2.PyMeanNonNorm-yData.EN2.PySE,...
    yData.EN2.PyMeanNonNorm+yData.EN2.PySE,...
    0:length(yData.EN2.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.Py,2)
    plot(yData.LN2.Py(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PyMeanNonNorm)-1,yData.LN2.PyMeanNonNorm)
ciplot(yData.LN2.PyMeanNonNorm-yData.LN2.PySE,...
    yData.LN2.PyMeanNonNorm+yData.LN2.PySE,...
    0:length(yData.LN2.PyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN2')

% suptitle('Young Subject Data - Average Trajectories by Phase')

%% Plot with Time-Normalized Trajectories
% Pre Load Old Subect Data or run importSubjGroupDataWInterp

figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.normPx,2)
    plot(yData.EN1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PxMean)-1,yData.EN1.PxMean)
ciplot(yData.EN1.PxMean-yData.EN1.PxSE,...
    yData.EN1.PxMean+yData.EN1.PxSE,...
    0:length(yData.EN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.normPx,2)
    plot(yData.LN1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PxMean)-1,yData.LN1.PxMean)
ciplot(yData.LN1.PxMean-yData.LN1.PxSE,...
    yData.LN1.PxMean+yData.LN1.PxSE,...
    0:length(yData.LN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.normPx,2)
    plot(yData.EF1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PxMean)-1,yData.EF1.PxMean)
ciplot(yData.EF1.PxMean-yData.EF1.PxSE,...
    yData.EF1.PxMean+yData.EF1.PxSE,...
    0:length(yData.EF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.normPx,2)
    plot(yData.LF1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PxMean)-1,yData.LF1.PxMean)
ciplot(yData.LF1.PxMean-yData.LF1.PxSE,...
    yData.LF1.PxMean+yData.LF1.PxSE,...
    0:length(yData.LF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.normPx,2)
    plot(yData.EF2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PxMean)-1,yData.EF2.PxMean)
ciplot(yData.EF2.PxMean-yData.EF2.PxSE,...
    yData.EF2.PxMean+yData.EF2.PxSE,...
    0:length(yData.EF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.normPx,2)
    plot(yData.LF2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PxMean)-1,yData.LF2.PxMean)
ciplot(yData.LF2.PxMean-yData.LF2.PxSE,...
    yData.LF2.PxMean+yData.LF2.PxSE,...
    0:length(yData.LF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.normPx,2)
    plot(yData.EN2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PxMean)-1,yData.EN2.PxMean)
ciplot(yData.EN2.PxMean-yData.EN2.PxSE,...
    yData.EN2.PxMean+yData.EN2.PxSE,...
    0:length(yData.EN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.normPx,2)
    plot(yData.LN2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PxMean)-1,yData.LN2.PxMean)
ciplot(yData.LN2.PxMean-yData.LN2.PxSE,...
    yData.LN2.PxMean+yData.LN2.PxSE,...
    0:length(yData.LN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.normPx,2)
    plot(yData.EN1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.PyMean)-1,yData.EN1.PyMean)
ciplot(yData.EN1.PyMean-yData.EN1.PySE,...
    yData.EN1.PyMean+yData.EN1.PySE,...
    0:length(yData.EN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.normPx,2)
    plot(yData.LN1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.PyMean)-1,yData.LN1.PyMean)
ciplot(yData.LN1.PyMean-yData.LN1.PySE,...
    yData.LN1.PyMean+yData.LN1.PySE,...
    0:length(yData.LN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.normPx,2)
    plot(yData.EF1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.PyMean)-1,yData.EF1.PyMean)
ciplot(yData.EF1.PyMean-yData.EF1.PySE,...
    yData.EF1.PyMean+yData.EF1.PySE,...
    0:length(yData.EF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.normPx,2)
    plot(yData.LF1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.PyMean)-1,yData.LF1.PyMean)
ciplot(yData.LF1.PyMean-yData.LF1.PySE,...
    yData.LF1.PyMean+yData.LF1.PySE,...
    0:length(yData.LF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.normPx,2)
    plot(yData.EF2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.PyMean)-1,yData.EF2.PyMean)
ciplot(yData.EF2.PyMean-yData.EF2.PySE,...
    yData.EF2.PyMean+yData.EF2.PySE,...
    0:length(yData.EF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.normPx,2)
    plot(yData.LF2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.PyMean)-1,yData.LF2.PyMean)
ciplot(yData.LF2.PyMean-yData.LF2.PySE,...
    yData.LF2.PyMean+yData.LF2.PySE,...
    0:length(yData.LF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.normPx,2)
    plot(yData.EN2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.PyMean)-1,yData.EN2.PyMean)
ciplot(yData.EN2.PyMean-yData.EN2.PySE,...
    yData.EN2.PyMean+yData.EN2.PySE,...
    0:length(yData.EN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.normPx,2)
    plot(yData.LN2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.PyMean)-1,yData.LN2.PyMean)
ciplot(yData.LN2.PyMean-yData.LN2.PySE,...
    yData.LN2.PyMean+yData.LN2.PySE,...
    0:length(yData.LN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN2')

% suptitle('Young Subject Data - Average Trajectories by Phase - Time-Normalized')

%% Plot NonTime Normalized Velocity VS Time

figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.Vx,2)
    plot(yData.EN1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VxMeanNonNorm)-1,yData.EN1.VxMeanNonNorm)
ciplot(yData.EN1.VxMeanNonNorm-yData.EN1.VxSE,...
    yData.EN1.VxMeanNonNorm+yData.EN1.VxSE,...
    0:length(yData.EN1.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.Vx,2)
    plot(yData.LN1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VxMeanNonNorm)-1,yData.LN1.VxMeanNonNorm)
ciplot(yData.LN1.VxMeanNonNorm-yData.LN1.VxSE,...
    yData.LN1.VxMeanNonNorm+yData.LN1.VxSE,...
    0:length(yData.LN1.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.Vx,2)
    plot(yData.EF1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VxMeanNonNorm)-1,yData.EF1.VxMeanNonNorm)
ciplot(yData.EF1.VxMeanNonNorm-yData.EF1.VxSE,...
    yData.EF1.VxMeanNonNorm+yData.EF1.VxSE,...
    0:length(yData.EF1.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.Vx,2)
    plot(yData.LF1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VxMeanNonNorm)-1,yData.LF1.VxMeanNonNorm)
ciplot(yData.LF1.VxMeanNonNorm-yData.LF1.VxSE,...
    yData.LF1.VxMeanNonNorm+yData.LF1.VxSE,...
    0:length(yData.LF1.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.Vx,2)
    plot(yData.EF2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VxMeanNonNorm)-1,yData.EF2.VxMeanNonNorm)
ciplot(yData.EF2.VxMeanNonNorm-yData.EF2.VxSE,...
    yData.EF2.VxMeanNonNorm+yData.EF2.VxSE,...
    0:length(yData.EF2.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.Vx,2)
    plot(yData.LF2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VxMeanNonNorm)-1,yData.LF2.VxMeanNonNorm)
ciplot(yData.LF2.VxMeanNonNorm-yData.LF2.VxSE,...
    yData.LF2.VxMeanNonNorm+yData.LF2.VxSE,...
    0:length(yData.LF2.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.Vx,2)
    plot(yData.EN2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VxMeanNonNorm)-1,yData.EN2.VxMeanNonNorm)
ciplot(yData.EN2.VxMeanNonNorm-yData.EN2.VxSE,...
    yData.EN2.VxMeanNonNorm+yData.EN2.VxSE,...
    0:length(yData.EN2.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.Vx,2)
    plot(yData.LN2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VxMeanNonNorm)-1,yData.LN2.VxMeanNonNorm)
ciplot(yData.LN2.VxMeanNonNorm-yData.LN2.VxSE,...
    yData.LN2.VxMeanNonNorm+yData.LN2.VxSE,...
    0:length(yData.LN2.VxMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.Vy,2)
    plot(yData.EN1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VyMeanNonNorm)-1,yData.EN1.VyMeanNonNorm)
ciplot(yData.EN1.VyMeanNonNorm-yData.EN1.VySE,...
    yData.EN1.VyMeanNonNorm+yData.EN1.VySE,...
    0:length(yData.EN1.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.Vy,2)
    plot(yData.LN1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VyMeanNonNorm)-1,yData.LN1.VyMeanNonNorm)
ciplot(yData.LN1.VyMeanNonNorm-yData.LN1.VySE,...
    yData.LN1.VyMeanNonNorm+yData.LN1.VySE,...
    0:length(yData.LN1.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.Vy,2)
    plot(yData.EF1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VyMeanNonNorm)-1,yData.EF1.VyMeanNonNorm)
ciplot(yData.EF1.VyMeanNonNorm-yData.EF1.VySE,...
    yData.EF1.VyMeanNonNorm+yData.EF1.VySE,...
    0:length(yData.EF1.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.Vy,2)
    plot(yData.LF1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VyMeanNonNorm)-1,yData.LF1.VyMeanNonNorm)
ciplot(yData.LF1.VyMeanNonNorm-yData.LF1.VySE,...
    yData.LF1.VyMeanNonNorm+yData.LF1.VySE,...
    0:length(yData.LF1.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.Vy,2)
    plot(yData.EF2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VyMeanNonNorm)-1,yData.EF2.VyMeanNonNorm)
ciplot(yData.EF2.VyMeanNonNorm-yData.EF2.VySE,...
    yData.EF2.VyMeanNonNorm+yData.EF2.VySE,...
    0:length(yData.EF2.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.Vy,2)
    plot(yData.LF2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VyMeanNonNorm)-1,yData.LF2.VyMeanNonNorm)
ciplot(yData.LF2.VyMeanNonNorm-yData.LF2.VySE,...
    yData.LF2.VyMeanNonNorm+yData.LF2.VySE,...
    0:length(yData.LF2.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.Vy,2)
    plot(yData.EN2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VyMeanNonNorm)-1,yData.EN2.VyMeanNonNorm)
ciplot(yData.EN2.VyMeanNonNorm-yData.EN2.VySE,...
    yData.EN2.VyMeanNonNorm+yData.EN2.VySE,...
    0:length(yData.EN2.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.Vy,2)
    plot(yData.LN2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VyMeanNonNorm)-1,yData.LN2.VyMeanNonNorm)
ciplot(yData.LN2.VyMeanNonNorm-yData.LN2.VySE,...
    yData.LN2.VyMeanNonNorm+yData.LN2.VySE,...
    0:length(yData.LN2.VyMeanNonNorm)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN2')

% suptitle('Young Subject Data - Average Trajectories by Phase - Velocities')

%% Plot with Time-Normalized Trajectories
% Pre Load Old Subect Data or run importSubjGroupDataWInterp

figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.normVx,2)
    plot(yData.EN1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VxMean)-1,yData.EN1.VxMean)
ciplot(yData.EN1.VxMean-yData.EN1.VxSE,...
    yData.EN1.VxMean+yData.EN1.VxSE,...
    0:length(yData.EN1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.normVx,2)
    plot(yData.LN1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VxMean)-1,yData.LN1.VxMean)
ciplot(yData.LN1.VxMean-yData.LN1.VxSE,...
    yData.LN1.VxMean+yData.LN1.VxSE,...
    0:length(yData.LN1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.normVx,2)
    plot(yData.EF1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VxMean)-1,yData.EF1.VxMean)
ciplot(yData.EF1.VxMean-yData.EF1.VxSE,...
    yData.EF1.VxMean+yData.EF1.VxSE,...
    0:length(yData.EF1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.normVx,2)
    plot(yData.LF1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VxMean)-1,yData.LF1.VxMean)
ciplot(yData.LF1.VxMean-yData.LF1.VxSE,...
    yData.LF1.VxMean+yData.LF1.VxSE,...
    0:length(yData.LF1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.normVx,2)
    plot(yData.EF2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VxMean)-1,yData.EF2.VxMean)
ciplot(yData.EF2.VxMean-yData.EF2.VxSE,...
    yData.EF2.VxMean+yData.EF2.VxSE,...
    0:length(yData.EF2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.normVx,2)
    plot(yData.LF2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VxMean)-1,yData.LF2.VxMean)
ciplot(yData.LF2.VxMean-yData.LF2.VxSE,...
    yData.LF2.VxMean+yData.LF2.VxSE,...
    0:length(yData.LF2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.normVx,2)
    plot(yData.EN2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VxMean)-1,yData.EN2.VxMean)
ciplot(yData.EN2.VxMean-yData.EN2.VxSE,...
    yData.EN2.VxMean+yData.EN2.VxSE,...
    0:length(yData.EN2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.normVx,2)
    plot(yData.LN2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VxMean)-1,yData.LN2.VxMean)
ciplot(yData.LN2.VxMean-yData.LN2.VxSE,...
    yData.LN2.VxMean+yData.LN2.VxSE,...
    0:length(yData.LN2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.normVx,2)
    plot(yData.EN1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VyMean)-1,yData.EN1.VyMean)
ciplot(yData.EN1.VyMean-yData.EN1.VySE,...
    yData.EN1.VyMean+yData.EN1.VySE,...
    0:length(yData.EN1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.normVx,2)
    plot(yData.LN1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VyMean)-1,yData.LN1.VyMean)
ciplot(yData.LN1.VyMean-yData.LN1.VySE,...
    yData.LN1.VyMean+yData.LN1.VySE,...
    0:length(yData.LN1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.normVx,2)
    plot(yData.EF1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VyMean)-1,yData.EF1.VyMean)
ciplot(yData.EF1.VyMean-yData.EF1.VySE,...
    yData.EF1.VyMean+yData.EF1.VySE,...
    0:length(yData.EF1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.normVx,2)
    plot(yData.LF1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VyMean)-1,yData.LF1.VyMean)
ciplot(yData.LF1.VyMean-yData.LF1.VySE,...
    yData.LF1.VyMean+yData.LF1.VySE,...
    0:length(yData.LF1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.normVx,2)
    plot(yData.EF2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VyMean)-1,yData.EF2.VyMean)
ciplot(yData.EF2.VyMean-yData.EF2.VySE,...
    yData.EF2.VyMean+yData.EF2.VySE,...
    0:length(yData.EF2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.normVx,2)
    plot(yData.LF2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VyMean)-1,yData.LF2.VyMean)
ciplot(yData.LF2.VyMean-yData.LF2.VySE,...
    yData.LF2.VyMean+yData.LF2.VySE,...
    0:length(yData.LF2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.normVx,2)
    plot(yData.EN2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VyMean)-1,yData.EN2.VyMean)
ciplot(yData.EN2.VyMean-yData.EN2.VySE,...
    yData.EN2.VyMean+yData.EN2.VySE,...
    0:length(yData.EN2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.normVx,2)
    plot(yData.LN2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VyMean)-1,yData.LN2.VyMean)
ciplot(yData.LN2.VyMean-yData.LN2.VySE,...
    yData.LN2.VyMean+yData.LN2.VySE,...
    0:length(yData.LN2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN2')

% suptitle('Young Subject Data - Average Trajectories by Phase - Time-Normalized Velocities')