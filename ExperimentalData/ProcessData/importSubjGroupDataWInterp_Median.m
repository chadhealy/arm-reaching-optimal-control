% Interpolate Trajectories, so average trajectories are smooth

% Needs to be run twice--once on all "Late" Phases, and once with the group
% of Data



% Find Mean Time of Trajectory for Given Phase


function D = importSubjGroupDataWInterp(age)
%
%
% 'subj' is 3-letter acronym, ex: 'FTN'
% 'age' is age group, ex: 'Y' or 'O'

% Check Age Group
if age == 'Y' || age == 'y'
    dirLoc = '/Users/Chad/Documents/CU-IPHY/Research/Sensorimotor Control Research/PracticeModel/HelenData/armmet/RESULTS';
    subjs = {'FFO','FFT','FSH','FST','FTN','ITK','OJL','OTG','OUW','SXI',...
            'YEC','ZEH','ZSE'}; 
%    subjs = {'FTN'}; 
elseif age == 'O' || age == 'o'
    dirLoc = '/Users/Chad/Documents/CU-IPHY/Research/Sensorimotor Control Research/PracticeModel/HelenData/olderfolks/RESULTS';
    subjs = {'BET','EXM','EZC','LGO','MCS','MFQ','MTT','OLT','OSG','OZH',...
            'OZH','QFE','SST','TDS','UTI'};
else
    disp('ERROR: Invalid Age Group')
    return
end

% Change Directory, Save Original Directory
oldDir = pwd;
cd(dirLoc)
addpath(oldDir)
    
phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};% Defined By Helen
%phaseNames = {'EF1'};

for k = 1:length(subjs)
    % Check/Load Subject Data
    if age == 'Y' || age == 'y'
        if exist([dirLoc,'/results_',subjs{k},'_emglp50_rmsmax_v2.mat'])
            load([dirLoc,'/results_',subjs{k},'_emglp50_rmsmax_v2.mat'])
        else
            disp('ERROR: Invalid Subject')
            return
        end
    elseif age == 'O' || age == 'o'
        if exist([dirLoc,'/results_',subjs{k},'_emglp50_mvc_oldms.mat'])
            load([dirLoc,'/results_',subjs{k},'_emglp50_mvc_oldms.mat'])
        else
            disp('ERROR: Invalid Subject')
            return
        end
    end

    % Get Trial Numbers for Specified Phase
    for j = 1:length(phaseNames)
        eval(['trialNums.',phaseNames{j},' = R.trials.',phaseNames{j},';'])
        eval(['ctrialNums.',phaseNames{j},' = R.ctrials.',phaseNames{j},';'])
    
        % Eliminiate "PULL" Trajectories
%        trialNums = trialNums(rem(trialNums, 2)==1);
        eval(['trialNums.',phaseNames{j},' = trialNums.',phaseNames{j},...
            '(rem(trialNums.',phaseNames{j},', 2)==1);'])
        
        % Find Number of Trials
        eval(['nTrials = length(trialNums.',phaseNames{j},');'])
        
        if nTrials >= 1
        maxLength = 0;
        % Find Movement Start (Vy > 0.03 m/s) and Movement End (Vy < 0.03 m/s)   
        for m = 1:nTrials 
            eval(['start = find(TR.trials.Vy.',phaseNames{j},'.out(:,m) > 0.03,1);'])
            eval(['finish = start - 2 + ',...
                'find(TR.trials.Vy.',phaseNames{j},'.out(start:end,m) < 0.03',...
                '& abs(TR.trials.Px.',phaseNames{j},'.out(start:end,m)) < 0.008',...
                '& abs(TR.trials.Py.',phaseNames{j},'.out(start:end,m) - 0.1) < 0.008,1);'])
            if isempty(finish)
                eval(['finish = length(TR.trials.Vy.',phaseNames{j},'.out(:,m));'])
            end
            % Resave Trajectories to New Length
            eval(['TR.trials.Px.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Px.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Py.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Py.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Vx.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Vx.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Vy.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Vy.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Px.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            eval(['TR.trials.Py.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            eval(['TR.trials.Vx.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            eval(['TR.trials.Vy.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            % Store Max Trial Length
            if finish - start + 1 > maxLength
                maxLength = finish - start + 1;
            end

%             % Check if with in time limit (0.3 - 0.6 ms) (60 - 120 time steps)
%             if finish - start + 1 < 60
%                 % Too Fast
%                 eval(['trialNums.',phaseNames{j},' = [];'])
%             elseif finish - start + 1 > 120
%                 % Too Slow
%                 eval(['trialNums.',phaseNames{j},' = [];'])
%             end
        end
        % Remove NaNs
        eval(['TR.trials.Px.',phaseNames{j},'.out = TR.trials.Px.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Py.',phaseNames{j},'.out = TR.trials.Py.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Vx.',phaseNames{j},'.out = TR.trials.Vx.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Vy.',phaseNames{j},'.out = TR.trials.Vy.',phaseNames{j},'.out(1:maxLength,:);'])
        
        end
        
        % Eliminate Botched Trials (too short)
        % (Ex: Subj-'FTN', Phase-EF2)
         minLength = 50;
         maxLength = 500;
         if eval(['size(TR.trials.Px.',phaseNames{j},'.out,2)==1'])
             if eval(['length(TR.trials.Px.',phaseNames{j},'.out) < minLength'])
                 eval(['trialNums.',phaseNames{j},' = [];']) 
             elseif eval(['length(TR.trials.Px.',phaseNames{j},'.out) > maxLength'])
                 eval(['trialNums.',phaseNames{j},' = [];']) 
             end
         else
             eval(['[II,JJ] = find(isnan(TR.trials.Px.',phaseNames{j},'.out));'])
             III = find(II < minLength);
             eval(['trialNums.',phaseNames{j},'(JJ(III)) = [];']) 
         end

        % Re-Find Number of Trials
        eval(['nTrials = length(trialNums.',phaseNames{j},');'])


        
        if nTrials > 1
        % Find Average Trial Length        
        eval(['trLength = ones(1,nTrials)*size(TR.trials.Px.',...
            phaseNames{j},'.out,1);'])
        for jj = 1:nTrials
            eval(['notLongest = ~isempty(find(isnan(TR.trials.Px.',...
                phaseNames{j},'.out(:,jj)),1,''first''));'])
            if notLongest    
                eval(['trLength(jj) = find(isnan(TR.trials.Px.',...
                    phaseNames{j},'.out(:,jj)),1,''first'')-1;'])
            end
        end
        % Is Floor a bad assumption? Would Rounding up be better or worse?
        avgTrLength = floor(mean(trLength));
        
        % Resample Trajectories so the same time length
        normPx = zeros(avgTrLength,nTrials);
        normPy = zeros(avgTrLength,nTrials);
        normVx = zeros(avgTrLength,nTrials);
        normVy = zeros(avgTrLength,nTrials);
        for jj = 1:nTrials;
            eval(['normPx(:,jj) = resample(TR.trials.Px.',phaseNames{j},...
                '.out(:,jj),avgTrLength,length(TR.trials.Px.',phaseNames{j},'.out(:,jj)),0);'])
            eval(['normPy(:,jj) = resample(TR.trials.Py.',phaseNames{j},...
                '.out(:,jj),avgTrLength,length(TR.trials.Py.',phaseNames{j},'.out(:,jj)),0);'])
            eval(['normVx(:,jj) = resample(TR.trials.Vx.',phaseNames{j},...
                '.out(:,jj),avgTrLength,length(TR.trials.Vx.',phaseNames{j},'.out(:,jj)),0);'])
            eval(['normVy(:,jj) = resample(TR.trials.Vy.',phaseNames{j},...
                '.out(:,jj),avgTrLength,length(TR.trials.Vy.',phaseNames{j},'.out(:,jj)),0);'])
        end

        % Find the average trajectory
        for ii = 1:avgTrLength
            eval([phaseNames{j},'.Px(k,ii) = mean(normPx(ii,:));'])
            eval([phaseNames{j},'.Py(k,ii) = mean(normPy(ii,:));'])
            eval([phaseNames{j},'.Vx(k,ii) = mean(normVx(ii,:));'])
            eval([phaseNames{j},'.Vy(k,ii) = mean(normVy(ii,:));'])
        end
        
        elseif nTrials == 1  
            
            eval(['trialLength = size(TR.trials.Px.',phaseNames{j},'.out,1);'])

            for ii = 1:trialLength
                eval([phaseNames{j},'.Px(k,ii) = TR.trials.Px.',phaseNames{j},'.out(ii,:);'])
                eval([phaseNames{j},'.Py(k,ii) = TR.trials.Py.',phaseNames{j},'.out(ii,:);'])
                eval([phaseNames{j},'.Vx(k,ii) = TR.trials.Vx.',phaseNames{j},'.out(ii,:);'])
                eval([phaseNames{j},'.Vy(k,ii) = TR.trials.Vy.',phaseNames{j},'.out(ii,:);'])
            end
            
        else
            eval(['trialLength = size(TR.trials.Px.',phaseNames{j},'.out,1);'])
            for ii = 1:trialLength
                eval([phaseNames{j},'.Px(k,ii) = NaN;'])
                eval([phaseNames{j},'.Py(k,ii) = NaN;'])
                eval([phaseNames{j},'.Vx(k,ii) = NaN;'])
                eval([phaseNames{j},'.Vy(k,ii) = NaN;'])
            end
        end

        % Get Curl Field Estimation
        %      Calculates average if phase has multiple catch trials
        estGain = 0;
        eval(['nTemp = length(ctrialNums.',phaseNames{j},');']);
        for i = 1:nTemp
            eval(['estGain = estGain + R.estgain(ctrialNums.',phaseNames{j},'(i));'])
        end
        eval([phaseNames{j},'.estGain(k) = -1*estGain/length(ctrialNums.',phaseNames{j},');'])%Takes averages, converts to negative (from abs. value)
        
        % Get Max Perpendicular Error
        eval(['temp = isempty(trialNums.',phaseNames{j},');'])
        if temp
            % This case is for single trials that are most likely PULL
            % trials.  This chooses the following PUSH trial.
            eval([phaseNames{j},'.maxPerp(k) = R.maxperp(R.trials.',phaseNames{j},'+1);'])
        else
            eval([phaseNames{j},'.maxPerp(k) = R.maxperp(min(trialNums.',phaseNames{j},'));'])
        end
    end
end

%Reformat Zeros to NaNs
for j = 1:length(phaseNames)
    eval([phaseNames{j},'.Px(find(',phaseNames{j},'.Px == 0)) = NaN;'])
    eval([phaseNames{j},'.Py(find(',phaseNames{j},'.Py == 0)) = NaN;'])
    eval([phaseNames{j},'.Vx(find(',phaseNames{j},'.Vx == 0)) = NaN;'])
    eval([phaseNames{j},'.Vy(find(',phaseNames{j},'.Vy == 0)) = NaN;'])
end



%%%%%%%%%%%%%%%%%%%%%%%%
% Average Subject Data

if length(subjs) > 1
    for j = 1:length(phaseNames)
        % Find Valid Trials for Given Phase
        eval(['validSubjs = find(~isnan(',phaseNames{j},'.Px(:,1)));'])
        
        % Find Average Trial Length
        eval(['trLength = ones(1,length(validSubjs))*size(',phaseNames{j},'.Px,2);'])
        for jj = 1:length(validSubjs)
            eval(['notLongest = ~isempty(find(isnan(',phaseNames{j},'.Px(validSubjs(jj),:)),1,''first''));'])
            if notLongest    
                eval(['trLength(jj) = find(isnan(',phaseNames{j},'.Px(validSubjs(jj),:)),1,''first'')-1;'])
            end
        end
        % Is Floor a bad assumption? Would Rounding up be better or worse?
        avgTrLength = floor(mean(trLength));

        % Resample Trajectories so the same time length
        normPx = zeros(avgTrLength,length(validSubjs));
        normPy = zeros(avgTrLength,length(validSubjs));
        normVx = zeros(avgTrLength,length(validSubjs));
        normVy = zeros(avgTrLength,length(validSubjs));
        for jj = 1:length(validSubjs);
            eval(['normPx(:,jj) = resample(',phaseNames{j},...
                '.Px(validSubjs(jj),1:trLength(jj)),avgTrLength,trLength(jj),0);'])
            eval(['normPy(:,jj) = resample(',phaseNames{j},...
                '.Py(validSubjs(jj),1:trLength(jj)),avgTrLength,trLength(jj),0);'])            
            eval(['normVx(:,jj) = resample(',phaseNames{j},...
                '.Vx(validSubjs(jj),1:trLength(jj)),avgTrLength,trLength(jj),0);'])            
            eval(['normVy(:,jj) = resample(',phaseNames{j},...
                '.Vy(validSubjs(jj),1:trLength(jj)),avgTrLength,trLength(jj),0);'])
        end
        
        %Save Normalized Trajectories
        eval([phaseNames{j},'.normPx = normPx;'])
        eval([phaseNames{j},'.normPy = normPy;'])
        eval([phaseNames{j},'.normVx = normVx;'])
        eval([phaseNames{j},'.normVy = normVy;'])

        
        % Find the average trajectory
        for ii = 1:avgTrLength
            eval([phaseNames{j},'.PxMean(ii) = median(normPx(ii,:));'])
            eval([phaseNames{j},'.PyMean(ii) = median(normPy(ii,:));'])
            eval([phaseNames{j},'.VxMean(ii) = median(normVx(ii,:));'])
            eval([phaseNames{j},'.VyMean(ii) = median(normVy(ii,:));'])
            eval([phaseNames{j},'.PxSE(ii) = std(normPx(ii,:))/sqrt(length(normPx(ii,:)));'])
            eval([phaseNames{j},'.PySE(ii) = std(normPy(ii,:))/sqrt(length(normPy(ii,:)));'])
            eval([phaseNames{j},'.VxSE(ii) = std(normVx(ii,:))/sqrt(length(normVx(ii,:)));'])
            eval([phaseNames{j},'.VySE(ii) = std(normVy(ii,:))/sqrt(length(normVy(ii,:)));'])
        end

        eval([phaseNames{j},'.maxPerpMean = nanmean(',phaseNames{j},'.maxPerp);'])
        eval([phaseNames{j},'.estGainMean = nanmean(',phaseNames{j},'.estGain);'])
    
    end
    
else %If a single subject
    for j = 1:length(phaseNames)
        
        eval(['trialLength = size(',phaseNames{j},'.Px,2);'])
        for ii = 1:trialLength
            eval([phaseNames{j},'.PxMean(ii) = ',phaseNames{j},'.Px(ii);'])
            eval([phaseNames{j},'.PyMean(ii) = ',phaseNames{j},'.Py(ii);'])
            eval([phaseNames{j},'.VxMean(ii) = ',phaseNames{j},'.Vx(ii);'])
            eval([phaseNames{j},'.VyMean(ii) = ',phaseNames{j},'.Vy(ii);'])
        end    
        eval([phaseNames{j},'.maxPerpMean = nanmean(',phaseNames{j},'.maxPerp);'])
        eval([phaseNames{j},'.estGainMean = nanmean(',phaseNames{j},'.estGain);'])    
    end
end
    

% Combine into Structure for Variable Out
for j = 1:length(phaseNames)
    eval(['D.',phaseNames{j},' = ',phaseNames{j},';'])  
end