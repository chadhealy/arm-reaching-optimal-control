%Making Median Trajectory Data to Fit

%load('/Users/Chad/Documents/CU-IPHY/Research/ArmModelLearningExtent/CurrentFiles/ExperimentalData/RunFitCode/ArmModel/YoungData-ToFit-4TrialMean.mat');
load('/Users/Chad/Documents/CU-IPHY/Research/ArmModelLearningExtent/CurrentFiles/ExperimentalData/RunFitCode/ArmModel/OldData-ToFit-4TrialMean.mat');
yData = oData;

phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LF2'};

for j = 1:length(phaseNames)
    eval(['nTrials = size(yData.',phaseNames{j},'.Px,2);'])
    for i = 1:nTrials
        eval(['[~,maxPerpInd] = max(abs(yData.',phaseNames{j},'.Px(:,i)));'])
        eval(['maxPerp(i) = yData.',phaseNames{j},'.Px(maxPerpInd,i);'])        
    end
    sorted = sort(maxPerp);
    medTrial = find(maxPerp==sorted(floor(length(maxPerp)/2)));
    % Get rid of NaNs
    eval(['endPt = find(isnan(yData.',phaseNames{j},'.Px(:,medTrial)),1,''first'')-1;'])
    %
    eval(['yData.',phaseNames{j},'.PxMean = yData.',phaseNames{j},'.Px(1:endPt,medTrial)'';'])
    eval(['yData.',phaseNames{j},'.PyMean = yData.',phaseNames{j},'.Py(1:endPt,medTrial)'';'])
    eval(['yData.',phaseNames{j},'.VxMean = yData.',phaseNames{j},'.Vx(1:endPt,medTrial)'';'])
    eval(['yData.',phaseNames{j},'.VyMean = yData.',phaseNames{j},'.Vy(1:endPt,medTrial)'';'])
    clear maxPerp
end

oData = yData;

