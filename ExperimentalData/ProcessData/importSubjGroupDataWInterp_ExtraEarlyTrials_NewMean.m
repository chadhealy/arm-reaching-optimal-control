% Interpolate Trajectories, so average trajectories are smooth

% Needs to be run twice--once on all "Late" Phases, and once with the group
% of Data



% Find Mean Time of Trajectory for Given Phase


function D = importSubjGroupDataWInterp_ExtraEarlyTrials_NewMean(age)
%
%
% 'subj' is 3-letter acronym, ex: 'FTN'
% 'age' is age group, ex: 'Y' or 'O'

% Check Age Group
if age == 'Y' || age == 'y'
    dirLoc = '/Users/Chad/Documents/CU-IPHY/Research/Sensorimotor Control Research/PracticeModel/HelenData/armmet/RESULTS';
    dirLoc2 = '/Users/Chad/Documents/CU-IPHY/Research/Sensorimotor Control Research/PracticeModel/HelenData/armmet';
    subjs = {'FFO','FFT','FSH','FST','FTN','ITK','OJL','OTG','OUW','SXI',...
            'YEC','ZEH','ZSE'}; 
%    subjs = {'FTN'}; 
elseif age == 'O' || age == 'o'
    dirLoc = '/Users/Chad/Documents/CU-IPHY/Research/Sensorimotor Control Research/PracticeModel/HelenData/olderfolks/RESULTS';
    dirLoc2 = '/Users/Chad/Documents/CU-IPHY/Research/Sensorimotor Control Research/PracticeModel/HelenData/olderfolks';
    subjs = {'BET','EXM','EZC','LGO','MCS','MFQ','MTT','OLT','OSG','OZH',...
            'OZH','QFE','SST','TDS','UTI'};
else
    disp('ERROR: Invalid Age Group')
    return
end

% Change Directory, Save Original Directory
oldDir = pwd;
cd(dirLoc)
addpath(oldDir)
    
phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};% Defined By Helen
%phaseNames = {'EF1'};

phaseTrialNums.EN1 = 1:10;
phaseTrialNums.EF1 = 201:210;
phaseTrialNums.EF2 = 451:460;
phaseTrialNums.EN2 = 701:710;

for k = 1:length(subjs)
    % Check/Load Subject Data
    if age == 'Y' || age == 'y'
        if exist([dirLoc,'/results_',subjs{k},'_emglp50_rmsmax_v2.mat'])
            load([dirLoc,'/results_',subjs{k},'_emglp50_rmsmax_v2.mat'])
        else
            disp('ERROR: Invalid Subject')
            return
        end
    elseif age == 'O' || age == 'o'
        if exist([dirLoc,'/results_',subjs{k},'_emglp50_mvc_oldms.mat'])
            load([dirLoc,'/results_',subjs{k},'_emglp50_mvc_oldms.mat'])
        else
            disp('ERROR: Invalid Subject')
            return
        end
    end

    % Get Trial Numbers for Specified Phase
    for j = 1:length(phaseNames)
        eval(['trialNums.',phaseNames{j},' = R.trials.',phaseNames{j},';'])
        eval(['ctrialNums.',phaseNames{j},' = R.ctrials.',phaseNames{j},';'])
        
        
        % Add Additional Trial to Early Phases (for now, out only)
        if rem(j,2)==1
            % Reload Trial Data
            clear T             
            eval(['load(''',dirLoc2,'/',subjs{k},'/',subjs{k},'.mat'', ''T'');'])            
            % Re-find Catch Trials
            eval(['ctrialNums.',phaseNames{j},' = intersect(find(strcmp(T.trialtypename,''clamp'')),phaseTrialNums.',phaseNames{j},');'])
            % Re-find actual Trials
            eval(['trialNums.',phaseNames{j},' = setxor(ctrialNums.',...
                phaseNames{j},',phaseTrialNums.',phaseNames{j},');'])
        end
        
        % Eliminiate "PULL" Trajectories
%        trialNums = trialNums(rem(trialNums, 2)==1);
        eval(['trialNums.',phaseNames{j},' = trialNums.',phaseNames{j},...
            '(rem(trialNums.',phaseNames{j},', 2)==1);'])

        % Find Number of Trials
        eval(['nTrials = length(trialNums.',phaseNames{j},');'])
        
        if rem(j,2)==1
            % Add to TR structure

            % Find Max Trial Length
            maxLength = 0;
            for m = 1:nTrials
                eval(['lengths(m) = length(T.framedata(1,trialNums.',phaseNames{j},'(m)).x);'])
                if lengths(m) > maxLength
                   maxLength = lengths(m); 
                end
            end
            % Preallocate Matrix
            eval(['TR.trials.Px.',phaseNames{j},'.out = nan(maxLength,nTrials);'])
            eval(['TR.trials.Py.',phaseNames{j},'.out = nan(maxLength,nTrials);'])
            eval(['TR.trials.Vx.',phaseNames{j},'.out = nan(maxLength,nTrials);'])
            eval(['TR.trials.Vy.',phaseNames{j},'.out = nan(maxLength,nTrials);'])
            % Fill Matrix with Trial Data
            for m = 1:nTrials
                eval(['TR.trials.Px.',phaseNames{j},'.out(1:lengths(m),m) = T.framedata(1,trialNums.',phaseNames{j},'(m)).x;'])
                eval(['TR.trials.Py.',phaseNames{j},'.out(1:lengths(m),m) = T.framedata(1,trialNums.',phaseNames{j},'(m)).y;'])
                eval(['TR.trials.Vx.',phaseNames{j},'.out(1:lengths(m),m) = T.framedata(1,trialNums.',phaseNames{j},'(m)).vx;'])
                eval(['TR.trials.Vy.',phaseNames{j},'.out(1:lengths(m),m) = T.framedata(1,trialNums.',phaseNames{j},'(m)).vy;'])
            end
        end
            
        if nTrials >= 1
        maxLength = 0;
        % Find Movement Start (Vy > 0.03 m/s) and Movement End (Vy < 0.03 m/s)   
        for m = 1:nTrials 
%             eval(['start = find(TR.trials.Vy.',phaseNames{j},'.out(:,m) > 0.03,1);'])
            eval(['start = find(TR.trials.Vy.',phaseNames{j},...
                '.out(:,m) > 0.03 & (abs(TR.trials.Px.',phaseNames{j},...
                '.out(:,m)) > 0.008 | abs(TR.trials.Py.',phaseNames{j},...
                '.out(:,m) + 0.1) > 0.008),1);'])
            eval(['finish = start - 2 + ',...
                'find(TR.trials.Vy.',phaseNames{j},'.out(start:end,m) < 0.03',...
                '& abs(TR.trials.Px.',phaseNames{j},'.out(start:end,m)) < 0.008',...
                '& abs(TR.trials.Py.',phaseNames{j},'.out(start:end,m) - 0.1) < 0.008,1);'])
            if isempty(finish)
                eval(['finish = length(TR.trials.Vy.',phaseNames{j},'.out(:,m));'])
            end
            % Resave Trajectories to New Length
            eval(['TR.trials.Px.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Px.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Py.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Py.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Vx.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Vx.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Vy.',phaseNames{j},'.out(1:(finish-start+1),m) = TR.trials.Vy.',phaseNames{j},'.out(start:finish,m);'])
            eval(['TR.trials.Px.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            eval(['TR.trials.Py.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            eval(['TR.trials.Vx.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
            eval(['TR.trials.Vy.',phaseNames{j},'.out(finish-start+2:end,m) = NaN;'])
        end
        % Remove NaNs
        eval(['[inan,jnan] = find(~isnan(TR.trials.Px.',phaseNames{j},'.out));'])
        maxLength = max(inan);
        eval(['TR.trials.Px.',phaseNames{j},'.out = TR.trials.Px.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Py.',phaseNames{j},'.out = TR.trials.Py.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Vx.',phaseNames{j},'.out = TR.trials.Vx.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Vy.',phaseNames{j},'.out = TR.trials.Vy.',phaseNames{j},'.out(1:maxLength,:);'])


        end
        
        % Eliminate Botched Trials 
        % (Ex: Subj-'FTN', Phase-EF2)
         minLength = 50;%50     60  (300 ms)
         maxLength = 300;%300   120 (600 ms)
         if eval(['size(TR.trials.Px.',phaseNames{j},'.out,2)==1'])
             if eval(['length(TR.trials.Px.',phaseNames{j},'.out) < minLength'])
                 eval(['trialNums.',phaseNames{j},' = [];']) 
                 eval(['TR.trials.Px.',phaseNames{j},'.out = [];'])
                 eval(['TR.trials.Py.',phaseNames{j},'.out = [];'])
                 eval(['TR.trials.Vx.',phaseNames{j},'.out = [];'])
                 eval(['TR.trials.Vy.',phaseNames{j},'.out = [];'])
             elseif eval(['length(TR.trials.Px.',phaseNames{j},'.out) > maxLength'])
                 eval(['trialNums.',phaseNames{j},' = [];']) 
                 eval(['TR.trials.Px.',phaseNames{j},'.out = [];'])
                 eval(['TR.trials.Py.',phaseNames{j},'.out = [];'])
                 eval(['TR.trials.Vx.',phaseNames{j},'.out = [];'])
                 eval(['TR.trials.Vy.',phaseNames{j},'.out = [];'])
             end
         else
             % Remove if Too Short
             eval(['[II,JJ] = find(isnan(TR.trials.Px.',phaseNames{j},'.out));'])
             III = find(II < minLength);
             eval(['trialNums.',phaseNames{j},'(JJ(III)) = [];']) 
             eval(['TR.trials.Px.',phaseNames{j},'.out(:,JJ(III)) = [];'])
             eval(['TR.trials.Py.',phaseNames{j},'.out(:,JJ(III)) = [];'])
             eval(['TR.trials.Vx.',phaseNames{j},'.out(:,JJ(III)) = [];'])
             eval(['TR.trials.Vy.',phaseNames{j},'.out(:,JJ(III)) = [];'])
             % Remove if Too Long
             eval(['[nII,nJJ] = find(~isnan(TR.trials.Px.',phaseNames{j},'.out));'])
             nIII = find(nII > maxLength);
             eval(['trialNums.',phaseNames{j},'(nJJ(nIII)) = [];'])
             eval(['TR.trials.Px.',phaseNames{j},'.out(:,nJJ(nIII)) = [];'])
             eval(['TR.trials.Py.',phaseNames{j},'.out(:,nJJ(nIII)) = [];'])
             eval(['TR.trials.Vx.',phaseNames{j},'.out(:,nJJ(nIII)) = [];'])
             eval(['TR.trials.Vy.',phaseNames{j},'.out(:,nJJ(nIII)) = [];'])
         end
         
        % Re-remove NaNs
        eval(['[inan,jnan] = find(~isnan(TR.trials.Px.',phaseNames{j},'.out));'])
        maxLength = max(inan);
        eval(['TR.trials.Px.',phaseNames{j},'.out = TR.trials.Px.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Py.',phaseNames{j},'.out = TR.trials.Py.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Vx.',phaseNames{j},'.out = TR.trials.Vx.',phaseNames{j},'.out(1:maxLength,:);'])
        eval(['TR.trials.Vy.',phaseNames{j},'.out = TR.trials.Vy.',phaseNames{j},'.out(1:maxLength,:);'])

         

        % Re-Find Number of Trials
        eval(['nTrials = length(trialNums.',phaseNames{j},');'])


        
%         if nTrials > 1
%         % Find Average Trial Length        
%         eval(['trLength = ones(1,nTrials)*size(TR.trials.Px.',...
%             phaseNames{j},'.out,1);'])
%         for jj = 1:nTrials
%             eval(['notLongest = ~isempty(find(isnan(TR.trials.Px.',...
%                 phaseNames{j},'.out(:,jj)),1,''first''));'])
%             if notLongest    
%                 eval(['trLength(jj) = find(isnan(TR.trials.Px.',...
%                     phaseNames{j},'.out(:,jj)),1,''first'')-1;'])
%             end
%         end
%         % Is Floor a bad assumption? Would Rounding up be better or worse?
%         avgTrLength = floor(mean(trLength));
%         
%         % Resample Trajectories so the same time length
%         normPx = zeros(avgTrLength,nTrials);
%         normPy = zeros(avgTrLength,nTrials);
%         normVx = zeros(avgTrLength,nTrials);
%         normVy = zeros(avgTrLength,nTrials);
%         for jj = 1:nTrials;
%             eval(['normPx(:,jj) = resample(TR.trials.Px.',...
%                 phaseNames{j},'.out(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])
%             eval(['normPy(:,jj) = resample(TR.trials.Py.',...
%                 phaseNames{j},'.out(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])
%             eval(['normVx(:,jj) = resample(TR.trials.Vx.',...
%                 phaseNames{j},'.out(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])
%             eval(['normVy(:,jj) = resample(TR.trials.Vy.',...
%                 phaseNames{j},'.out(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])
%         end
% 
%         % Find the average trajectory
%         for ii = 1:avgTrLength
%             eval([phaseNames{j},'.Px(k,ii) = mean(normPx(ii,:));'])
%             eval([phaseNames{j},'.Py(k,ii) = mean(normPy(ii,:));'])
%             eval([phaseNames{j},'.Vx(k,ii) = mean(normVx(ii,:));'])
%             eval([phaseNames{j},'.Vy(k,ii) = mean(normVy(ii,:));'])
%         end
%         
%         elseif nTrials == 1  
%             
%             eval(['trialLength = size(TR.trials.Px.',phaseNames{j},'.out,1);'])
% 
%             for ii = 1:trialLength
%                 eval([phaseNames{j},'.Px(k,ii) = TR.trials.Px.',phaseNames{j},'.out(ii,:);'])
%                 eval([phaseNames{j},'.Py(k,ii) = TR.trials.Py.',phaseNames{j},'.out(ii,:);'])
%                 eval([phaseNames{j},'.Vx(k,ii) = TR.trials.Vx.',phaseNames{j},'.out(ii,:);'])
%                 eval([phaseNames{j},'.Vy(k,ii) = TR.trials.Vy.',phaseNames{j},'.out(ii,:);'])
%             end
%             
%         else
%             eval(['trialLength = size(TR.trials.Px.',phaseNames{j},'.out,1);'])
%             for ii = 1:trialLength
%                 eval([phaseNames{j},'.Px(k,ii) = NaN;'])
%                 eval([phaseNames{j},'.Py(k,ii) = NaN;'])
%                 eval([phaseNames{j},'.Vx(k,ii) = NaN;'])
%                 eval([phaseNames{j},'.Vy(k,ii) = NaN;'])
%             end
%         end

        if k == 1
            eval([phaseNames{j},'.Px = TR.trials.Px.',phaseNames{j},'.out;'])
            eval([phaseNames{j},'.Py = TR.trials.Py.',phaseNames{j},'.out;'])
            eval([phaseNames{j},'.Vx = TR.trials.Vx.',phaseNames{j},'.out;'])
            eval([phaseNames{j},'.Vy = TR.trials.Vy.',phaseNames{j},'.out;'])
        elseif nTrials > 0
            eval(['trialLength = size(TR.trials.Px.',phaseNames{j},'.out,1);'])
            eval(['matrixLength = size(',phaseNames{j},'.Px,1);'])
            if trialLength > matrixLength
                eval([phaseNames{j},'.Px(matrixLength+1:trialLength,:) = nan(trialLength - matrixLength,size(',phaseNames{j},'.Px,2));'])
                eval([phaseNames{j},'.Py(matrixLength+1:trialLength,:) = nan(trialLength - matrixLength,size(',phaseNames{j},'.Py,2));'])
                eval([phaseNames{j},'.Vx(matrixLength+1:trialLength,:) = nan(trialLength - matrixLength,size(',phaseNames{j},'.Vx,2));'])
                eval([phaseNames{j},'.Vy(matrixLength+1:trialLength,:) = nan(trialLength - matrixLength,size(',phaseNames{j},'.Vy,2));'])            
            elseif trialLength < matrixLength
                eval(['TR.trials.Px.',phaseNames{j},'.out(trialLength+1:matrixLength,:) = nan(matrixLength - trialLength,nTrials);'])
                eval(['TR.trials.Py.',phaseNames{j},'.out(trialLength+1:matrixLength,:) = nan(matrixLength - trialLength,nTrials);'])
                eval(['TR.trials.Vx.',phaseNames{j},'.out(trialLength+1:matrixLength,:) = nan(matrixLength - trialLength,nTrials);'])
                eval(['TR.trials.Vy.',phaseNames{j},'.out(trialLength+1:matrixLength,:) = nan(matrixLength - trialLength,nTrials);'])
            end
            eval([phaseNames{j},'.Px(:,end+1:end+nTrials) = TR.trials.Px.',phaseNames{j},'.out;'])
            eval([phaseNames{j},'.Py(:,end+1:end+nTrials) = TR.trials.Py.',phaseNames{j},'.out;'])
            eval([phaseNames{j},'.Vx(:,end+1:end+nTrials) = TR.trials.Vx.',phaseNames{j},'.out;'])
            eval([phaseNames{j},'.Vy(:,end+1:end+nTrials) = TR.trials.Vy.',phaseNames{j},'.out;'])
        end
        
        % Get Curl Field Estimation
        % Find Additional Curl Field Trials (this is only applicable if we
        % extend early phase trials past the first batch.
%         if rem(j,2)==1
%             eval(['ctrialNums,',phaseNames{j},' = intersect(find(strcmp(T.trialtypename,''clamp'')),phaseTrialNums.',phaseNames{j},');'])
%         end
        
        %      Calculates average if phase has multiple catch trials
        estGain = 0;
        eval(['nTemp = length(ctrialNums.',phaseNames{j},');']);
        for i = 1:nTemp
            eval(['estGain = estGain + R.estgain(ctrialNums.',phaseNames{j},'(i));'])
        end
        eval([phaseNames{j},'.estGain(k) = -1*estGain/length(ctrialNums.',phaseNames{j},');'])%Takes averages, converts to negative (from abs. value)
        
%         % Get Max Perpendicular Error
%         eval(['temp = isempty(trialNums.',phaseNames{j},');'])
%         if temp
%             % This case is for single trials that are most likely PULL
%             % trials.  This chooses the following PUSH trial.
%             eval([phaseNames{j},'.maxPerp(k) = R.maxperp(R.trials.',phaseNames{j},'+1);'])
%         else
%             eval([phaseNames{j},'.maxPerp(k) = R.maxperp(min(trialNums.',phaseNames{j},'));'])
%         end
%    disp(['Subj: ',num2str(subjs{k}),'   ',phaseNames{j},'.trialNums = ',eval(['num2str(trialNums.',phaseNames{j},''')'])])
            
        %Max Perp and Angular Error
        if k == 1
           eval([phaseNames{j},'.maxPerp = [];'])
            if nTrials > 0
                for i = 1:nTrials
                    eval(['[~,maxPerpInd] = max(abs(TR.trials.Px.',phaseNames{j},'.out(:,i)));'])
                    eval([phaseNames{j},'.maxPerp = TR.trials.Px.',phaseNames{j},'.out(maxPerpInd,i);'])
                    
                    eval(['[~,maxVelInd] = max(TR.trials.Vx.',phaseNames{j},...
                        '.out(:,i).^2 + TR.trials.Vy.',phaseNames{j},'.out(:,i).^2);'])
                    eval([phaseNames{j},'.angErr = acosd(dot([TR.trials.Px.',phaseNames{j},...
                        '.out(maxVelInd,i) - 0; TR.trials.Py.',phaseNames{j},'.out(maxVelInd,i) + 0.1],[0;1])/(norm([TR.trials.Px.',phaseNames{j},...
                        '.out(maxVelInd,i) - 0; TR.trials.Py.',phaseNames{j},'.out(maxVelInd,i) + 0.1])*norm([0;1])));'])
                end
            end
        else
            eval(['totTrials = length(',phaseNames{j},'.maxPerp);'])
            for i = 1:nTrials
                eval(['[~,maxPerpInd] = max(abs(TR.trials.Px.',phaseNames{j},'.out(:,i)));'])
                eval([phaseNames{j},'.maxPerp(totTrials+i) = TR.trials.Px.',phaseNames{j},'.out(maxPerpInd,i);'])
                
                eval(['[~,maxVelInd] = max(TR.trials.Vx.',phaseNames{j},...
                        '.out(:,i).^2 + TR.trials.Vy.',phaseNames{j},'.out(:,i).^2);'])
                eval([phaseNames{j},'.angErr(totTrials+i) = acosd(dot([TR.trials.Px.',phaseNames{j},...
                        '.out(maxVelInd,i) - 0; TR.trials.Py.',phaseNames{j},'.out(maxVelInd,i) + 0.1],[0;1])/(norm([TR.trials.Px.',phaseNames{j},...
                        '.out(maxVelInd,i) - 0; TR.trials.Py.',phaseNames{j},'.out(maxVelInd,i) + 0.1])*norm([0;1])));'])

            end
        end
    end
end

% %Reformat Zeros to NaNs
% for j = 1:length(phaseNames)
%     eval([phaseNames{j},'.Px(find(',phaseNames{j},'.Px == 0)) = NaN;'])
%     eval([phaseNames{j},'.Py(find(',phaseNames{j},'.Py == 0)) = NaN;'])
%     eval([phaseNames{j},'.Vx(find(',phaseNames{j},'.Vx == 0)) = NaN;'])
%     eval([phaseNames{j},'.Vy(find(',phaseNames{j},'.Vy == 0)) = NaN;'])
% end



%%%%%%%%%%%%%%%%%%%%%%%%
% Average Subject Data

% if length(subjs) > 1
    for j = 1:length(phaseNames)
        % Find Valid Trials for Given Phase
%         eval(['validSubjs = find(~isnan(',phaseNames{j},'.Px(:,1)));'])
%        eval(['validTrials = find(~isnan(',phaseNames{j},'.Px(:,1)));'])
        eval(['numTrials = size(',phaseNames{j},'.Px,2);'])
        
        if numTrials > 0 
        % Find Average Trial Length
        eval(['trLength = ones(1,numTrials)*size(',phaseNames{j},'.Px,1);'])
        for jj = 1:numTrials
            eval(['notLongest = ~isempty(find(isnan(',phaseNames{j},'.Px(:,jj)),1,''first''));'])
            if notLongest    
                eval(['trLength(jj) = find(isnan(',phaseNames{j},'.Px(:,jj)),1,''first'')-1;'])
            end
        end
        % Is Floor a bad assumption? Would Rounding up be better or worse?
        avgTrLength = floor(mean(trLength));

        % Resample Trajectories so the same time length
        normPx = zeros(avgTrLength,numTrials);
        normPy = zeros(avgTrLength,numTrials);
        normVx = zeros(avgTrLength,numTrials);
        normVy = zeros(avgTrLength,numTrials);
        for jj = 1:numTrials;
            eval(['normPx(:,jj) = resample(',phaseNames{j},...
                '.Px(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])
            eval(['normPy(:,jj) = resample(',phaseNames{j},...
                '.Py(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])            
            eval(['normVx(:,jj) = resample(',phaseNames{j},...
                '.Vx(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])            
            eval(['normVy(:,jj) = resample(',phaseNames{j},...
                '.Vy(1:trLength(jj),jj),avgTrLength,trLength(jj),0);'])
        end
    
                
        %Save Normalized Trajectories
        eval([phaseNames{j},'.normPx = normPx;'])
        eval([phaseNames{j},'.normPy = normPy;'])
        eval([phaseNames{j},'.normVx = normVx;'])
        eval([phaseNames{j},'.normVy = normVy;'])
        
        % Find the average trajectory
        for ii = 1:avgTrLength
            eval([phaseNames{j},'.PxMean(ii) = mean(normPx(ii,:));'])
            eval([phaseNames{j},'.PyMean(ii) = mean(normPy(ii,:));'])
            eval([phaseNames{j},'.VxMean(ii) = mean(normVx(ii,:));'])
            eval([phaseNames{j},'.VyMean(ii) = mean(normVy(ii,:));'])
            eval([phaseNames{j},'.PxSE(ii) = std(normPx(ii,:))/sqrt(length(normPx(ii,:)));'])
            eval([phaseNames{j},'.PySE(ii) = std(normPy(ii,:))/sqrt(length(normPy(ii,:)));'])
            eval([phaseNames{j},'.VxSE(ii) = std(normVx(ii,:))/sqrt(length(normVx(ii,:)));'])
            eval([phaseNames{j},'.VySE(ii) = std(normVy(ii,:))/sqrt(length(normVy(ii,:)));'])
        end

        % Need to adjust....
%         eval([phaseNames{j},'.maxPerpMean = nanmean(',phaseNames{j},'.maxPerp);'])
        eval([phaseNames{j},'.estGainMean = nanmean(',phaseNames{j},'.estGain);'])
    
        % Max Perp Mean, STD
        eval([phaseNames{j},'.maxPerpMean = mean(',phaseNames{j},'.maxPerp);'])
        eval([phaseNames{j},'.maxPerpSTD = std(',phaseNames{j},'.maxPerp);'])
        
        % Ang Error Mean, STD
        eval([phaseNames{j},'.angErrMean = mean(',phaseNames{j},'.angErr);'])
        eval([phaseNames{j},'.angErrSTD = std(',phaseNames{j},'.angErr);'])
        
        else
            eval([phaseNames{j},'.normPx = [];'])
            eval([phaseNames{j},'.normPy = [];'])
            eval([phaseNames{j},'.normVx = [];'])
            eval([phaseNames{j},'.normVy = [];'])
            eval([phaseNames{j},'.PxMean = [];'])
            eval([phaseNames{j},'.PyMean = [];'])
            eval([phaseNames{j},'.VxMean = [];'])
            eval([phaseNames{j},'.VyMean = [];'])
            eval([phaseNames{j},'.PxSE = [];'])
            eval([phaseNames{j},'.PySE = [];'])
            eval([phaseNames{j},'.VxSE = [];'])
            eval([phaseNames{j},'.VySE = [];'])
            
            eval([phaseNames{j},'.estGainMean = [];'])
            eval([phaseNames{j},'.maxPerpMean = [];'])
            eval([phaseNames{j},'.maxPerpSTD = [];'])
            eval([phaseNames{j},'.angErrMean = [];'])
            eval([phaseNames{j},'.angErrSTD = [];'])
            
        end
    end
    
% %else %If a single subject
%     for j = 1:length(phaseNames)
%         
%         eval(['trialLength = size(',phaseNames{j},'.Px,2);'])
%         for ii = 1:trialLength
%             eval([phaseNames{j},'.PxMean(ii) = ',phaseNames{j},'.Px(ii);'])
%             eval([phaseNames{j},'.PyMean(ii) = ',phaseNames{j},'.Py(ii);'])
%             eval([phaseNames{j},'.VxMean(ii) = ',phaseNames{j},'.Vx(ii);'])
%             eval([phaseNames{j},'.VyMean(ii) = ',phaseNames{j},'.Vy(ii);'])
%         end    
%         eval([phaseNames{j},'.maxPerpMean = nanmean(',phaseNames{j},'.maxPerp);'])
%         eval([phaseNames{j},'.estGainMean = nanmean(',phaseNames{j},'.estGain);'])    
%     end
% %end

% Add Curl Gain Information
for j = 1:length(phaseNames)
    if j>=3 && j <=6
        eval([phaseNames{j},'.curlGain = -20;'])
    else
        eval([phaseNames{j},'.curlGain = 0;'])
    end
end
    

% Combine into Structure for Variable Out
for j = 1:length(phaseNames)
    eval(['D.',phaseNames{j},' = ',phaseNames{j},';'])  
end