% Pre Load Young Subect Data or run importSubjGroupDataWInterp

% Plot Average Trajectories
figure
subplot(2,4,1)
hold on
for i = 1:size(yData.EN1.Px,2)
    plot(yData.EN1.Px(:,i),yData.EN1.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.EN1.PxMean)
    xSE_Sq = [yData.EN1.PxMean(ii) - yData.EN1.PxSE(ii);
              yData.EN1.PxMean(ii) + yData.EN1.PxSE(ii);
              yData.EN1.PxMean(ii) + yData.EN1.PxSE(ii);
              yData.EN1.PxMean(ii) - yData.EN1.PxSE(ii);
              yData.EN1.PxMean(ii) - yData.EN1.PxSE(ii)];
    ySE_Sq = [yData.EN1.PyMean(ii) - yData.EN1.PySE(ii);
              yData.EN1.PyMean(ii) - yData.EN1.PySE(ii);
              yData.EN1.PyMean(ii) + yData.EN1.PySE(ii);
              yData.EN1.PyMean(ii) + yData.EN1.PySE(ii);
              yData.EN1.PyMean(ii) - yData.EN1.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EN1.PxMean,yData.EN1.PyMean)
% ciplotY(yData.EN1.PxMean-yData.EN1.PxSE,...
%     yData.EN1.PxMean+yData.EN1.PxSE,...
%     yData.EN1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EN1')

subplot(2,4,2)
hold on;
for i = 1:size(yData.EF1.Px,2)
    plot(yData.EF1.Px(:,i),yData.EF1.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.EF1.PxMean)
    xSE_Sq = [yData.EF1.PxMean(ii) - yData.EF1.PxSE(ii);
              yData.EF1.PxMean(ii) + yData.EF1.PxSE(ii);
              yData.EF1.PxMean(ii) + yData.EF1.PxSE(ii);
              yData.EF1.PxMean(ii) - yData.EF1.PxSE(ii);
              yData.EF1.PxMean(ii) - yData.EF1.PxSE(ii)];
    ySE_Sq = [yData.EF1.PyMean(ii) - yData.EF1.PySE(ii);
              yData.EF1.PyMean(ii) - yData.EF1.PySE(ii);
              yData.EF1.PyMean(ii) + yData.EF1.PySE(ii);
              yData.EF1.PyMean(ii) + yData.EF1.PySE(ii);
              yData.EF1.PyMean(ii) - yData.EF1.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EF1.PxMean,yData.EF1.PyMean)
%plot(SOL_EF1.x(:,1),SOL_EF1.x(:,2),'r')
% ciplotY(yData.EF1.PxMean-yData.EF1.PxSE,...
%     yData.EF1.PxMean+yData.EF1.PxSE,...
%     yData.EF1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EF1')
%set(gca,'Color',[1 1 .53]);

subplot(2,4,3)
hold on
for i = 1:size(yData.EF2.Px,2)
    plot(yData.EF2.Px(:,i),yData.EF2.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.EF2.PxMean)
    xSE_Sq = [yData.EF2.PxMean(ii) - yData.EF2.PxSE(ii);
              yData.EF2.PxMean(ii) + yData.EF2.PxSE(ii);
              yData.EF2.PxMean(ii) + yData.EF2.PxSE(ii);
              yData.EF2.PxMean(ii) - yData.EF2.PxSE(ii);
              yData.EF2.PxMean(ii) - yData.EF2.PxSE(ii)];
    ySE_Sq = [yData.EF2.PyMean(ii) - yData.EF2.PySE(ii);
              yData.EF2.PyMean(ii) - yData.EF2.PySE(ii);
              yData.EF2.PyMean(ii) + yData.EF2.PySE(ii);
              yData.EF2.PyMean(ii) + yData.EF2.PySE(ii);
              yData.EF2.PyMean(ii) - yData.EF2.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EF2.PxMean,yData.EF2.PyMean)
% ciplotY(yData.EF2.PxMean-yData.EF2.PxSE,...
%     yData.EF2.PxMean+yData.EF2.PxSE,...
%     yData.EF2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EF2')

subplot(2,4,4)
hold on;
for i = 1:size(yData.EN2.Px,2)
    plot(yData.EN2.Px(:,i),yData.EN2.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.EN2.PxMean)
    xSE_Sq = [yData.EN2.PxMean(ii) - yData.EN2.PxSE(ii);
              yData.EN2.PxMean(ii) + yData.EN2.PxSE(ii);
              yData.EN2.PxMean(ii) + yData.EN2.PxSE(ii);
              yData.EN2.PxMean(ii) - yData.EN2.PxSE(ii);
              yData.EN2.PxMean(ii) - yData.EN2.PxSE(ii)];
    ySE_Sq = [yData.EN2.PyMean(ii) - yData.EN2.PySE(ii);
              yData.EN2.PyMean(ii) - yData.EN2.PySE(ii);
              yData.EN2.PyMean(ii) + yData.EN2.PySE(ii);
              yData.EN2.PyMean(ii) + yData.EN2.PySE(ii);
              yData.EN2.PyMean(ii) - yData.EN2.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.EN2.PxMean,yData.EN2.PyMean)
%plot(SOL_EN2.x(:,1),SOL_EN2.x(:,2),'r')
% ciplotY(yData.EN2.PxMean-yData.EN2.PxSE,...
%     yData.EN2.PxMean+yData.EN2.PxSE,...
%     yData.EN2.PyMean,[0 0 1],0.5)
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EN2')
%set(gca,'Color',[1 1 .53]);

subplot(2,4,5)
hold on;
for i = 1:size(yData.LN1.Px,2)
    plot(yData.LN1.Px(:,i),yData.LN1.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.LN1.PxMean)
    xSE_Sq = [yData.LN1.PxMean(ii) - yData.LN1.PxSE(ii);
              yData.LN1.PxMean(ii) + yData.LN1.PxSE(ii);
              yData.LN1.PxMean(ii) + yData.LN1.PxSE(ii);
              yData.LN1.PxMean(ii) - yData.LN1.PxSE(ii);
              yData.LN1.PxMean(ii) - yData.LN1.PxSE(ii)];
    ySE_Sq = [yData.LN1.PyMean(ii) - yData.LN1.PySE(ii);
              yData.LN1.PyMean(ii) - yData.LN1.PySE(ii);
              yData.LN1.PyMean(ii) + yData.LN1.PySE(ii);
              yData.LN1.PyMean(ii) + yData.LN1.PySE(ii);
              yData.LN1.PyMean(ii) - yData.LN1.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LN1.PxMean,yData.LN1.PyMean)
% ciplotY(yData.LN1.PxMean-yData.LN1.PxSE,...
%     yData.LN1.PxMean+yData.LN1.PxSE,...
%     yData.LN1.PyMean,[0 0 1],0.5)
%plot(SOL_LN1.x(:,1),SOL_LN1.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LN1')
%set(gca,'Color',[1 1 .53]);

subplot(2,4,6)
hold on
for i = 1:size(yData.LF1.Px,2)
    plot(yData.LF1.Px(:,i),yData.LF1.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.LF1.PxMean)
    xSE_Sq = [yData.LF1.PxMean(ii) - yData.LF1.PxSE(ii);
              yData.LF1.PxMean(ii) + yData.LF1.PxSE(ii);
              yData.LF1.PxMean(ii) + yData.LF1.PxSE(ii);
              yData.LF1.PxMean(ii) - yData.LF1.PxSE(ii);
              yData.LF1.PxMean(ii) - yData.LF1.PxSE(ii)];
    ySE_Sq = [yData.LF1.PyMean(ii) - yData.LF1.PySE(ii);
              yData.LF1.PyMean(ii) - yData.LF1.PySE(ii);
              yData.LF1.PyMean(ii) + yData.LF1.PySE(ii);
              yData.LF1.PyMean(ii) + yData.LF1.PySE(ii);
              yData.LF1.PyMean(ii) - yData.LF1.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LF1.PxMean,yData.LF1.PyMean)
% ciplotY(yData.LF1.PxMean-yData.LF1.PxSE,...
%     yData.LF1.PxMean+yData.LF1.PxSE,...
%     yData.LF1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LF1')

subplot(2,4,7)
hold on;
for i = 1:size(yData.LF2.Px,2)
    p(2) = plot(yData.LF2.Px(:,i),yData.LF2.Py(:,i),'color',[.7 .7 .7]);
end
for ii = 1:length(yData.LF2.PxMean)
    xSE_Sq = [yData.LF2.PxMean(ii) - yData.LF2.PxSE(ii);
              yData.LF2.PxMean(ii) + yData.LF2.PxSE(ii);
              yData.LF2.PxMean(ii) + yData.LF2.PxSE(ii);
              yData.LF2.PxMean(ii) - yData.LF2.PxSE(ii);
              yData.LF2.PxMean(ii) - yData.LF2.PxSE(ii)];
    ySE_Sq = [yData.LF2.PyMean(ii) - yData.LF2.PySE(ii);
              yData.LF2.PyMean(ii) - yData.LF2.PySE(ii);
              yData.LF2.PyMean(ii) + yData.LF2.PySE(ii);
              yData.LF2.PyMean(ii) + yData.LF2.PySE(ii);
              yData.LF2.PyMean(ii) - yData.LF2.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
p(1) = plot(yData.LF2.PxMean,yData.LF2.PyMean);
%p(2) = plot(SOL_LF2.x(:,1),SOL_LF2.x(:,2),'r');
% ciplotY(yData.LF2.PxMean-yData.LF2.PxSE,...
%     yData.LF2.PxMean+yData.LF2.PxSE,...
%     yData.LF2.PyMean,[0 0 1],0.5)
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LF2')
%set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Subject Mean','Individual',...
    'Orientation','Horizontal',...
    'Location','South');
%set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

subplot(2,4,8)
hold on
for i = 1:size(yData.LN2.Px,2)
    plot(yData.LN2.Px(:,i),yData.LN2.Py(:,i),'color',[.7 .7 .7])
end
for ii = 1:length(yData.LN2.PxMean)
    xSE_Sq = [yData.LN2.PxMean(ii) - yData.LN2.PxSE(ii);
              yData.LN2.PxMean(ii) + yData.LN2.PxSE(ii);
              yData.LN2.PxMean(ii) + yData.LN2.PxSE(ii);
              yData.LN2.PxMean(ii) - yData.LN2.PxSE(ii);
              yData.LN2.PxMean(ii) - yData.LN2.PxSE(ii)];
    ySE_Sq = [yData.LN2.PyMean(ii) - yData.LN2.PySE(ii);
              yData.LN2.PyMean(ii) - yData.LN2.PySE(ii);
              yData.LN2.PyMean(ii) + yData.LN2.PySE(ii);
              yData.LN2.PyMean(ii) + yData.LN2.PySE(ii);
              yData.LN2.PyMean(ii) - yData.LN2.PySE(ii)];
    plot(xSE_Sq,ySE_Sq,'c')
end
plot(yData.LN2.PxMean,yData.LN2.PyMean)
% ciplotY(yData.LN2.PxMean-yData.LN2.PxSE,...
%     yData.LN2.PxMean+yData.LN2.PxSE,...
%     yData.LN2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LN2')

suptitle('Young Subject Data - Average Trajectories by Phase - SE X and Y')