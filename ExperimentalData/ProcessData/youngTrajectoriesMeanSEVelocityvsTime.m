% Pre Load Old Subect Data or run importSubjGroupDataWInterp


figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.Vx,2)
    plot(yData.EN1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VxMean)-1,yData.EN1.VxMean)
ciplot(yData.EN1.VxMean-yData.EN1.VxSE,...
    yData.EN1.VxMean+yData.EN1.VxSE,...
    0:length(yData.EN1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.Vx,2)
    plot(yData.LN1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VxMean)-1,yData.LN1.VxMean)
ciplot(yData.LN1.VxMean-yData.LN1.VxSE,...
    yData.LN1.VxMean+yData.LN1.VxSE,...
    0:length(yData.LN1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.Vx,2)
    plot(yData.EF1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VxMean)-1,yData.EF1.VxMean)
ciplot(yData.EF1.VxMean-yData.EF1.VxSE,...
    yData.EF1.VxMean+yData.EF1.VxSE,...
    0:length(yData.EF1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.Vx,2)
    plot(yData.LF1.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VxMean)-1,yData.LF1.VxMean)
ciplot(yData.LF1.VxMean-yData.LF1.VxSE,...
    yData.LF1.VxMean+yData.LF1.VxSE,...
    0:length(yData.LF1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.Vx,2)
    plot(yData.EF2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VxMean)-1,yData.EF2.VxMean)
ciplot(yData.EF2.VxMean-yData.EF2.VxSE,...
    yData.EF2.VxMean+yData.EF2.VxSE,...
    0:length(yData.EF2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.Vx,2)
    plot(yData.LF2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VxMean)-1,yData.LF2.VxMean)
ciplot(yData.LF2.VxMean-yData.LF2.VxSE,...
    yData.LF2.VxMean+yData.LF2.VxSE,...
    0:length(yData.LF2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.Vx,2)
    plot(yData.EN2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VxMean)-1,yData.EN2.VxMean)
ciplot(yData.EN2.VxMean-yData.EN2.VxSE,...
    yData.EN2.VxMean+yData.EN2.VxSE,...
    0:length(yData.EN2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.Vx,2)
    plot(yData.LN2.Vx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VxMean)-1,yData.LN2.VxMean)
ciplot(yData.LN2.VxMean-yData.LN2.VxSE,...
    yData.LN2.VxMean+yData.LN2.VxSE,...
    0:length(yData.LN2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.Vy,2)
    plot(yData.EN1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VyMean)-1,yData.EN1.VyMean)
ciplot(yData.EN1.VyMean-yData.EN1.VySE,...
    yData.EN1.VyMean+yData.EN1.VySE,...
    0:length(yData.EN1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.Vy,2)
    plot(yData.LN1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VyMean)-1,yData.LN1.VyMean)
ciplot(yData.LN1.VyMean-yData.LN1.VySE,...
    yData.LN1.VyMean+yData.LN1.VySE,...
    0:length(yData.LN1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.Vy,2)
    plot(yData.EF1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VyMean)-1,yData.EF1.VyMean)
ciplot(yData.EF1.VyMean-yData.EF1.VySE,...
    yData.EF1.VyMean+yData.EF1.VySE,...
    0:length(yData.EF1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.Vy,2)
    plot(yData.LF1.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VyMean)-1,yData.LF1.VyMean)
ciplot(yData.LF1.VyMean-yData.LF1.VySE,...
    yData.LF1.VyMean+yData.LF1.VySE,...
    0:length(yData.LF1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.Vy,2)
    plot(yData.EF2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VyMean)-1,yData.EF2.VyMean)
ciplot(yData.EF2.VyMean-yData.EF2.VySE,...
    yData.EF2.VyMean+yData.EF2.VySE,...
    0:length(yData.EF2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.Vy,2)
    plot(yData.LF2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VyMean)-1,yData.LF2.VyMean)
ciplot(yData.LF2.VyMean-yData.LF2.VySE,...
    yData.LF2.VyMean+yData.LF2.VySE,...
    0:length(yData.LF2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.Vy,2)
    plot(yData.EN2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VyMean)-1,yData.EN2.VyMean)
ciplot(yData.EN2.VyMean-yData.EN2.VySE,...
    yData.EN2.VyMean+yData.EN2.VySE,...
    0:length(yData.EN2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.Vy,2)
    plot(yData.LN2.Vy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VyMean)-1,yData.LN2.VyMean)
ciplot(yData.LN2.VyMean-yData.LN2.VySE,...
    yData.LN2.VyMean+yData.LN2.VySE,...
    0:length(yData.LN2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN2')

suptitle('Young Subject Data - Average Trajectories by Phase - Velocities')

%% Plot with Time-Normalized Trajectories
% Pre Load Old Subect Data or run importSubjGroupDataWInterp

figure
subplot(8,2,1)
hold on
for i = 1:size(yData.EN1.normVx,2)
    plot(yData.EN1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VxMean)-1,yData.EN1.VxMean)
ciplot(yData.EN1.VxMean-yData.EN1.VxSE,...
    yData.EN1.VxMean+yData.EN1.VxSE,...
    0:length(yData.EN1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(yData.LN1.normVx,2)
    plot(yData.LN1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VxMean)-1,yData.LN1.VxMean)
ciplot(yData.LN1.VxMean-yData.LN1.VxSE,...
    yData.LN1.VxMean+yData.LN1.VxSE,...
    0:length(yData.LN1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(yData.EF1.normVx,2)
    plot(yData.EF1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VxMean)-1,yData.EF1.VxMean)
ciplot(yData.EF1.VxMean-yData.EF1.VxSE,...
    yData.EF1.VxMean+yData.EF1.VxSE,...
    0:length(yData.EF1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(yData.LF1.normVx,2)
    plot(yData.LF1.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VxMean)-1,yData.LF1.VxMean)
ciplot(yData.LF1.VxMean-yData.LF1.VxSE,...
    yData.LF1.VxMean+yData.LF1.VxSE,...
    0:length(yData.LF1.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(yData.EF2.normVx,2)
    plot(yData.EF2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VxMean)-1,yData.EF2.VxMean)
ciplot(yData.EF2.VxMean-yData.EF2.VxSE,...
    yData.EF2.VxMean+yData.EF2.VxSE,...
    0:length(yData.EF2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(yData.LF2.normVx,2)
    plot(yData.LF2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VxMean)-1,yData.LF2.VxMean)
ciplot(yData.LF2.VxMean-yData.LF2.VxSE,...
    yData.LF2.VxMean+yData.LF2.VxSE,...
    0:length(yData.LF2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(yData.EN2.normVx,2)
    plot(yData.EN2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VxMean)-1,yData.EN2.VxMean)
ciplot(yData.EN2.VxMean-yData.EN2.VxSE,...
    yData.EN2.VxMean+yData.EN2.VxSE,...
    0:length(yData.EN2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(yData.LN2.normVx,2)
    plot(yData.LN2.normVx(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VxMean)-1,yData.LN2.VxMean)
ciplot(yData.LN2.VxMean-yData.LN2.VxSE,...
    yData.LN2.VxMean+yData.LN2.VxSE,...
    0:length(yData.LN2.VxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VX -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(yData.EN1.normVx,2)
    plot(yData.EN1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN1.VyMean)-1,yData.EN1.VyMean)
ciplot(yData.EN1.VyMean-yData.EN1.VySE,...
    yData.EN1.VyMean+yData.EN1.VySE,...
    0:length(yData.EN1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(yData.LN1.normVx,2)
    plot(yData.LN1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN1.VyMean)-1,yData.LN1.VyMean)
ciplot(yData.LN1.VyMean-yData.LN1.VySE,...
    yData.LN1.VyMean+yData.LN1.VySE,...
    0:length(yData.LN1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(yData.EF1.normVx,2)
    plot(yData.EF1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF1.VyMean)-1,yData.EF1.VyMean)
ciplot(yData.EF1.VyMean-yData.EF1.VySE,...
    yData.EF1.VyMean+yData.EF1.VySE,...
    0:length(yData.EF1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(yData.LF1.normVx,2)
    plot(yData.LF1.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF1.VyMean)-1,yData.LF1.VyMean)
ciplot(yData.LF1.VyMean-yData.LF1.VySE,...
    yData.LF1.VyMean+yData.LF1.VySE,...
    0:length(yData.LF1.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(yData.EF2.normVx,2)
    plot(yData.EF2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EF2.VyMean)-1,yData.EF2.VyMean)
ciplot(yData.EF2.VyMean-yData.EF2.VySE,...
    yData.EF2.VyMean+yData.EF2.VySE,...
    0:length(yData.EF2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(yData.LF2.normVx,2)
    plot(yData.LF2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LF2.VyMean)-1,yData.LF2.VyMean)
ciplot(yData.LF2.VyMean-yData.LF2.VySE,...
    yData.LF2.VyMean+yData.LF2.VySE,...
    0:length(yData.LF2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(yData.EN2.normVx,2)
    plot(yData.EN2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.EN2.VyMean)-1,yData.EN2.VyMean)
ciplot(yData.EN2.VyMean-yData.EN2.VySE,...
    yData.EN2.VyMean+yData.EN2.VySE,...
    0:length(yData.EN2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(yData.LN2.normVx,2)
    plot(yData.LN2.normVy(:,i),'color',[.7 .7 .7])
end
plot(0:length(yData.LN2.VyMean)-1,yData.LN2.VyMean)
ciplot(yData.LN2.VyMean-yData.LN2.VySE,...
    yData.LN2.VyMean+yData.LN2.VySE,...
    0:length(yData.LN2.VyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('VY -- LN2')

suptitle('Young Subject Data - Average Trajectories by Phase - Time-Normalized Velocities')