% Pre Load Old Subect Data or run importSubjGroupDataWInterp

figure
subplot(8,2,1)
hold on
for i = 1:15
    plot(oData.EN1.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EN1.PxMean)-1,oData.EN1.PxMean)
ciplot(oData.EN1.PxMean-oData.EN1.PxSE,...
    oData.EN1.PxMean+oData.EN1.PxSE,...
    0:length(oData.EN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN1')

subplot(8,2,3)
hold on
for i = 1:15
    plot(oData.LN1.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LN1.PxMean)-1,oData.LN1.PxMean)
ciplot(oData.LN1.PxMean-oData.LN1.PxSE,...
    oData.LN1.PxMean+oData.LN1.PxSE,...
    0:length(oData.LN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN1')

subplot(8,2,5)
hold on
for i = 1:15
    plot(oData.EF1.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EF1.PxMean)-1,oData.EF1.PxMean)
ciplot(oData.EF1.PxMean-oData.EF1.PxSE,...
    oData.EF1.PxMean+oData.EF1.PxSE,...
    0:length(oData.EF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF1')

subplot(8,2,7)
hold on
for i = 1:15
    plot(oData.LF1.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LF1.PxMean)-1,oData.LF1.PxMean)
ciplot(oData.LF1.PxMean-oData.LF1.PxSE,...
    oData.LF1.PxMean+oData.LF1.PxSE,...
    0:length(oData.LF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF1')

subplot(8,2,9)
hold on
for i = 1:15
    plot(oData.EF2.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EF2.PxMean)-1,oData.EF2.PxMean)
ciplot(oData.EF2.PxMean-oData.EF2.PxSE,...
    oData.EF2.PxMean+oData.EF2.PxSE,...
    0:length(oData.EF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF2')

subplot(8,2,11)
hold on
for i = 1:15
    plot(oData.LF2.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LF2.PxMean)-1,oData.LF2.PxMean)
ciplot(oData.LF2.PxMean-oData.LF2.PxSE,...
    oData.LF2.PxMean+oData.LF2.PxSE,...
    0:length(oData.LF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF2')

subplot(8,2,13)
hold on
for i = 1:15
    plot(oData.EN2.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EN2.PxMean)-1,oData.EN2.PxMean)
ciplot(oData.EN2.PxMean-oData.EN2.PxSE,...
    oData.EN2.PxMean+oData.EN2.PxSE,...
    0:length(oData.EN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN2')

subplot(8,2,15)
hold on
for i = 1:15
    plot(oData.LN2.Px(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LN2.PxMean)-1,oData.LN2.PxMean)
ciplot(oData.LN2.PxMean-oData.LN2.PxSE,...
    oData.LN2.PxMean+oData.LN2.PxSE,...
    0:length(oData.LN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:15
    plot(oData.EN1.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EN1.PyMean)-1,oData.EN1.PyMean)
ciplot(oData.EN1.PyMean-oData.EN1.PySE,...
    oData.EN1.PyMean+oData.EN1.PySE,...
    0:length(oData.EN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN1')

subplot(8,2,4)
hold on
for i = 1:15
    plot(oData.LN1.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LN1.PyMean)-1,oData.LN1.PyMean)
ciplot(oData.LN1.PyMean-oData.LN1.PySE,...
    oData.LN1.PyMean+oData.LN1.PySE,...
    0:length(oData.LN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN1')

subplot(8,2,6)
hold on
for i = 1:15
    plot(oData.EF1.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EF1.PyMean)-1,oData.EF1.PyMean)
ciplot(oData.EF1.PyMean-oData.EF1.PySE,...
    oData.EF1.PyMean+oData.EF1.PySE,...
    0:length(oData.EF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF1')

subplot(8,2,8)
hold on
for i = 1:15
    plot(oData.LF1.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LF1.PyMean)-1,oData.LF1.PyMean)
ciplot(oData.LF1.PyMean-oData.LF1.PySE,...
    oData.LF1.PyMean+oData.LF1.PySE,...
    0:length(oData.LF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF1')

subplot(8,2,10)
hold on
for i = 1:15
    plot(oData.EF2.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EF2.PyMean)-1,oData.EF2.PyMean)
ciplot(oData.EF2.PyMean-oData.EF2.PySE,...
    oData.EF2.PyMean+oData.EF2.PySE,...
    0:length(oData.EF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF2')

subplot(8,2,12)
hold on
for i = 1:15
    plot(oData.LF2.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LF2.PyMean)-1,oData.LF2.PyMean)
ciplot(oData.LF2.PyMean-oData.LF2.PySE,...
    oData.LF2.PyMean+oData.LF2.PySE,...
    0:length(oData.LF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF2')

subplot(8,2,14)
hold on
for i = 1:15
    plot(oData.EN2.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.EN2.PyMean)-1,oData.EN2.PyMean)
ciplot(oData.EN2.PyMean-oData.EN2.PySE,...
    oData.EN2.PyMean+oData.EN2.PySE,...
    0:length(oData.EN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN2')

subplot(8,2,16)
hold on
for i = 1:15
    plot(oData.LN2.Py(i,:),'color',[.7 .7 .7])
end
plot(0:length(oData.LN2.PyMean)-1,oData.LN2.PyMean)
ciplot(oData.LN2.PyMean-oData.LN2.PySE,...
    oData.LN2.PyMean+oData.LN2.PySE,...
    0:length(oData.LN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN2')

suptitle('Old Subject Data - Average Trajectories by Phase')

%% Plot with Time-Normalized Trajectories
% Pre Load Old Subect Data or run importSubjGroupDataWInterp

figure
subplot(8,2,1)
hold on
for i = 1:size(oData.EN1.normPx,2)
    plot(oData.EN1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EN1.PxMean)-1,oData.EN1.PxMean)
ciplot(oData.EN1.PxMean-oData.EN1.PxSE,...
    oData.EN1.PxMean+oData.EN1.PxSE,...
    0:length(oData.EN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN1')

subplot(8,2,3)
hold on
for i = 1:size(oData.LN1.normPx,2)
    plot(oData.LN1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LN1.PxMean)-1,oData.LN1.PxMean)
ciplot(oData.LN1.PxMean-oData.LN1.PxSE,...
    oData.LN1.PxMean+oData.LN1.PxSE,...
    0:length(oData.LN1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN1')

subplot(8,2,5)
hold on
for i = 1:size(oData.EF1.normPx,2)
    plot(oData.EF1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EF1.PxMean)-1,oData.EF1.PxMean)
ciplot(oData.EF1.PxMean-oData.EF1.PxSE,...
    oData.EF1.PxMean+oData.EF1.PxSE,...
    0:length(oData.EF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF1')

subplot(8,2,7)
hold on
for i = 1:size(oData.LF1.normPx,2)
    plot(oData.LF1.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LF1.PxMean)-1,oData.LF1.PxMean)
ciplot(oData.LF1.PxMean-oData.LF1.PxSE,...
    oData.LF1.PxMean+oData.LF1.PxSE,...
    0:length(oData.LF1.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF1')

subplot(8,2,9)
hold on
for i = 1:size(oData.EF2.normPx,2)
    plot(oData.EF2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EF2.PxMean)-1,oData.EF2.PxMean)
ciplot(oData.EF2.PxMean-oData.EF2.PxSE,...
    oData.EF2.PxMean+oData.EF2.PxSE,...
    0:length(oData.EF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EF2')

subplot(8,2,11)
hold on
for i = 1:size(oData.LF2.normPx,2)
    plot(oData.LF2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LF2.PxMean)-1,oData.LF2.PxMean)
ciplot(oData.LF2.PxMean-oData.LF2.PxSE,...
    oData.LF2.PxMean+oData.LF2.PxSE,...
    0:length(oData.LF2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LF2')

subplot(8,2,13)
hold on
for i = 1:size(oData.EN2.normPx,2)
    plot(oData.EN2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EN2.PxMean)-1,oData.EN2.PxMean)
ciplot(oData.EN2.PxMean-oData.EN2.PxSE,...
    oData.EN2.PxMean+oData.EN2.PxSE,...
    0:length(oData.EN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- EN2')

subplot(8,2,15)
hold on
for i = 1:size(oData.LN2.normPx,2)
    plot(oData.LN2.normPx(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LN2.PxMean)-1,oData.LN2.PxMean)
ciplot(oData.LN2.PxMean-oData.LN2.PxSE,...
    oData.LN2.PxMean+oData.LN2.PxSE,...
    0:length(oData.LN2.PxMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('X -- LN2')

% Y-Data
subplot(8,2,2)
hold on
for i = 1:size(oData.EN1.normPx,2)
    plot(oData.EN1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EN1.PyMean)-1,oData.EN1.PyMean)
ciplot(oData.EN1.PyMean-oData.EN1.PySE,...
    oData.EN1.PyMean+oData.EN1.PySE,...
    0:length(oData.EN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN1')

subplot(8,2,4)
hold on
for i = 1:size(oData.LN1.normPx,2)
    plot(oData.LN1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LN1.PyMean)-1,oData.LN1.PyMean)
ciplot(oData.LN1.PyMean-oData.LN1.PySE,...
    oData.LN1.PyMean+oData.LN1.PySE,...
    0:length(oData.LN1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN1')

subplot(8,2,6)
hold on
for i = 1:size(oData.EF1.normPx,2)
    plot(oData.EF1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EF1.PyMean)-1,oData.EF1.PyMean)
ciplot(oData.EF1.PyMean-oData.EF1.PySE,...
    oData.EF1.PyMean+oData.EF1.PySE,...
    0:length(oData.EF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF1')

subplot(8,2,8)
hold on
for i = 1:size(oData.LF1.normPx,2)
    plot(oData.LF1.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LF1.PyMean)-1,oData.LF1.PyMean)
ciplot(oData.LF1.PyMean-oData.LF1.PySE,...
    oData.LF1.PyMean+oData.LF1.PySE,...
    0:length(oData.LF1.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF1')

subplot(8,2,10)
hold on
for i = 1:size(oData.EF2.normPx,2)
    plot(oData.EF2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EF2.PyMean)-1,oData.EF2.PyMean)
ciplot(oData.EF2.PyMean-oData.EF2.PySE,...
    oData.EF2.PyMean+oData.EF2.PySE,...
    0:length(oData.EF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EF2')

subplot(8,2,12)
hold on
for i = 1:size(oData.LF2.normPx,2)
    plot(oData.LF2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LF2.PyMean)-1,oData.LF2.PyMean)
ciplot(oData.LF2.PyMean-oData.LF2.PySE,...
    oData.LF2.PyMean+oData.LF2.PySE,...
    0:length(oData.LF2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LF2')

subplot(8,2,14)
hold on
for i = 1:size(oData.EN2.normPx,2)
    plot(oData.EN2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.EN2.PyMean)-1,oData.EN2.PyMean)
ciplot(oData.EN2.PyMean-oData.EN2.PySE,...
    oData.EN2.PyMean+oData.EN2.PySE,...
    0:length(oData.EN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- EN2')

subplot(8,2,16)
hold on
for i = 1:size(oData.LN2.normPx,2)
    plot(oData.LN2.normPy(:,i),'color',[.7 .7 .7])
end
plot(0:length(oData.LN2.PyMean)-1,oData.LN2.PyMean)
ciplot(oData.LN2.PyMean-oData.LN2.PySE,...
    oData.LN2.PyMean+oData.LN2.PySE,...
    0:length(oData.LN2.PyMean)-1,[0 0 1],0.5)
hold off
xlim([0 300])
title('Y -- LN2')

suptitle('Old Subject Data - Average Trajectories by Phase')