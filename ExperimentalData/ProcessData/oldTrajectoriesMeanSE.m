% Pre Load Young Subect Data or run importSubjGroupDataWInterp

% Plot Average Trajectories
figure
subplot(2,4,1)
hold on
for i = 1:size(oData.EN1.Px,2)
    plot(oData.EN1.Px(:,i),oData.EN1.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.EN1.PxMean,oData.EN1.PyMean)
ciplotY(oData.EN1.PxMean-oData.EN1.PxSE,...
    oData.EN1.PxMean+oData.EN1.PxSE,...
    oData.EN1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EN1')

subplot(2,4,2)
hold on;
for i = 1:size(oData.EF1.Px,2)
    plot(oData.EF1.Px(:,i),oData.EF1.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.EF1.PxMean,oData.EF1.PyMean)
%plot(SOL_EF1.x(:,1),SOL_EF1.x(:,2),'r')
ciplotY(oData.EF1.PxMean-oData.EF1.PxSE,...
    oData.EF1.PxMean+oData.EF1.PxSE,...
    oData.EF1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EF1')
%set(gca,'Color',[1 1 .53]);

subplot(2,4,3)
hold on
for i = 1:size(oData.EF2.Px,2)
    plot(oData.EF2.Px(:,i),oData.EF2.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.EF2.PxMean,oData.EF2.PyMean)
ciplotY(oData.EF2.PxMean-oData.EF2.PxSE,...
    oData.EF2.PxMean+oData.EF2.PxSE,...
    oData.EF2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EF2')

subplot(2,4,4)
hold on;
for i = 1:size(oData.EN2.Px,2)
    plot(oData.EN2.Px(:,i),oData.EN2.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.EN2.PxMean,oData.EN2.PyMean)
%plot(SOL_EN2.x(:,1),SOL_EN2.x(:,2),'r')
ciplotY(oData.EN2.PxMean-oData.EN2.PxSE,...
    oData.EN2.PxMean+oData.EN2.PxSE,...
    oData.EN2.PyMean,[0 0 1],0.5)
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EN2')
%set(gca,'Color',[1 1 .53]);

subplot(2,4,5)
hold on;
for i = 1:size(oData.LN1.Px,2)
    plot(oData.LN1.Px(:,i),oData.LN1.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.LN1.PxMean,oData.LN1.PyMean)
ciplotY(oData.LN1.PxMean-oData.LN1.PxSE,...
    oData.LN1.PxMean+oData.LN1.PxSE,...
    oData.LN1.PyMean,[0 0 1],0.5)
%plot(SOL_LN1.x(:,1),SOL_LN1.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LN1')
%set(gca,'Color',[1 1 .53]);

subplot(2,4,6)
hold on
for i = 1:size(oData.LF1.Px,2)
    plot(oData.LF1.Px(:,i),oData.LF1.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.LF1.PxMean,oData.LF1.PyMean)
ciplotY(oData.LF1.PxMean-oData.LF1.PxSE,...
    oData.LF1.PxMean+oData.LF1.PxSE,...
    oData.LF1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LF1')

subplot(2,4,7)
hold on;
for i = 1:size(oData.LF2.Px,2)
    p(2) = plot(oData.LF2.Px(:,i),oData.LF2.Py(:,i),'color',[.7 .7 .7]);
end
p(1) = plot(oData.LF2.PxMean,oData.LF2.PyMean);
%p(2) = plot(SOL_LF2.x(:,1),SOL_LF2.x(:,2),'r');
ciplotY(oData.LF2.PxMean-oData.LF2.PxSE,...
    oData.LF2.PxMean+oData.LF2.PxSE,...
    oData.LF2.PyMean,[0 0 1],0.5)
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LF2')
%set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Subject Mean','Individual',...
    'Orientation','Horizontal',...
    'Location','South');
%set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

subplot(2,4,8)
hold on
for i = 1:size(oData.LN2.Px,2)
    plot(oData.LN2.Px(:,i),oData.LN2.Py(:,i),'color',[.7 .7 .7])
end
plot(oData.LN2.PxMean,oData.LN2.PyMean)
ciplotY(oData.LN2.PxMean-oData.LN2.PxSE,...
    oData.LN2.PxMean+oData.LN2.PxSE,...
    oData.LN2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LN2')

suptitle('Old Subject Data - Average Trajectories by Phase')