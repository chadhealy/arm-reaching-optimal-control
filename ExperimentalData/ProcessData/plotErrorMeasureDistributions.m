phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};% Defined By Helen

% Max Perp Error
figure
for j = 1:length(phaseNames)
    subplot(4,2,j)
    eval(['[nx,xout] = hist(yData.',phaseNames{j},'.maxPerp,30);'])
    bar(xout,nx/sum(nx))
    xTemp = get(gca,'XTick');
    xTemp = xTemp(1):0.001:xTemp(end);
    eval(['XY = pdf(''norm'',xTemp,yData.',phaseNames{j},...
        '.maxPerpMean,yData.',phaseNames{j},...
        '.maxPerpSTD);'])
    XY = XY/(max(XY)/max(nx/sum(nx)));
    hold on
    plot(xTemp,XY,'r')
    hold off
    eval(['text(max(get(gca,''XTick'')),max(get(gca,''YTick'')),[''n = '',num2str(length(yData.',...
        phaseNames{j},'.maxPerp))],''FontSize'',10,''HorizontalAlignment'',''Right'',''VerticalAlignment'',''Top'');'])
    
%     eval(['yTemp = 1/(sqrt(2*pi)*yData.',phaseNames{j},...
%         '.maxPerpSTD)*exp((-1*(xTemp - yData.',phaseNames{j},...
%         '.maxPerpMean).^2)/(2*yData.',phaseNames{j},'.maxPerpSTD^2));'])
%     hold on
%     plot(xTemp,yTemp,'r')
%     hold off
     title(phaseNames{j})
end
suptitle('Maximum Perpendicular Error Distributions by Phase (Older Adults)')

% Angular Error
figure
for j = 1:length(phaseNames)
    subplot(4,2,j)
    eval(['[nx,xout] = hist(yData.',phaseNames{j},'.angErr,30);'])
    bar(xout,nx/sum(nx))
    xTemp = get(gca,'XTick');
    xTemp = xTemp(1):0.001:xTemp(end);
    eval(['XY = pdf(''norm'',xTemp,yData.',phaseNames{j},...
        '.angErrMean,yData.',phaseNames{j},...
        '.angErrSTD);'])
    XY = XY/(max(XY)/max(nx/sum(nx)));
    hold on
    plot(xTemp,XY,'r')
    hold off
    eval(['text(max(get(gca,''XTick'')),max(get(gca,''YTick'')),[''n = '',num2str(length(yData.',...
        phaseNames{j},'.angErr))],''FontSize'',10,''HorizontalAlignment'',''Right'',''VerticalAlignment'',''Top'');'])
    title(phaseNames{j})
end

suptitle('Angular Error Distributions by Phase (Older Adults)')