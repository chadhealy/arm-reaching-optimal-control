function varargout = displaySubjectIndividualPhaseResults(varargin)
% DISPLAYSUBJECTINDIVIDUALPHASERESULTS MATLAB code for displaySubjectIndividualPhaseResults.fig
%      DISPLAYSUBJECTINDIVIDUALPHASERESULTS, by itself, creates a new DISPLAYSUBJECTINDIVIDUALPHASERESULTS or raises the existing
%      singleton*.
%
%      H = DISPLAYSUBJECTINDIVIDUALPHASERESULTS returns the handle to a new DISPLAYSUBJECTINDIVIDUALPHASERESULTS or the handle to
%      the existing singleton*.
%
%      DISPLAYSUBJECTINDIVIDUALPHASERESULTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAYSUBJECTINDIVIDUALPHASERESULTS.M with the given input arguments.
%
%      DISPLAYSUBJECTINDIVIDUALPHASERESULTS('Property','Value',...) creates a new DISPLAYSUBJECTINDIVIDUALPHASERESULTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before displaySubjectIndividualPhaseResults_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to displaySubjectIndividualPhaseResults_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help displaySubjectIndividualPhaseResults

% Last Modified by GUIDE v2.5 28-May-2014 23:35:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @displaySubjectIndividualPhaseResults_OpeningFcn, ...
                   'gui_OutputFcn',  @displaySubjectIndividualPhaseResults_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before displaySubjectIndividualPhaseResults is made visible.
function displaySubjectIndividualPhaseResults_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to displaySubjectIndividualPhaseResults (see VARARGIN)

% Choose default command line output for displaySubjectIndividualPhaseResults
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes displaySubjectIndividualPhaseResults wait for user response (see UIRESUME)
% uiwait(handles.figure1);
files = dir('DataFiles/IndividualPhases/*.mat');
bbb = 1;
for a = 1:length(files)
    load(['DataFiles/IndividualPhases/',num2str(files(a).name)]);
    if exist('yData')
        handles.fileNames{bbb} = files(a).name;
        bbb = bbb + 1;
        clear yData
    elseif exist('oData')
        handles.fileNames{bbb} = files(a).name;
        bbb = bbb + 1;
        clear oData
    end
end
if ~isfield(handles,'fileNames')
    error('No Data Files Found')
end
set(handles.popupmenu1,'String',handles.fileNames)
set(handles.popupmenu1,'Value',1)
load(['DataFiles/IndividualPhases/',num2str(handles.fileNames{1})]);

if exist('oData')
    set(handles.ageGroup,'String','OLD')
    Data = oData;
    SOL = Old;
    clear oData Old
elseif exist('yData')
    set(handles.ageGroup,'String','YOUNG')
    Data = yData;
    SOL = Young;
    clear yData Young
end

axes(handles.axes1)
hold on
plot(Data.EN1.PxMean,Data.EN1.PyMean)
if length(Data.EN1.PxMean) == length(Data.EN1.PxSE)
    ciplotY(Data.EN1.PxMean-Data.EN1.PxSE,...
        Data.EN1.PxMean+Data.EN1.PxSE,...
        Data.EN1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EN1')
    plot(SOL.EN1.Traj.x(:,1),SOL.EN1.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EN1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes2)
hold on;
plot(Data.EF1.PxMean,Data.EF1.PyMean)
if length(Data.EF1.PxMean) == length(Data.EF1.PxSE)
    ciplotY(Data.EF1.PxMean-Data.EF1.PxSE,...
        Data.EF1.PxMean+Data.EF1.PxSE,...
        Data.EF1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EF1')
    plot(SOL.EF1.Traj.x(:,1),SOL.EF1.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EF1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes3)
hold on
plot(Data.EF2.PxMean,Data.EF2.PyMean)
if length(Data.EF2.PxMean) == length(Data.EF2.PxSE)
    ciplotY(Data.EF2.PxMean-Data.EF2.PxSE,...
        Data.EF2.PxMean+Data.EF2.PxSE,...
        Data.EF2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EF2')
    plot(SOL.EF2.Traj.x(:,1),SOL.EF2.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EF2')
set(gca,'Color',[1 1 .53]);

axes(handles.axes4)
hold on;
plot(Data.EN2.PxMean,Data.EN2.PyMean)
if length(Data.EN2.PxMean) == length(Data.EN2.PxSE)
    ciplotY(Data.EN2.PxMean-Data.EN2.PxSE,...
        Data.EN2.PxMean+Data.EN2.PxSE,...
        Data.EN2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EN2')
    plot(SOL.EN2.Traj.x(:,1),SOL.EN2.Traj.x(:,2),'r')
end
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EN2')
set(gca,'Color',[1 1 .53]);

axes(handles.axes5)
hold on;
plot(Data.LN1.PxMean,Data.LN1.PyMean)
if length(Data.LN1.PxMean) == length(Data.LN1.PxSE)
    ciplotY(Data.LN1.PxMean-Data.LN1.PxSE,...
        Data.LN1.PxMean+Data.LN1.PxSE,...
        Data.LN1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LN1')
    plot(SOL.LN1.Traj.x(:,1),SOL.LN1.Traj.x(:,2),'r')
end
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTick',[-0.1 -0.05 0 0.05 0.1])
set(handles.axes1,'YTick',[-.1 -.05 0 .05 .1])
set(handles.axes1,'YTickLabel',[-.1 -.05 0 .05 .1])
title('LN1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes6)
hold on
plot(Data.LF1.PxMean,Data.LF1.PyMean)
if length(Data.LF1.PxMean) == length(Data.LF1.PxSE)
    ciplotY(Data.LF1.PxMean-Data.LF1.PxSE,...
        Data.LF1.PxMean+Data.LF1.PxSE,...
        Data.LF1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LF1')
    plot(SOL.LF1.Traj.x(:,1),SOL.LF1.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('LF1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes7)
hold on;
p(1) = plot(Data.LF2.PxMean,Data.LF2.PyMean);
if length(Data.LF2.PxMean) == length(Data.LF2.PxSE)
    ciplotY(Data.LF2.PxMean-Data.LF2.PxSE,...
        Data.LF2.PxMean+Data.LF2.PxSE,...
        Data.LF2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LF2')
    p(2) = plot(SOL.LF2.Traj.x(:,1),SOL.LF2.Traj.x(:,2),'r');
end
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('LF2')
set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Experimental','Model Results',...
    'Orientation','Horizontal',...
    'Location','South');
set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

axes(handles.axes8)
hold on
plot(Data.LN2.PxMean,Data.LN2.PyMean)
if length(Data.LN2.PxMean) == length(Data.LN2.PxSE)
    ciplotY(Data.LN2.PxMean-Data.LN2.PxSE,...
        Data.LN2.PxMean+Data.LN2.PxSE,...
        Data.LN2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LN2')
    plot(SOL.LN2.Traj.x(:,1),SOL.LN2.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('LN2')
set(gca,'Color',[1 1 .53]);

%phases = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};
% hack for now
if isfield(SOL,'LN2')
    phases = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};
    bigPhases = {' ','Null-1',' ','Force-1',' ','Force-2',' ', 'Null-2',' '};
else
    phases = {'EN1','EF1','EF2','EN2'};
    bigPhases = {'Null-1','Force-1','Force-2','Null-2'};
end
%bigPhases = {' ','Null-1',' ','Force-1',' ','Force-2',' ', 'Null-2',' '};
axes(handles.axes9)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(1,1),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes9,'XTick',1:length(phases))
%set(handles.axes9,'XTickLabel',phases)
set(handles.axes9,'XTickLabel',[])
set(handles.axes9,'YScale','log')
title('Q_1_1')
grid on

axes(handles.axes10)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(2,2),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes10,'XTick',1:length(phases))
%set(handles.axes10,'XTickLabel',phases)
set(handles.axes10,'XTickLabel',[])
set(handles.axes10,'YScale','log')
title('Q_2_2')
grid on

axes(handles.axes11)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(3,3),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes11,'XTick',1:length(phases))
%set(handles.axes11,'XTickLabel',phases)
set(handles.axes11,'XTickLabel',[])
set(handles.axes11,'YScale','log')
title('Q_3_3')
grid on

axes(handles.axes12)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(5,5),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes12,'XTick',1:length(phases))
%set(handles.axes12,'XTickLabel',phases)
set(handles.axes12,'XTickLabel',[])
set(handles.axes12,'YScale','log')
title('Q_5_5, Q_6_6')
grid on

axes(handles.axes13)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.R(1,1),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes13,'XTick',1:length(phases))
%set(handles.axes13,'XTickLabel',phases)
set(handles.axes13,'XTickLabel',[])
set(handles.axes13,'YScale','log')
title('R_1_1, R_2_2')
grid on

axes(handles.axes14)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Phi(1,1),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes14,'XTick',1:length(phases))
%set(handles.axes14,'XTickLabel',phases)
set(handles.axes14,'XTickLabel',[])
set(handles.axes14,'YScale','log')
title('\Phi_1_1, \Phi_2_2')
grid on

axes(handles.axes15)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Phi(3,3),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes15,'XTick',1:length(phases))
set(handles.axes15,'XTickLabel',bigPhases)
set(handles.axes15,'YScale','log')
title('\Phi_3_3, \Phi_4_4')
grid on

axes(handles.axes16)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Phi(5,5),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
grid on
set(handles.axes16,'XTick',1:length(phases))
set(handles.axes16,'XTickLabel',bigPhases)
set(handles.axes16,'YScale','log')
title('\Phi_5_5, \Phi_6_6')

axes(handles.axes17)
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.estGain,''o'');'])
    eval(['plot(j,SOL.',phases{j},'.Params.curlGain,''rx'');'])
end
hold off
set(kk,'MarkerFaceColor','b')
xlim([0 length(phases)+1])
set(handles.axes17,'XTick',1:length(phases))
set(handles.axes17,'XTickLabel',bigPhases)
title('Estimated Gain')
grid on

% Update handles structure
guidata(hObject, handles);

%-------------END OF INITIALIZATION------------------------%
%----------------------------------------------------------%
%----------------------------------------------------------%
%----------------------------------------------------------%


% --- Outputs from this function are returned to the command line.
function varargout = displaySubjectIndividualPhaseResults_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

ind = get(handles.popupmenu1,'Value');
file = handles.fileNames{ind};
load(['DataFiles/IndividualPhases/',num2str(file)]);

if exist('oData')
    set(handles.ageGroup,'String','OLD')
    clear Data SOL
    Data = oData;
    SOL = Old;
    clear oData Old
elseif exist('yData')
    set(handles.ageGroup,'String','YOUNG')
    clear Data SOL
    Data = yData;
    SOL = Young;
    clear yData Young
end

axes(handles.axes1)
plot(Data.EN1.PxMean,Data.EN1.PyMean)
hold on
if length(Data.EN1.PxMean) == length(Data.EN1.PxSE)
    ciplotY(Data.EN1.PxMean-Data.EN1.PxSE,...
        Data.EN1.PxMean+Data.EN1.PxSE,...
        Data.EN1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EN1')
    plot(SOL.EN1.Traj.x(:,1),SOL.EN1.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EN1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes2)
plot(Data.EF1.PxMean,Data.EF1.PyMean)
hold on;
if length(Data.EF1.PxMean) == length(Data.EF1.PxSE)
    ciplotY(Data.EF1.PxMean-Data.EF1.PxSE,...
        Data.EF1.PxMean+Data.EF1.PxSE,...
        Data.EF1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EF1')
    plot(SOL.EF1.Traj.x(:,1),SOL.EF1.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EF1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes3)
plot(Data.EF2.PxMean,Data.EF2.PyMean)
hold on
if length(Data.EF2.PxMean) == length(Data.EF2.PxSE)
    ciplotY(Data.EF2.PxMean-Data.EF2.PxSE,...
        Data.EF2.PxMean+Data.EF2.PxSE,...
        Data.EF2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EF2')
    plot(SOL.EF2.Traj.x(:,1),SOL.EF2.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EF2')
set(gca,'Color',[1 1 .53]);

axes(handles.axes4)
plot(Data.EN2.PxMean,Data.EN2.PyMean)
hold on;
if length(Data.EN2.PxMean) == length(Data.EN2.PxSE)
    ciplotY(Data.EN2.PxMean-Data.EN2.PxSE,...
        Data.EN2.PxMean+Data.EN2.PxSE,...
        Data.EN2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'EN2')
    plot(SOL.EN2.Traj.x(:,1),SOL.EN2.Traj.x(:,2),'r')
end
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EN2')
set(gca,'Color',[1 1 .53]);

axes(handles.axes5)
plot(Data.LN1.PxMean,Data.LN1.PyMean)
hold on;
if length(Data.LN1.PxMean) == length(Data.LN1.PxSE)
    ciplotY(Data.LN1.PxMean-Data.LN1.PxSE,...
        Data.LN1.PxMean+Data.LN1.PxSE,...
        Data.LN1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LN1')
    plot(SOL.LN1.Traj.x(:,1),SOL.LN1.Traj.x(:,2),'r')
end
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTick',[-0.1 -0.05 0 0.05 0.1])
set(handles.axes1,'YTick',[-.1 -.05 0 .05 .1])
set(handles.axes1,'YTickLabel',[-.1 -.05 0 .05 .1])
title('LN1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes6)
plot(Data.LF1.PxMean,Data.LF1.PyMean)
hold on
if length(Data.LF1.PxMean) == length(Data.LF1.PxSE)
    ciplotY(Data.LF1.PxMean-Data.LF1.PxSE,...
        Data.LF1.PxMean+Data.LF1.PxSE,...
        Data.LF1.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LF1')
    plot(SOL.LF1.Traj.x(:,1),SOL.LF1.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('LF1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes7)
p(1) = plot(Data.LF2.PxMean,Data.LF2.PyMean);
hold on;
if length(Data.LF2.PxMean) == length(Data.LF2.PxSE)
    ciplotY(Data.LF2.PxMean-Data.LF2.PxSE,...
        Data.LF2.PxMean+Data.LF2.PxSE,...
        Data.LF2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LF2')
    p(2) = plot(SOL.LF2.Traj.x(:,1),SOL.LF2.Traj.x(:,2),'r');
end
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('LF2')
set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Experimental','Model Results',...
    'Orientation','Horizontal',...
    'Location','South');
set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

axes(handles.axes8)
plot(Data.LN2.PxMean,Data.LN2.PyMean)
hold on
if length(Data.LN2.PxMean) == length(Data.LN2.PxSE)
    ciplotY(Data.LN2.PxMean-Data.LN2.PxSE,...
        Data.LN2.PxMean+Data.LN2.PxSE,...
        Data.LN2.PyMean,[0 0 1],0.5)
end
if isfield(SOL,'LN2')
    plot(SOL.LN2.Traj.x(:,1),SOL.LN2.Traj.x(:,2),'r')
end
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('LN2')
set(gca,'Color',[1 1 .53]);

%phases = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};
% hack for now
if isfield(SOL,'LN2')
    phases = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};
    bigPhases = {' ','Null-1',' ','Force-1',' ','Force-2',' ', 'Null-2',' '};
else
    phases = {'EN1','EF1','EF2','EN2'};
    bigPhases = {'Null-1','Force-1','Force-2','Null-2'};
end
%bigPhases = {' ','Null-1',' ','Force-1',' ','Force-2',' ', 'Null-2',' '};
axes(handles.axes9)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(1,1),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes9,'XTick',1:length(phases))
%set(handles.axes9,'XTickLabel',phases)
set(handles.axes9,'XTickLabel',[])
set(handles.axes9,'YScale','log')
title('Q_1_1')
grid on

axes(handles.axes10)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(2,2),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes10,'XTick',1:length(phases))
%set(handles.axes10,'XTickLabel',phases)
set(handles.axes10,'XTickLabel',[])
set(handles.axes10,'YScale','log')
title('Q_2_2')
grid on

axes(handles.axes11)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(3,3),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes11,'XTick',1:length(phases))
%set(handles.axes11,'XTickLabel',phases)
set(handles.axes11,'XTickLabel',[])
set(handles.axes11,'YScale','log')
title('Q_3_3')
grid on

axes(handles.axes12)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Q(5,5),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes12,'XTick',1:length(phases))
%set(handles.axes12,'XTickLabel',phases)
set(handles.axes12,'XTickLabel',[])
set(handles.axes12,'YScale','log')
title('Q_5_5, Q_6_6')
grid on

axes(handles.axes13)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.R(1,1),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes13,'XTick',1:length(phases))
%set(handles.axes13,'XTickLabel',phases)
set(handles.axes13,'XTickLabel',[])
set(handles.axes13,'YScale','log')
title('R_1_1, R_2_2')
grid on

axes(handles.axes14)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Phi(1,1),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes14,'XTick',1:length(phases))
%set(handles.axes14,'XTickLabel',phases)
set(handles.axes14,'XTickLabel',[])
set(handles.axes14,'YScale','log')
title('\Phi_1_1, \Phi_2_2')
grid on

axes(handles.axes15)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Phi(3,3),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
set(handles.axes15,'XTick',1:length(phases))
set(handles.axes15,'XTickLabel',bigPhases)
set(handles.axes15,'YScale','log')
title('\Phi_3_3, \Phi_4_4')
grid on

axes(handles.axes16)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.Phi(5,5),''o'');'])
end
hold off
xlim([0 length(phases)+1])
set(kk,'MarkerFaceColor','b')
grid on
set(handles.axes16,'XTick',1:length(phases))
set(handles.axes16,'XTickLabel',bigPhases)
set(handles.axes16,'YScale','log')
title('\Phi_5_5, \Phi_6_6')

axes(handles.axes17)
cla
hold on
for j = 1:length(phases)
    eval(['kk(j) = plot(j,SOL.',phases{j},'.Params.estGain,''o'');'])
    eval(['plot(j,SOL.',phases{j},'.Params.curlGain,''rx'');'])
end
hold off
set(kk,'MarkerFaceColor','b')
xlim([0 length(phases)+1])
set(handles.axes17,'XTick',1:length(phases))
set(handles.axes17,'XTickLabel',bigPhases)
title('Estimated Gain')
grid on

% Update handles structure
guidata(hObject, handles);

%---------------CREATION FUNCTIONS-----------------------%


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
