function varargout = displaySubjectFitResults(varargin)
% DISPLAYSUBJECTFITRESULTS MATLAB code for displaySubjectFitResults.fig
%      DISPLAYSUBJECTFITRESULTS, by itself, creates a new DISPLAYSUBJECTFITRESULTS or raises the existing
%      singleton*.
%
%      H = DISPLAYSUBJECTFITRESULTS returns the handle to a new DISPLAYSUBJECTFITRESULTS or the handle to
%      the existing singleton*.
%
%      DISPLAYSUBJECTFITRESULTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in DISPLAYSUBJECTFITRESULTS.M with the given input arguments.
%
%      DISPLAYSUBJECTFITRESULTS('Property','Value',...) creates a new DISPLAYSUBJECTFITRESULTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before displaySubjectFitResults_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to displaySubjectFitResults_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help displaySubjectFitResults

% Last Modified by GUIDE v2.5 03-May-2014 01:16:44

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @displaySubjectFitResults_OpeningFcn, ...
                   'gui_OutputFcn',  @displaySubjectFitResults_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before displaySubjectFitResults is made visible.
function displaySubjectFitResults_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to displaySubjectFitResults (see VARARGIN)

% Choose default command line output for displaySubjectFitResults
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes displaySubjectFitResults wait for user response (see UIRESUME)
% uiwait(handles.figure1);
files = dir('DataFiles/*.mat');
bbb = 1;
for a = 1:length(files)
    load(['DataFiles/',num2str(files(a).name)]);
    if exist('yData')
        handles.fileNames{bbb} = files(a).name;
        bbb = bbb + 1;
        clear yData
    elseif exist('oData')
        handles.fileNames{bbb} = files(a).name;
        bbb = bbb + 1;
        clear oData
    end
end
if ~isfield(handles,'fileNames')
    error('No Data Files Found')
end
set(handles.popupmenu1,'String',handles.fileNames)
set(handles.popupmenu1,'Value',1)
load(['DataFiles/',num2str(handles.fileNames{1})]);

if exist('yData')
    set(handles.ageGroup,'String','YOUNG')
    Data = yData;
elseif exist('oData')
    set(handles.ageGroup,'String','OLD')
    Data = oData;
end

axes(handles.axes1)
hold on
plot(Data.EN1.PxMean,Data.EN1.PyMean)
ciplotY(Data.EN1.PxMean-Data.EN1.PxSE,...
    Data.EN1.PxMean+Data.EN1.PxSE,...
    Data.EN1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EN1')

axes(handles.axes2)
hold on;
plot(Data.EF1.PxMean,Data.EF1.PyMean)
ciplotY(Data.EF1.PxMean-Data.EF1.PxSE,...
    Data.EF1.PxMean+Data.EF1.PxSE,...
    Data.EF1.PyMean,[0 0 1],0.5)
plot(SOL_EF1.x(:,1),SOL_EF1.x(:,2),'r')
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes2,'XTickLabel',[])
set(handles.axes2,'YTickLabel',[])
title('EF1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes3)
hold on
plot(Data.EF2.PxMean,Data.EF2.PyMean)
ciplotY(Data.EF2.PxMean-Data.EF2.PxSE,...
    Data.EF2.PxMean+Data.EF2.PxSE,...
    Data.EF2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes3,'XTickLabel',[])
set(handles.axes3,'YTickLabel',[])
title('EF2')

axes(handles.axes4)
hold on;
plot(Data.EN2.PxMean,Data.EN2.PyMean)
ciplotY(Data.EN2.PxMean-Data.EN2.PxSE,...
    Data.EN2.PxMean+Data.EN2.PxSE,...
    Data.EN2.PyMean,[0 0 1],0.5)
plot(SOL_EN2.x(:,1),SOL_EN2.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes4,'XTickLabel',[])
set(handles.axes4,'YTickLabel',[])
title('EN2')
set(gca,'Color',[1 1 .53]);

axes(handles.axes5)
hold on;
plot(Data.LN1.PxMean,Data.LN1.PyMean)
ciplotY(Data.LN1.PxMean-Data.LN1.PxSE,...
    Data.LN1.PxMean+Data.LN1.PxSE,...
    Data.LN1.PyMean,[0 0 1],0.5)
plot(SOL_LN1.x(:,1),SOL_LN1.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes5,'XTick',[-0.1 -0.05 0 0.05 0.1])
set(handles.axes5,'YTick',[-.1 -.05 0 .05 .1])
set(handles.axes5,'YTickLabel',[-.1 -.05 0 .05 .1])
title('LN1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes6)
hold on
plot(Data.LF1.PxMean,Data.LF1.PyMean)
ciplotY(Data.LF1.PxMean-Data.LF1.PxSE,...
    Data.LF1.PxMean+Data.LF1.PxSE,...
    Data.LF1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes6,'XTickLabel',[])
set(handles.axes6,'YTickLabel',[])
title('LF1')

axes(handles.axes7)
hold on;
p(1) = plot(Data.LF2.PxMean,Data.LF2.PyMean);
ciplotY(Data.LF2.PxMean-Data.LF2.PxSE,...
    Data.LF2.PxMean+Data.LF2.PxSE,...
    Data.LF2.PyMean,[0 0 1],0.5)
p(2) = plot(SOL_LF2.x(:,1),SOL_LF2.x(:,2),'r');
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes7,'XTickLabel',[])
set(handles.axes7,'YTickLabel',[])
title('LF2')
set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Experimental','Model Results',...
    'Orientation','Horizontal',...
    'Location','South');
set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

axes(handles.axes8)
hold on
plot(Data.LN2.PxMean,Data.LN2.PyMean)
ciplotY(Data.LN2.PxMean-Data.LN2.PxSE,...
    Data.LN2.PxMean+Data.LN2.PxSE,...
    Data.LN2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes8,'XTickLabel',[])
set(handles.axes8,'YTickLabel',[])
title('LN2')

% Set Parameter Values
set(handles.q1177,'String',num2str(bestFitParams.Q(1,1)));
set(handles.q2288,'String',num2str(bestFitParams.Q(2,2)));
set(handles.q33,'String',num2str(bestFitParams.Q(3,3)));
set(handles.q5566,'String',num2str(bestFitParams.Q(5,5)));
set(handles.r1122,'String',num2str(bestFitParams.R(1,1)));
set(handles.phi1122,'String',num2str(bestFitParams.Phi(1,1)));
set(handles.phi3344,'String',num2str(bestFitParams.Phi(3,3)));
set(handles.phi5566,'String',num2str(bestFitParams.Phi(5,5)));
if length(FitIndices) > 9
if FitIndices(9)
    set(handles.gainLN1,'String',num2str(bestFitParams.estGainLN1));
    set(handles.gainLN1,'BackgroundColor',[1 1 .53])
else
    set(handles.gainLN1,'String',num2str(Data.LN1.estGainMean));
    set(handles.gainLN1,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(10)
    set(handles.gainEF1,'String',num2str(bestFitParams.estGainEF1));
    set(handles.gainEF1,'BackgroundColor',[1 1 .53])
else
    set(handles.gainEF1,'String',num2str(Data.EF1.estGainMean))
    set(handles.gainEF1,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(11)
    set(handles.gainLF2,'String',num2str(bestFitParams.estGainLF2));
    set(handles.gainLF2,'BackgroundColor',[1 1 .53])
else
    set(handles.gainLF2,'String',num2str(Data.LF2.estGainMean));
    set(handles.gainLF2,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(12)
    set(handles.gainEN2,'String',num2str(bestFitParams.estGainEN2));
    set(handles.gainEN2,'BackgroundColor',[1 1 .53])
else
    set(handles.gainEN2,'String',num2str(Data.EN2.estGainMean));
    set(handles.gainEN2,'BackgroundColor',[0.5 0.5 0.5]);
end

if FitIndices(1)
    set(handles.r1122,'BackgroundColor',[1 1 .53])
else
    set(handles.r1122,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(2)
    set(handles.q5566,'BackgroundColor',[1 1 .53])
else
    set(handles.q5566,'BackgroundColor',[0.5 0.5 0.5])    
end
if FitIndices(3)
    set(handles.q1177,'BackgroundColor',[1 1 .53])
else
    set(handles.q1177,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(4)
    set(handles.q2288,'BackgroundColor',[1 1 .53])
else
    set(handles.q2288,'BackgroundColor',[0.5 0.5 0.5])    
end
if FitIndices(5)
    set(handles.q33,'BackgroundColor',[1 1 .53])
else
    set(handles.q33,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(6)
    set(handles.phi1122,'BackgroundColor',[1 1 .53])
else
    set(handles.phi1122,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(7)
    set(handles.phi3344,'BackgroundColor',[1 1 .53])
else
    set(handles.phi3344,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(8)
    set(handles.phi5566,'BackgroundColor',[1 1 .53])
else
    set(handles.phi5566,'BackgroundColor',[0.5 0.5 0.5])
end


% Update handles structure
guidata(hObject, handles);

%-------------END OF INITIALIZATION------------------------%
%----------------------------------------------------------%
%----------------------------------------------------------%
%----------------------------------------------------------%



% --- Outputs from this function are returned to the command line.
function varargout = displaySubjectFitResults_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

ind = get(handles.popupmenu1,'Value');
file = handles.fileNames{ind};
load(['DataFiles/',num2str(file)])

if exist('yData')
    set(handles.ageGroup,'String','YOUNG')
    Data = yData;
elseif exist('oData')
    set(handles.ageGroup,'String','OLD')
    Data = oData;
end

axes(handles.axes1)
plot(Data.EN1.PxMean,Data.EN1.PyMean)
hold on
ciplotY(Data.EN1.PxMean-Data.EN1.PxSE,...
    Data.EN1.PxMean+Data.EN1.PxSE,...
    Data.EN1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes1,'XTickLabel',[])
set(handles.axes1,'YTickLabel',[])
title('EN1')

axes(handles.axes2)
plot(Data.EF1.PxMean,Data.EF1.PyMean)
hold on;
ciplotY(Data.EF1.PxMean-Data.EF1.PxSE,...
    Data.EF1.PxMean+Data.EF1.PxSE,...
    Data.EF1.PyMean,[0 0 1],0.5)
plot(SOL_EF1.x(:,1),SOL_EF1.x(:,2),'r')
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes2,'XTickLabel',[])
set(handles.axes2,'YTickLabel',[])
title('EF1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes3)
plot(Data.EF2.PxMean,Data.EF2.PyMean)
hold on
ciplotY(Data.EF2.PxMean-Data.EF2.PxSE,...
    Data.EF2.PxMean+Data.EF2.PxSE,...
    Data.EF2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes3,'XTickLabel',[])
set(handles.axes3,'YTickLabel',[])
title('EF2')

axes(handles.axes4)
plot(Data.EN2.PxMean,Data.EN2.PyMean)
hold on;
ciplotY(Data.EN2.PxMean-Data.EN2.PxSE,...
    Data.EN2.PxMean+Data.EN2.PxSE,...
    Data.EN2.PyMean,[0 0 1],0.5)
plot(SOL_EN2.x(:,1),SOL_EN2.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes4,'XTickLabel',[])
set(handles.axes4,'YTickLabel',[])
title('EN2')
set(gca,'Color',[1 1 .53]);

axes(handles.axes5)
plot(Data.LN1.PxMean,Data.LN1.PyMean)
hold on;
ciplotY(Data.LN1.PxMean-Data.LN1.PxSE,...
    Data.LN1.PxMean+Data.LN1.PxSE,...
    Data.LN1.PyMean,[0 0 1],0.5)
plot(SOL_LN1.x(:,1),SOL_LN1.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes5,'XTick',[-0.1 -0.05 0 0.05 0.1])
set(handles.axes5,'YTick',[-.1 -.05 0 .05 .1])
set(handles.axes5,'YTickLabel',[-.1 -.05 0 .05 .1])
title('LN1')
set(gca,'Color',[1 1 .53]);

axes(handles.axes6)
plot(Data.LF1.PxMean,Data.LF1.PyMean)
hold on
ciplotY(Data.LF1.PxMean-Data.LF1.PxSE,...
    Data.LF1.PxMean+Data.LF1.PxSE,...
    Data.LF1.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes6,'XTickLabel',[])
set(handles.axes6,'YTickLabel',[])
title('LF1')

axes(handles.axes7)
p(1) = plot(Data.LF2.PxMean,Data.LF2.PyMean);
hold on;
ciplotY(Data.LF2.PxMean-Data.LF2.PxSE,...
    Data.LF2.PxMean+Data.LF2.PxSE,...
    Data.LF2.PyMean,[0 0 1],0.5)
p(2) = plot(SOL_LF2.x(:,1),SOL_LF2.x(:,2),'r');
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes7,'XTickLabel',[])
set(handles.axes7,'YTickLabel',[])
title('LF2')
set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Experimental','Model Results',...
    'Orientation','Horizontal',...
    'Location','South');
set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

axes(handles.axes8)
plot(Data.LN2.PxMean,Data.LN2.PyMean)
hold on
ciplotY(Data.LN2.PxMean-Data.LN2.PxSE,...
    Data.LN2.PxMean+Data.LN2.PxSE,...
    Data.LN2.PyMean,[0 0 1],0.5)
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
set(handles.axes8,'XTickLabel',[])
set(handles.axes8,'YTickLabel',[])
title('LN2')

% Set Parameter Values
set(handles.q1177,'String',num2str(bestFitParams.Q(1,1)));
set(handles.q2288,'String',num2str(bestFitParams.Q(2,2)));
set(handles.q33,'String',num2str(bestFitParams.Q(3,3)));
set(handles.q5566,'String',num2str(bestFitParams.Q(5,5)));
set(handles.r1122,'String',num2str(bestFitParams.R(1,1)));
set(handles.phi1122,'String',num2str(bestFitParams.Phi(1,1)));
set(handles.phi3344,'String',num2str(bestFitParams.Phi(3,3)));
set(handles.phi5566,'String',num2str(bestFitParams.Phi(5,5)));
if length(FitIndices)>9
    if FitIndices(9)
        set(handles.gainLN1,'String',num2str(bestFitParams.estGainLN1));
        set(handles.gainLN1,'BackgroundColor',[1 1 .53])
    else
        set(handles.gainLN1,'String',num2str(Data.LN1.estGainMean));
        set(handles.gainLN1,'BackgroundColor',[0.5 0.5 0.5])
    end
    if FitIndices(10)
        set(handles.gainEF1,'String',num2str(bestFitParams.estGainEF1));
        set(handles.gainEF1,'BackgroundColor',[1 1 .53])
    else
        set(handles.gainEF1,'String',num2str(Data.EF1.estGainMean))
        set(handles.gainEF1,'BackgroundColor',[0.5 0.5 0.5])
    end
    if FitIndices(11)
        set(handles.gainLF2,'String',num2str(bestFitParams.estGainLF2));
        set(handles.gainLF2,'BackgroundColor',[1 1 .53])
    else
        set(handles.gainLF2,'String',num2str(Data.LF2.estGainMean));
        set(handles.gainLF2,'BackgroundColor',[0.5 0.5 0.5])
    end
    if FitIndices(12)
        set(handles.gainEN2,'String',num2str(bestFitParams.estGainEN2));
        set(handles.gainEN2,'BackgroundColor',[1 1 .53])
    else
        set(handles.gainEN2,'String',num2str(Data.EN2.estGainMean));
        set(handles.gainEN2,'BackgroundColor',[0.5 0.5 0.5]);
    end
else
    set(handles.gainLN1,'String',num2str(bestFitParams.estGainLN1));
    set(handles.gainLN1,'BackgroundColor',[0.5 0.5 0.5])
    set(handles.gainEF1,'String',num2str(bestFitParams.estGainEF1));
    set(handles.gainEF1,'BackgroundColor',[0.5 0.5 0.5])
    if FitIndices(9)
        set(handles.gainLF2,'String',num2str(bestFitParams.estGainLF2));
        set(handles.gainLF2,'BackgroundColor',[1 1 .53])
        set(handles.gainEN2,'String',num2str(bestFitParams.estGainEN2));
        set(handles.gainEN2,'BackgroundColor',[1 1 .53])
    else
        set(handles.gainLF2,'String',num2str(Data.LF2.estGainMean));
        set(handles.gainLF2,'BackgroundColor',[0.5 0.5 0.5])   
        set(handles.gainEN2,'String',num2str(Data.EN2.estGainMean));
        set(handles.gainEN2,'BackgroundColor',[0.5 0.5 0.5]);
    end
end


if FitIndices(1)
    set(handles.r1122,'BackgroundColor',[1 1 .53])
else
    set(handles.r1122,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(2)
    set(handles.q5566,'BackgroundColor',[1 1 .53])
else
    set(handles.q5566,'BackgroundColor',[0.5 0.5 0.5])    
end
if FitIndices(3)
    set(handles.q1177,'BackgroundColor',[1 1 .53])
else
    set(handles.q1177,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(4)
    set(handles.q2288,'BackgroundColor',[1 1 .53])
else
    set(handles.q2288,'BackgroundColor',[0.5 0.5 0.5])    
end
if FitIndices(5)
    set(handles.q33,'BackgroundColor',[1 1 .53])
else
    set(handles.q33,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(6)
    set(handles.phi1122,'BackgroundColor',[1 1 .53])
else
    set(handles.phi1122,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(7)
    set(handles.phi3344,'BackgroundColor',[1 1 .53])
else
    set(handles.phi3344,'BackgroundColor',[0.5 0.5 0.5])
end
if FitIndices(8)
    set(handles.phi5566,'BackgroundColor',[1 1 .53])
else
    set(handles.phi5566,'BackgroundColor',[0.5 0.5 0.5])
end

% Update handles structure
guidata(hObject, handles);



%---------------CREATION FUNCTIONS-----------------------%



% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
