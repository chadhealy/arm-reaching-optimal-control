% Plot Results

% Import Data
load('YoungData.mat')

phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};

% Reformat for Plotting
for i = 1:length(phaseNames)
    eval(['maxPerpMean(i) = yData.',phaseNames{i},'.maxPerpMean;'])
    eval(['estGainMean(i) = yData.',phaseNames{i},'.estGainMean;'])
end

% Plot Results
figure
plot(maxPerpMean,'o','LineWidth',2)
set(gca,'XTickLabel',phaseNames)
title('Maximum Perpendicular Deviation - Young Mean')

figure
plot(estGainMean,'o','LineWidth',2)
set(gca,'XTickLabel',phaseNames)
title('Estimated Curl Gain - Young Mean')

% Plot Average Trajectories
figure
subplot(2,4,1)
plot(yData.EN1.PxMean,yData.EN1.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('EN1')

subplot(2,4,2)
plot(yData.EF1.PxMean,yData.EF1.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('EF1')
set(gca,'Color',[1 1 0]);

subplot(2,4,3)
plot(yData.EF2.PxMean,yData.EF2.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('EF2')

subplot(2,4,4)
plot(yData.EN2.PxMean,yData.EN2.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('EN2')
set(gca,'Color',[1 1 0]);

subplot(2,4,5)
plot(yData.LN1.PxMean,yData.LN1.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('LN1')
set(gca,'Color',[1 1 0]);

subplot(2,4,6)
plot(yData.LF1.PxMean,yData.LF1.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('LF1')

subplot(2,4,7)
plot(yData.LF2.PxMean,yData.LF2.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('LF2')
set(gca,'Color',[1 1 0]);

subplot(2,4,8)
plot(yData.LN2.PxMean,yData.LN2.PyMean)
axis equal
axis([-0.1 0.1 -0.12 0.12])
title('LN2')

suptitle('Young Subject Data - Average Trajectories by Phase')