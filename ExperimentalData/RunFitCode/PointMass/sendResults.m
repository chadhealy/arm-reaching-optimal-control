function sendResults(file)
% Written by Chadwick Healy
%
%
% Sends a text and/or email notification that your code is done running
% 
% By default, the code sends a text message.  If you add results (variable
% name, "file") then it will send an email with the results as an
% attachment.
%
% If you want to run this function without it being in the directory you
% are currently in, copy and paste this line of code:
%
%       addpath(pwd);
%
% It will add the directory to the search path, so be sure to have this saved
% in the place you really want it!
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CUSTOMIZE THESE VARIABLES
username = 'chadhealy09';
password = 'THirteen13!!';
domain = 'gmail.com';
phoneNum = '5413013576';
carrier = 'att';
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% If you want these values (like your password) to be protected, you can 
% create a p-file and refer to it instead...


% Setup Email to Send From
if strcmp(domain,'gmail.com') || strcmp(domain,'colorado.edu')
    setpref('Internet','SMTP_Server', 'smtp.gmail.com');
elseif strcmp(domain,'yahoo.com')
    setpref('Internet','SMTP_Server','smtp.mail.yahoo.com');
else
    error('Unrecognized domain.  Please choose from gmail, yahoo or colorado.edu')
end
setpref('Internet','E_mail',[username,'@',domain]);
setpref('Internet','SMTP_Username',[username,'@',domain]);
setpref('Internet','SMTP_Password',password);

props = java.lang.System.getProperties;
props.setProperty('mail.smtp.auth','true');
props.setProperty('mail.smtp.socketFactory.class', ...
                  'javax.net.ssl.SSLSocketFactory');
props.setProperty('mail.smtp.socketFactory.port','465');    


% Send Text Message
if strcmp(carrier,'att')
    sendmail([phoneNum,'@txt.att.net'],'MATLAB',...
                'Your code is done running!')
elseif strcmp(carrier,'verizon')
    sendmail([phoneNum,'@vtext.com'],'MATLAB',...
                'Your code is done running!')
elseif strcmp(carrier,'tmobile')
    sendmail([phoneNum,'@tmomail.net'],'MATLAB',...
                'Your code is done running!')
else
    error('Unrecognized carrier. Please choose from ''att'', ''verizon'', or ''tmobile''.')
end

% Send Email with Results Attached
if nargin
	sendmail([username,'@',domain],'Results',...
    'See attached.',{file});
end