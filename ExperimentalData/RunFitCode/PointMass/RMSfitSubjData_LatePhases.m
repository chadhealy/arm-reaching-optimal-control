% RMS Fit Code for Young Subject

% Looks at Late Phases Only
% - LN1
% - LF1
% - LF2
% - LN2
function RMS = RMSfitSubjData_LatePhases(X,subjData,FitIndices,Params)





count = 1;
if FitIndices(1)
    Params.R(1,1) = X(count);
    Params.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.Q(5,5) = X(count);
    Params.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.Q(1,1) = X(count);
    Params.Q(7,7) = X(count);
    Params.Q(1,7) = -1*X(count);
    Params.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.Q(2,2) = X(count);
    Params.Q(8,8) = X(count);
    Params.Q(2,8) = -1*X(count);
    Params.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.Phi(1,1) = X(count);
    Params.Phi(7,7) = X(count);
    Params.Phi(1,7) = -1*X(count);
    Params.Phi(7,1) = -1*X(count);
    Params.Phi(2,2) = X(count);
    Params.Phi(8,8) = X(count);
    Params.Phi(2,8) = -1*X(count);
    Params.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.Phi(3,3) = X(count);
    Params.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.Phi(5,5) = X(count);
    Params.Phi(6,6) = X(count);
    count = count + 1;
end

% First Trajectory: LN1
if FitIndices(9)
    Params.estGain = X(count);
    count = count + 1;
else
    Params.estGain = subjData.LN1.estGainMean;
end
Params.curlGain = 0;
nLN1 = length(subjData.LN1.PxMean);
Params.trialLength = nLN1;
% TEST: Have initial position match experimental data
Params.x0 = [subjData.LN1.PxMean(1);
            subjData.LN1.PyMean(1);
            subjData.LN1.VxMean(1);
            subjData.LN1.VyMean(1);
            0; 0; 0; 0.1];
SOL_LN1 = OptimalReachSubjData(Params);

% Second Trajectory: LF1
if FitIndices(10)
    Params.estGain = X(count);
    count = count + 1;
else
    Params.estGain = subjData.LF1.estGainMean;
end
Params.curlGain = -20;
nLF1 = length(subjData.LF1.PxMean);
Params.trialLength = nLF1;
Params.x0 = [subjData.LF1.PxMean(1);
            subjData.LF1.PyMean(1);
            subjData.LF1.VxMean(1);
            subjData.LF1.VyMean(1);
            0; 0; 0; 0.1];
SOL_LF1 = OptimalReachSubjData(Params);

% Third Trajectory: LF2
if FitIndices(11)
    Params.estGain = X(count);
    count = count + 1;
else
    Params.estGain = subjData.LF2.estGainMean;
end
Params.curlGain = -20;
nLF2 = length(subjData.LF2.PxMean);
Params.trialLength = nLF2;
Params.x0 = [subjData.LF2.PxMean(1);
            subjData.LF2.PyMean(1);
            subjData.LF2.VxMean(1);
            subjData.LF2.VyMean(1);
            0; 0; 0; 0.1];
SOL_LF2 = OptimalReachSubjData(Params);

% Fourth Trajectory: LN2
if FitIndices(12)
    Params.estGain = X(count);
else
    Params.estGain = subjData.LN2.estGainMean;
end
Params.curlGain = 0;
nLN2 = length(subjData.LN2.PxMean);
Params.trialLength = nLN2;
Params.x0 = [subjData.LN2.PxMean(1);
            subjData.LN2.PyMean(1);
            subjData.LN2.VxMean(1);
            subjData.LN2.VyMean(1);
            0; 0; 0; 0.1];
SOL_LN2 = OptimalReachSubjData(Params);




% Calculate Error
% Normalized by trial length, and number of arrays compared 
RMS_LN1 = sum((subjData.LN1.PxMean' - SOL_LN1.x(:,1)).^2) + ...
    sum((subjData.LN1.PyMean' - SOL_LN1.x(:,2)).^2);
RMS_LN1 = RMS_LN1/2/nLN1;

RMS_LF1 = sum((subjData.LF1.PxMean' - SOL_LF1.x(:,1)).^2) + ...
    sum((subjData.LF1.PyMean' - SOL_LF1.x(:,2)).^2);
RMS_LF1 = RMS_LF1/2/nLF1;

RMS_LF2 = sum((subjData.LF2.PxMean' - SOL_LF2.x(:,1)).^2) + ...
    sum((subjData.LF2.PyMean' - SOL_LF2.x(:,2)).^2);
RMS_LF2 = RMS_LF2/2/nLF2;

RMS_LN2 = sum((subjData.LN2.PxMean' - SOL_LN2.x(:,1)).^2) + ...
    sum((subjData.LN2.PyMean' - SOL_LN2.x(:,2)).^2);
RMS_LN2 = RMS_LN2/2/nLN2;

RMS = sqrt((RMS_LN1 + RMS_LF1 + RMS_LF2 + RMS_LN2)/4);
