%% Compare Solutions for different inertial Arm Models


fileNames = {'EF2-PtMass-20140924.mat';...   % For reference
             'EF2-InertArm-20140924.mat';...
             'EF2-NoOffDiagInertArm-20140924.mat';...
             'EF2-OtherInertArm-20140924.mat';...
             'EF2-InertArm-NoQ22.mat'};

colorArray = hsv(length(fileNames));
load(fileNames{1})
% load(fileNames(1).name)
time = 0:1/200:(length(yData.EN2.PxMeanNonNorm)-1)/200;
legendNames{1} = 'Data';

figure
subplot(1,2,1)
hold on
h(1) = plot(yData.EN2.PxMeanNonNorm,yData.EN2.PyMeanNonNorm,'k');
ciplotY(yData.EN2.PxMeanNonNorm - yData.EN2.PxSENonNorm,...
    yData.EN2.PxMeanNonNorm + yData.EN2.PxSENonNorm,...
    yData.EN2.PyMeanNonNorm,'k', 0.5)
axis equal
axis([-0.08 0.08 -.15 .15])

subplot(6,2,2)
hold on
plot(time,yData.EN2.PxMeanNonNorm,'k')

subplot(6,2,4)
hold on
plot(time,yData.EN2.PyMeanNonNorm,'k')

subplot(6,2,6)
hold on
plot(time,yData.EN2.VxMeanNonNorm,'k')

subplot(6,2,8)
hold on
plot(time,yData.EN2.VyMeanNonNorm,'k')

subplot(6,2,10)
hold on
plot(time,yData.EN2.AxMeanNonNorm,'k')

subplot(6,2,12)
hold on
plot(time,yData.EN2.AyMeanNonNorm,'k')


for ii = 1:length(fileNames)
    load(fileNames{ii})
    legendNames{ii+1} = [fileNames{ii},' estGain = ',num2str(Young.EN2.Params.estGain)];
%     load(fileNames(ii).name)
    subplot(1,2,1)
    h(ii + 1) = plot(Young.EN2.Traj.x(:,1),Young.EN2.Traj.x(:,2),'Color',colorArray(ii,:));
    
    subplot(6,2,2)
    plot(time,Young.EN2.Traj.x(:,1),'Color',colorArray(ii,:));
    
    subplot(6,2,4)
    plot(time,Young.EN2.Traj.x(:,2),'Color',colorArray(ii,:));
    
    subplot(6,2,6)
    plot(time,Young.EN2.Traj.x(:,3),'Color',colorArray(ii,:));

    subplot(6,2,8)
    plot(time,Young.EN2.Traj.x(:,4),'Color',colorArray(ii,:));
    
    subplot(6,2,10)
    plot(time,Young.EN2.Traj.x(:,5),'Color',colorArray(ii,:));
  
    subplot(6,2,12)
    plot(time,Young.EN2.Traj.x(:,6),'Color',colorArray(ii,:));
    
end

% Post-Plotting Formatting
subplot(1,2,1)
title('XY-Trajectory')
legend(h,legendNames)
subplot(6,2,2)
title('X-Position')
xlim([time(1) time(end)])
subplot(6,2,4)
title('Y-Position')
xlim([time(1) time(end)])
subplot(6,2,6)
title('X-Velocity')
xlim([time(1) time(end)])
subplot(6,2,8)
title('Y-Velocity')
xlim([time(1) time(end)])
subplot(6,2,10)
title('X-Accel')
xlim([time(1) time(end)])
subplot(6,2,12)
title('Y-Accel')
xlim([time(1) time(end)])

suptitle('Comparison of Inertial Models: EN2 Trajectory')