%% Plot to See consistency of results

numSols = 1:length(Results(:,1));

titleStrings = {'R_1_1'; ...
                'Q_5_5'; ...
                'Q_1_1'; ...
                'Q_2_2'; ...
                '\Phi_1_1'; ...
                '\Phi_3_3'; ...
                '\Phi_5_5'; ...
                'Est Gain'};

figure
for ii = 1:length(titleStrings)
    subplot(3,3,ii)
    
    semilogy(numSols,Results(:,ii),'bo')

    %semilogy(numSols,Results(in,ii),'ro')

    title(titleStrings{ii})
end

subplot(3,3,length(titleStrings)+1)
plot(numSols,Values,'bo')
title('RMS Values')

% subplot(4,3,length(titleStrings)+2)
% plot(numSols,Flags,'b.')
% title('Error Flags')

suptitle('Consistency of Results - Inertial Arm, EN2')

