% Fit Code for Subject Data - Single Trajectory
clear all;clc

    


%load('YoungData-ToFit')
%load('OldData-ToFit-2TrialMean.mat')
%load('OldData-ToFit')
load('Young-ToFit-MedianTraj.mat')
%yData = oData;% Hack for now...

%%%%%%%
% Choose Trajectory (Phase) to Fit
% EN1, LN1, EF1, LF1, EF2, LF2, EN2, or LN2
%Params.phase = 'EN1';
%
%%%%%%%%%%

phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};% Defined By Helen


for j = 1:length(phaseNames)
    
Params.phase = phaseNames{j};
    
% Dynamics
x0 = [0;-0.1;0;0;0;0;0;0.1];
% curlGain = -20;%Ns/m (curl gain)
eval(['curlGain = yData.',Params.phase,'.curlGain;']);%Ns/m (curl gain)
% From experimental Data
dt = 1/200;%sec

% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);

% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22];

Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);


t0 = 0;
%trialLength = length(t0:dt:tf);
Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
% Fsys(3:4,3:4) = curlGain/mass*[0 1;-1 0];
Fsys(3:4,3:4) = invMass*[0 1;-1 0];


% Controller Matrix
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;

% Make discrete Time
AdisNull = eye(8) + Asys*dt;
AdisCurl = eye(8) + Asys*dt + Fsys*dt;
Bdis = dt*Bsys;


% Make Parameter Structure
Params.x0 = x0;
Params.dt = dt;
Params.Asys = Asys;
Params.B = Bdis;
Params.Fsys = Fsys;

%... will vary
Params.curlGain = curlGain;
Params.trialLength = 0.6*200;
Params.estGain = 0;
%... will also vary
Params.Q = zeros(size(Asys));
Params.R = eye(size(Bdis,2));
Params.Phi = eye(size(Asys));

%initialize BestFitParams Array
bestFitParams = Params;

% Assign Fit Indices
FitIndices = [1, ... % R(1,1) = R(2,2) (control effort)
              1, ... % Q(5,5) = Q(6,6) (force penalty)
              1, ... % Q(1,1) = Q(7,7) = -Q(1,7) = -Q(7,1) (horizontal tracking)             
              1, ... % Q(2,2) = Q(8,8) = -Q(2,8) = -Q(8,2) (vertical dist. penalty)             
              0, ... % Q(3,3) % Horizontal Velocity
              1, ... % Phi(1,1) = Phi(2,2) = Phi(7,7) = Phi(8,8) = -Phi(1,7)... (terminal position)
              1, ... % Phi(3,3) = Phi(4,4) (terminal velocity)
              1, ... % Phi(5,5) = Phi(6,6) (terminal force)
              1];    % estGain (proportional estimate of curl field)

% Force Est Gain to Zero for EN1, LN1, EF1          
if j < 4
    FitIndices(end) = 0;
end
          
FitIndices = logical(FitIndices);
numX = sum(FitIndices);


%%% Number of solutions
%
numsols = 10;
%
%%%


% Initial FMINCON Parameters
%       [R(1,1), Q(5,5), Q(1,1), Q(2,2), Q(3,3), Phi(1,1), Phi(3,3), Phi(5,5), estGain
BaseX = [1e4;   0;      0;      0;   0;      1e2;      1e2;      1e2;      0;]; 

AA = [];
bb = [];
Aeq = [];
beq = [];
lb = zeros(length(FitIndices),1);
lb(1) = 1;
lb(9:12) = -50;
lb = lb(FitIndices);
ub = 1e20*ones(length(FitIndices),1);
ub(9:12) = 50;
ub = ub(FitIndices);
nonlcon = [];
options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-14,...
        'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');

% Initialize Variables
Values = zeros(numsols,1);
Results = zeros(numsols,numX);
Flags = zeros(numsols,1);

for m = 1:numsols 
    disp(['minimization loop: ',num2str(m)])

    Xo = BaseX;
    Xo(1:8) = Xo(1:8) + 1000*rand(8,1).*Xo(1:8);
    Xo(9) = Xo(9) + 10*rand(1,1).*Xo(9);
    Xo = Xo(FitIndices);

    [X, fval, flag] = fmincon(@(X) RMSfitSubjData_SingleTraj_NoReg(X,yData,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);

    Results(m,:) = X';
    Values(m) = fval;
    Flags(m) = flag;
end

% Refined Search
%                                     options = optimset('MaxFunEval',1E3,'MaxIter',1E3,'TolFun',1E-14,...
%                                         'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');
options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-18,...
    'TolX',1E-16,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');

for m = numsols+1:2*numsols
    disp(['refining minimization loop: ',num2str(m)])

    Xo = Results(m-numsols,:)';
    [X, fval, flag] = fmincon(@(X) RMSfitSubjData_SingleTraj_NoReg(X,yData,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
    %flag

    Results(m,:) = X';
    Values(m) = fval;
    Flags(m) = flag;
end


Results(2*numsols+1,:) = lb';
Values(2*numsols+1) = RMSfitSubjData_SingleTraj(lb,yData,FitIndices,Params);

[val, in] = min(Values);
BestX = Results(in,:);


% Commented out for initial run, so it doesn't get caught in an infinite
% loop
%
% nLoops = 2;
% while Flags(in) ~= 2
%     % Change step size to 1e-18
%     options = optimset('MaxFunEval',1E4,'MaxIter',1E4,'TolFun',1E-16,...
%         'TolX',1E-18,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');
% 
%     % Re-run for better solution
% 
%     % Set initial point at best
%     % solution that met fmincon
%     % conditions
%     [~,newInd] = min(Values(find(Flags==2)));
% 
%     for m = nLoops*numsols+1:(nLoops+1)*numsols
%         disp(['refining minimization loop ',num2str(nLoops),': ',num2str(m)])
% 
%         Xo = Results(newInd,:)';
%         [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
% 
%         Results(m,:) = X';
%         Values(m) = fval;
%         Flags(m) = flag;
%     end
% Results((nLoops+1)*numsols+1,:) = lb';
% Values((nLoops+1)*numsols+1) = RMSfitVector_noGlobal_XYOnly(lb,toFitTraj,FitIndices,Params);
% 
% [val, in] = min(Values(nLoops*numsols+1:end));
% BestX = Results(in,:);
% 
% nLoops = nLoops + 1;
% end

count = 1;
if FitIndices(1)
    bestFitParams.R(1,1) = BestX(count);
    bestFitParams.R(2,2) = BestX(count);
    count = count + 1;
end
if FitIndices(2)
    bestFitParams.Q(5,5) = BestX(count);
    bestFitParams.Q(6,6) = BestX(count);
    count = count + 1;
end
if FitIndices(3)
    bestFitParams.Q(1,1) = BestX(count);
    bestFitParams.Q(7,7) = BestX(count);
    bestFitParams.Q(1,7) = -1*BestX(count);
    bestFitParams.Q(7,1) = -1*BestX(count);
    count = count + 1;
end
if FitIndices(4) 
    bestFitParams.Q(2,2) = BestX(count);
    bestFitParams.Q(8,8) = BestX(count);
    bestFitParams.Q(2,8) = -1*BestX(count);
    bestFitParams.Q(8,2) = -1*BestX(count);
    count = count + 1;
end
if FitIndices(5)
   bestFitParams.Q(3,3) = BestX(count); 
   count = count + 1;
end
if FitIndices(6)
    bestFitParams.Phi(1,1) = BestX(count);
    bestFitParams.Phi(7,7) = BestX(count);
    bestFitParams.Phi(1,7) = -1*BestX(count);
    bestFitParams.Phi(7,1) = -1*BestX(count);
    bestFitParams.Phi(2,2) = BestX(count);
    bestFitParams.Phi(8,8) = BestX(count);
    bestFitParams.Phi(2,8) = -1*BestX(count);
    bestFitParams.Phi(8,2) = -1*BestX(count);
    count = count + 1;
end
if FitIndices(7)
    bestFitParams.Phi(3,3) = BestX(count);
    bestFitParams.Phi(4,4) = BestX(count);
    count = count + 1;
end
if FitIndices(8)
    bestFitParams.Phi(5,5) = BestX(count);
    bestFitParams.Phi(6,6) = BestX(count);
    count = count + 1;
end
if FitIndices(9)
    bestFitParams.estGain = BestX(count);
end


eval(['bestFitParams.x0 = [yData.',Params.phase,...
    '.PxMean(1);yData.',Params.phase,...
    '.PyMean(1);yData.',Params.phase,...
    '.VxMean(1);yData.',Params.phase,...
    '.VyMean(1);0; 0; 0; 0.1];'])
SOL = OptimalReachSubjData(bestFitParams);

% Plot Results

% Plot Average Trajectories
figure
hold on
eval(['plot(yData.',Params.phase,'.PxMean,yData.',Params.phase,'.PyMean)'])
plot(SOL.x(:,1),SOL.x(:,2),'r')
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
eval(['title(''',Params.phase,''')'])
set(gca,'Color',[1 1 .53]);

eval(['Young.',Params.phase,'.Params = bestFitParams;'])
eval(['Young.',Params.phase,'.Traj = SOL;'])


end

Old = Young;
%save('Old-FitAllPhasesIndividually-ZeroEstGainEarly.mat','Old','oData')
eval(['save(''Young-AllPhasesIndivid-noReg-MedianData.mat'',''Young'',''yData'')'])
eval(['sendResults(''Young-AllPhasesIndivid-noReg-MedianData.mat'')'])
