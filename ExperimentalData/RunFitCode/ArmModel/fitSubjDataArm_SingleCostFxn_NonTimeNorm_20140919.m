% Fit Code for Subject Data - NON TIME NORMALIZED, SSE METHOD
% LOOKS AT LN1, EF1, LF2, EN2

load('YoungData-ToFit-WAccel-NonTimeNormalized-20140918.mat')
%load('OldData-ToFit')
%yData = oData;% Hack for now...

% Dynamics
x0 = [0;-0.1;0;0;0;0;0;0.1];
curlGain = -20;%Ns/m (curl gain)
% From experimental Data
dt = 1/200;%sec

% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);

% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22];

Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);


t0 = 0;
%trialLength = length(t0:dt:tf);
Asys = [0 0 1 0 0 0 0 0; 
        0 0 0 1 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0;
        0 0 0 0 0 0 0 0];
Asys(3:4,5:6) = invMass;
Fsys = zeros(8);
% Fsys(3:4,3:4) = curlGain/mass*[0 1;-1 0];
Fsys(3:4,3:4) = invMass*[0 1;-1 0];


% Controller Matrix
Bsys = zeros(8,2);
motorGain = 1e6;% to make R weights on same scale as Q and Phi
Bsys(5:6,1:2) = eye(2)*motorGain;

% Make discrete Time
AdisNull = eye(8) + Asys*dt;
AdisCurl = eye(8) + Asys*dt + Fsys*dt;
Bdis = dt*Bsys;


% Make Parameter Structure
Params.x0 = x0;
Params.dt = dt;
Params.Asys = Asys;
Params.B = Bdis;
Params.Fsys = Fsys;

%... will vary
Params.curlGain = curlGain;
Params.trialLength = 0.6*200;
Params.estGain = 0;
%... will also vary
Params.Q = zeros(size(Asys));
Params.R = eye(size(Bdis,2));
Params.Phi = eye(size(Asys));

Params.Mass = Mass;


%initialize BestFitParams Array
bestFitParams = Params;

% Assign Fit Indices
FitIndices = [1, ... % R(1,1) = R(2,2) (control effort)
              1, ... % Q(5,5) = Q(6,6) (force penalty)
              1, ... % Q(1,1) = Q(7,7) = -Q(1,7) = -Q(7,1) (horizontal tracking)             
              1, ... % Q(2,2) = Q(8,8) = -Q(2,8) = -Q(8,2) (vertical dist. penalty)             
              0, ... % Q(3,3) % Horizontal Velocity
              1, ... % Phi(1,1) = Phi(2,2) = Phi(7,7) = Phi(8,8) = -Phi(1,7)... (terminal position)
              1, ... % Phi(3,3) = Phi(4,4) (terminal velocity)
              1, ... % Phi(5,5) = Phi(6,6) (terminal force)
              1, ... % estGain - LN1 (proportional estimate of curl field)
              1, ... % estGain - EF1 (proportional estimate of curl field)
              1, ... % estGain - LF2 (proportional estimate of curl field)
              1]; % estGain - EN2 (proportional estimate of curl field)

          %FOR NOW, JUST THESE 4 PHASES,  SHOULD DO ALL EVENTUALLY...
          
FitIndices = logical(FitIndices);
numX = sum(FitIndices);


%%% Number of solutions
%
numsols = 10;
%
%%%


% Initial FMINCON Parameters
%       [R(1,1), Q(5,5), Q(1,1), Q(2,2), Q(3,3), Phi(1,1), Phi(3,3), Phi(5,5),
BaseX = [1e4;   0;      0;      0;   0;      1e2;      1e2;      1e2;      ...
        %estGain-LN1,estGain-EF1,estGain-LF2,estGain-EN2]
        0;           0;          0;          0;]; 

AA = [];
bb = [];
Aeq = [];
beq = [];
lb = zeros(length(FitIndices),1);
lb(1) = 1;
lb(9:12) = -25;
lb = lb(FitIndices);
ub = 1e20*ones(length(FitIndices),1);
ub(9:12) = 0;
ub = ub(FitIndices);
nonlcon = [];
options = optimset('MaxFunEval',1E5,'MaxIter',1E5,'TolFun',1E-14,...
        'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');

% Initialize Variables
Values = zeros(numsols,1);
Results = zeros(numsols,numX);
Flags = zeros(numsols,1);

for m = 1:numsols 
    disp(['minimization loop: ',num2str(m)])

    Xo = BaseX;
    Xo(1:8) = Xo(1:8) + 1000*rand(8,1).*Xo(1:8);
    Xo(9:12) = Xo(9:12) + 10*rand(4,1).*Xo(9:12);
    Xo = Xo(FitIndices);
%     % If fitting over alpha, do not change by same scale.
%     if FitIndices(9)
%         Xo(1:end-1) = Xo(1:end-1) + 1000*rand(numX,1).*Xo(1:end-1);
%         Xo(end) = Xo(end) + rand;
%     else
%         Xo = Xo + 1000*rand(numX,1).*Xo;
%     end

    [X, fval, flag] = fmincon(@(X) RMSfitSubjData_4Phase_ZScorePosVelAcc_NonTimeNorm(X,yData,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
    %flag

    Results(m,:) = X';
    Values(m) = fval;
    Flags(m) = flag;
end

% Refined Search
%                                     options = optimset('MaxFunEval',1E3,'MaxIter',1E3,'TolFun',1E-14,...
%                                         'TolX',1E-14,'TolCon',1E-14,'LargeScale','off','Algorithm','sqp');
options = optimset('MaxFunEval',1E4,'MaxIter',1E4,'TolFun',1E-16,...
    'TolX',1E-16,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');

for m = numsols+1:2*numsols
    disp(['refining minimization loop: ',num2str(m)])

    Xo = Results(m-numsols,:)';
    [X, fval, flag] = fmincon(@(X) RMSfitSubjData_4Phase_ZScorePosVelAcc_NonTimeNorm(X,yData,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
    %flag

    Results(m,:) = X';
    Values(m) = fval;
    Flags(m) = flag;
end


Results(2*numsols+1,:) = lb';
Values(2*numsols+1) = RMSfitSubjData_4Phase_ZScorePosVelAcc_NonTimeNorm(lb,yData,FitIndices,Params);

[val, in] = min(Values);
BestX = Results(in,:);


% Commented out for initial run, so it doesn't get caught in an infinite
% loop
%
% nLoops = 2;
% while Flags(in) ~= 2
%     % Change step size to 1e-18
%     options = optimset('MaxFunEval',1E4,'MaxIter',1E4,'TolFun',1E-16,...
%         'TolX',1E-18,'TolCon',1E-16,'LargeScale','off','Algorithm','sqp');
% 
%     % Re-run for better solution
% 
%     % Set initial point at best
%     % solution that met fmincon
%     % conditions
%     [~,newInd] = min(Values(find(Flags==2)));
% 
%     for m = nLoops*numsols+1:(nLoops+1)*numsols
%         disp(['refining minimization loop ',num2str(nLoops),': ',num2str(m)])
% 
%         Xo = Results(newInd,:)';
%         [X, fval, flag] = fmincon(@(X) RMSfitVector_noGlobal_XYOnly(X,toFitTraj,FitIndices,Params),Xo,AA,bb,Aeq,beq,lb,ub,nonlcon,options);
% 
%         Results(m,:) = X';
%         Values(m) = fval;
%         Flags(m) = flag;
%     end
% Results((nLoops+1)*numsols+1,:) = lb';
% Values((nLoops+1)*numsols+1) = RMSfitVector_noGlobal_XYOnly(lb,toFitTraj,FitIndices,Params);
% 
% [val, in] = min(Values(nLoops*numsols+1:end));
% BestX = Results(in,:);
% 
% nLoops = nLoops + 1;
% end

count = 1;
if FitIndices(1)
    bestFitParams.R(1,1) = BestX(count);
    bestFitParams.R(2,2) = BestX(count);
    count = count + 1;
end
if FitIndices(2)
    bestFitParams.Q(5,5) = BestX(count);
    bestFitParams.Q(6,6) = BestX(count);
    count = count + 1;
end
if FitIndices(3)
    bestFitParams.Q(1,1) = BestX(count);
    bestFitParams.Q(7,7) = BestX(count);
    bestFitParams.Q(1,7) = -1*BestX(count);
    bestFitParams.Q(7,1) = -1*BestX(count);
    count = count + 1;
end
if FitIndices(4) 
    bestFitParams.Q(2,2) = BestX(count);
    bestFitParams.Q(8,8) = BestX(count);
    bestFitParams.Q(2,8) = -1*BestX(count);
    bestFitParams.Q(8,2) = -1*BestX(count);
    count = count + 1;
end
if FitIndices(5)
   bestFitParams.Q(3,3) = BestX(count); 
   count = count + 1;
end
if FitIndices(6)
    bestFitParams.Phi(1,1) = BestX(count);
    bestFitParams.Phi(7,7) = BestX(count);
    bestFitParams.Phi(1,7) = -1*BestX(count);
    bestFitParams.Phi(7,1) = -1*BestX(count);
    bestFitParams.Phi(2,2) = BestX(count);
    bestFitParams.Phi(8,8) = BestX(count);
    bestFitParams.Phi(2,8) = -1*BestX(count);
    bestFitParams.Phi(8,2) = -1*BestX(count);
    count = count + 1;
end
if FitIndices(7)
    bestFitParams.Phi(3,3) = BestX(count);
    bestFitParams.Phi(4,4) = BestX(count);
    count = count + 1;
end
if FitIndices(8)
    bestFitParams.Phi(5,5) = BestX(count);
    bestFitParams.Phi(6,6) = BestX(count);
    count = count + 1;
end
if FitIndices(9)
    bestFitParams.estGainLN1 = BestX(count);
    count = count + 1;
end
if FitIndices(10)
    bestFitParams.estGainEF1 = BestX(count);
    count = count + 1;
end
if FitIndices(11)
    bestFitParams.estGainLF2 = BestX(count);
    count = count + 1;
end
if FitIndices(12)
    bestFitParams.estGainEN2 = BestX(count);
end

% Generate Best Fit Trajectories
LN1Params = bestFitParams;
if FitIndices(9)
    LN1Params.estGain = bestFitParams.estGainLN1;
else
    LN1Params.estGain = yData.LN1.estGainMean;
end
LN1Params.curlGain = 0;
LN1Params.trialLength = length(yData.LN1.PxMeanNonNorm);
% TEST: Make initial parameters match experimental data
forceInit_LN1 = Params.Mass*[(yData.LN1.VxMeanNonNorm(2) - yData.LN1.VxMeanNonNorm(1))/Params.dt;
                         (yData.LN1.VyMeanNonNorm(2) - yData.LN1.VyMeanNonNorm(1))/Params.dt];
LN1Params.x0 = [yData.LN1.PxMeanNonNorm(1);
            yData.LN1.PyMeanNonNorm(1);
            yData.LN1.VxMeanNonNorm(1);
            yData.LN1.VyMeanNonNorm(1);
            forceInit_LN1(1);forceInit_LN1(2); 0; 0.1];
SOL_LN1 = OptimalReachSubjData(LN1Params);

EF1Params = bestFitParams;
if FitIndices(10)
    EF1Params.estGain = bestFitParams.estGainEF1;
else
    EF1Params.estGain = yData.EF1.estGainMean;
end
EF1Params.curlGain = -20;
EF1Params.trialLength = length(yData.EF1.PxMeanNonNorm);
forceInit_EF1 = Params.Mass*[(yData.EF1.VxMeanNonNorm(2) - yData.EF1.VxMeanNonNorm(1))/Params.dt;
                         (yData.EF1.VyMeanNonNorm(2) - yData.EF1.VyMeanNonNorm(1))/Params.dt];
EF1Params.x0 = [yData.EF1.PxMeanNonNorm(1);
            yData.EF1.PyMeanNonNorm(1);
            yData.EF1.VxMeanNonNorm(1);
            yData.EF1.VyMeanNonNorm(1);
            forceInit_EF1(1);forceInit_EF1(2); 0; 0.1];
SOL_EF1 = OptimalReachSubjData(EF1Params);

LF2Params = bestFitParams;
if FitIndices(11)
    LF2Params.estGain = bestFitParams.estGainLF2;
else
    LF2Params.estGain = yData.LF2.estGainMean;
end
LF2Params.curlGain = -20;
LF2Params.trialLength = length(yData.LF2.PxMeanNonNorm);
forceInit_LF2 = Params.Mass*[(yData.LF2.VxMeanNonNorm(2) - yData.LF2.VxMeanNonNorm(1))/Params.dt;
                         (yData.LF2.VyMeanNonNorm(2) - yData.LF2.VyMeanNonNorm(1))/Params.dt];
LF2Params.x0 = [yData.LF2.PxMeanNonNorm(1);
            yData.LF2.PyMeanNonNorm(1);
            yData.LF2.VxMeanNonNorm(1);
            yData.LF2.VyMeanNonNorm(1);
            forceInit_LF2(1);forceInit_LF2(2); 0; 0.1];
SOL_LF2 = OptimalReachSubjData(LF2Params);

EN2Params = bestFitParams;
if FitIndices(12)
    EN2Params.estGain = bestFitParams.estGainEN2;
else
    EN2Params.estGain = yData.EN2.estGainMean;
end
EN2Params.curlGain = 0;
EN2Params.trialLength = length(yData.EN2.PxMeanNonNorm);
forceInit_EN2 = Params.Mass*[(yData.EN2.VxMeanNonNorm(2) - yData.EN2.VxMeanNonNorm(1))/Params.dt;
                         (yData.EN2.VyMeanNonNorm(2) - yData.EN2.VyMeanNonNorm(1))/Params.dt];
EN2Params.x0 = [yData.EN2.PxMeanNonNorm(1);
            yData.EN2.PyMeanNonNorm(1);
            yData.EN2.VxMeanNonNorm(1);
            yData.EN2.VyMeanNonNorm(1);
            forceInit_EN2(1);forceInit_EN2(2); 0; 0.1];
SOL_EN2 = OptimalReachSubjData(EN2Params);

% Plot Results

% Plot Average Trajectories
figure
subplot(2,4,1)
plot(yData.EN1.PxMeanNonNorm,yData.EN1.PyMeanNonNorm)
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EN1')

subplot(2,4,2)
hold on;
plot(yData.EF1.PxMeanNonNorm,yData.EF1.PyMeanNonNorm)
plot(SOL_EF1.x(:,1),SOL_EF1.x(:,2),'r')
hold off
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EF1')
set(gca,'Color',[1 1 .53]);

subplot(2,4,3)
plot(yData.EF2.PxMeanNonNorm,yData.EF2.PyMeanNonNorm)
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EF2')

subplot(2,4,4)
hold on;
plot(yData.EN2.PxMeanNonNorm,yData.EN2.PyMeanNonNorm)
plot(SOL_EN2.x(:,1),SOL_EN2.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('EN2')
set(gca,'Color',[1 1 .53]);

subplot(2,4,5)
hold on;
plot(yData.LN1.PxMeanNonNorm,yData.LN1.PyMeanNonNorm)
plot(SOL_LN1.x(:,1),SOL_LN1.x(:,2),'r')
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LN1')
set(gca,'Color',[1 1 .53]);

subplot(2,4,6)
plot(yData.LF1.PxMeanNonNorm,yData.LF1.PyMeanNonNorm)
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LF1')

subplot(2,4,7)
hold on;
p(1) = plot(yData.LF2.PxMeanNonNorm,yData.LF2.PyMeanNonNorm);
p(2) = plot(SOL_LF2.x(:,1),SOL_LF2.x(:,2),'r');
hold off;
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LF2')
set(gca,'Color',[1 1 .53]);
hleg = legend(p,'Experimental','Model Results',...
    'Orientation','Horizontal',...
    'Location','South');
set(hleg,'Color',[1 1 1])
set(hleg,'Position',[0.3478    0.0205    0.3420    0.0453])

subplot(2,4,8)
plot(yData.LN2.PxMeanNonNorm,yData.LN2.PyMeanNonNorm)
axis equal
axis([-0.1 0.1 -0.15 0.15])
title('LN2')

%suptitle('Young Subject Data - Average Trajectories by Phase')
suptitle('Old Subject Data - Average Trajectories by Phase')