% RMS Fit Code for Young Subject

% Looks at Individual Phase

function RMS = RMSfitSubjData_ZScorePosVel(X,subjData,FitIndices,Params)

count = 1;
if FitIndices(1)
    Params.R(1,1) = X(count);
    Params.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.Q(5,5) = X(count);
    Params.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.Q(1,1) = X(count);
    Params.Q(7,7) = X(count);
    Params.Q(1,7) = -1*X(count);
    Params.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.Q(2,2) = X(count);
    Params.Q(8,8) = X(count);
    Params.Q(2,8) = -1*X(count);
    Params.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.Phi(1,1) = X(count);
    Params.Phi(7,7) = X(count);
    Params.Phi(1,7) = -1*X(count);
    Params.Phi(7,1) = -1*X(count);
    Params.Phi(2,2) = X(count);
    Params.Phi(8,8) = X(count);
    Params.Phi(2,8) = -1*X(count);
    Params.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.Phi(3,3) = X(count);
    Params.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.Phi(5,5) = X(count);
    Params.Phi(6,6) = X(count);
    count = count + 1;
end

% First Trajectory: LN1
if FitIndices(9)
    Params.estGain = X(count);
else
    eval(['Params.estGain = subjData.',Params.phase,'.estGainMean;'])
end
eval(['Params.curlGain = subjData.',Params.phase,'.curlGain;'])
eval(['n = length(subjData.',Params.phase,'.PxMean);'])
Params.trialLength = n;
% TEST: Have initial position match experimental data
% eval(['Params.x0 = [subjData.',Params.phase,...
%     '.PxMean(1);subjData.',Params.phase,...
%     '.PyMean(1);subjData.',Params.phase,...
%     '.VxMean(1);subjData.',Params.phase,...
%     '.VyMean(1);0; 0; 0; 0.1];'])

eval(['forceInit = Params.Mass*[subjData.',Params.phase,...
        '.VxMean(2) - subjData.',Params.phase,...
        '.VxMean(1); subjData.',Params.phase,...
        '.VyMean(2) - subjData.',Params.phase,...
        '.VyMean(1)];']);

eval(['Params.x0 = [subjData.',Params.phase,...
    '.PxMean(1);subjData.',Params.phase,...
    '.PyMean(1);subjData.',Params.phase,...
    '.VxMean(1);subjData.',Params.phase,...
    '.VyMean(1);forceInit(1);forceInit(2); 0; 0.1];'])
SOL = OptimalReachSubjData(Params);




% Calculate Error
% Normalized by trial length, number of arrays compared, and by STD so 
% it is essentially comparing z-scores.
% eval(['RMS = sum((subjData.',Params.phase,...
%     '.PxMean'' - SOL.x(:,1)).^2) + sum((subjData.',Params.phase,...
%     '.PyMean'' - SOL.x(:,2)).^2);'])
eval(['RMS = sum(abs((subjData.',Params.phase,...
    '.PxMean'' - SOL.x(:,1))./subjData.',Params.phase,...
    '.PxSE'')) + sum(abs((subjData.',Params.phase,...
    '.PyMean'' - SOL.x(:,2))./subjData.',Params.phase,...
    '.PySE'')) + sum(abs((subjData.',Params.phase,...
    '.VxMean'' - SOL.x(:,3))./subjData.',Params.phase,...
    '.VxSE'')) + sum(abs((subjData.',Params.phase,...
    '.VyMean'' - SOL.x(:,4))./subjData.',Params.phase,...
    '.VySE''));'])

RMS = sqrt(RMS/4/n);



%Regularization
%RMS = RMS + sum(X*1e-15);