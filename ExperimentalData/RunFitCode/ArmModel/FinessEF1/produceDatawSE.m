clear all;clc

load('YoungData-ToFit-4TrialMean-ExtraInitialPts-New.mat')

phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};% Defined By Helen

% 
for j = 1:length(phaseNames)
    
Params.phase = phaseNames{j};
    
% Dynamics
x0 = [0;-0.1;0;0;0;0;0;0.1];
% curlGain = -20;%Ns/m (curl gain)
eval(['curlGain = yData.',Params.phase,'.curlGain;']);%Ns/m (curl gain)
% From experimental Data
dt = 1/200;%sec
Params.dt = dt;

% Arm Inertia (by Max)
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);

% inverse position
x = 0;
y = .4;
D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);
q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian
J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22];

Mass = inv(J)'*Inertia*inv(J);
invMass = inv(Mass);




% Make Parameter Structure
Params.x0 = x0;
Params.Mass = Mass;

%%% Add FxMean and FxSE to Data
% Calculate Estimated Force Values
% Current Hack where F(1) = F(2)...
eval(['n = length(yData.',Params.phase,'.PxMean);'])
eval(['forceInit = Params.Mass*[(yData.',Params.phase,...
        '.VxMean(2) - yData.',Params.phase,...
        '.VxMean(1))/Params.dt; (yData.',Params.phase,...
        '.VyMean(2) - yData.',Params.phase,...
        '.VyMean(1))/Params.dt];']);
eval(['yData.',Params.phase,'.FxMean = zeros(1,n);'])
eval(['yData.',Params.phase,'.FxMean(1) = forceInit(1);'])
eval(['yData.',Params.phase,'.FyMean = zeros(1,n);'])
eval(['yData.',Params.phase,'.FyMean(1) = forceInit(2);'])
eval(['yData.',Params.phase,'.FxSE = zeros(1,n);'])
eval(['yData.',Params.phase,'.FySE = zeros(1,n);'])
eval(['FSEinit = Params.Mass*[yData.',Params.phase,...
    '.VxSE(1)/Params.dt; yData.',Params.phase,...
    '.VySE(1)/Params.dt];'])
eval(['yData.',Params.phase,'.FxSE(1) = FSEinit(1);'])
eval(['yData.',Params.phase,'.FySE(1) = FSEinit(2);'])
for ii = 2:n
    eval(['FMeanTemp = Params.Mass*[(yData.',Params.phase,...
        '.VxMean(ii) - yData.',Params.phase,...
        '.VxMean(ii-1))/Params.dt; (yData.',Params.phase,...
        '.VyMean(ii) - yData.',Params.phase,...
        '.VyMean(ii-1))/Params.dt];'])
    eval(['yData.',Params.phase,'.FxMean(ii) = FMeanTemp(1);'])
    eval(['yData.',Params.phase,'.FyMean(ii) = FMeanTemp(2);'])
    % Not sure of the best way to combine standard errors, for now, take
    % magnitude?
    eval(['FSETemp = Params.Mass*[sqrt(yData.',Params.phase,...
        '.VxSE(ii)^2 + yData.',Params.phase,...
        '.VxSE(ii-1)^2)/Params.dt; sqrt(yData.',Params.phase,...
        '.VySE(ii)^2 + yData.',Params.phase,...
        '.VySE(ii-1)^2)/Params.dt];'])
    eval(['yData.',Params.phase,'.FxSE(ii) = FSETemp(1);'])
    eval(['yData.',Params.phase,'.FySE(ii) = FSETemp(2);'])
end
end