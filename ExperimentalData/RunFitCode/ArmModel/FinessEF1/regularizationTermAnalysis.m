
% load('Young-AllPhasesIndivid-ZScorePosVelAcc-GainConstraints-0_25-20140828.mat')
load('AllPhases-ZScorePosVelAcc-NewGainConstraints-20140821.mat')

subjData = yData;
phaseNames = {'EN1','LN1','EF1','LF1','EF2','LF2','EN2','LN2'};% Defined By Helen

for ii = 1:length(phaseNames)
    
    Params.phase = phaseNames{ii};
    eval(['SOL = Young.',Params.phase,'.Traj;']);

    eval(['RMS = sum(abs((subjData.',Params.phase,...
        '.PxMean'' - SOL.x(:,1))./subjData.',Params.phase,...
        '.PxSE'')) + sum(abs((subjData.',Params.phase,...
        '.PyMean'' - SOL.x(:,2))./subjData.',Params.phase,...
        '.PySE'')) + sum(abs((subjData.',Params.phase,...
        '.VxMean'' - SOL.x(:,3))./subjData.',Params.phase,...
        '.VxSE'')) + sum(abs((subjData.',Params.phase,...
        '.VyMean'' - SOL.x(:,4))./subjData.',Params.phase,...
        '.VySE'')) + sum(abs((subjData.',Params.phase,...
        '.FxMean'' - SOL.x(:,5))./subjData.',Params.phase,...
        '.FxSE'')) + sum(abs((subjData.',Params.phase,...
        '.FyMean'' - SOL.x(:,6))./subjData.',Params.phase,...
        '.FySE''));'])


    RMS = sqrt(RMS/6/n);
    sRMS(ii) = RMS;
    
    % Sum Parameter Values to Add
    eval(['pTemp = Young.',Params.phase,'.Params;'])
    paramSum(ii) = pTemp.Q(1,1) + pTemp.Q(2,2) + pTemp.Q(5,5) + pTemp.R(1,1) + ...
        pTemp.Phi(1,1) + pTemp.Phi(3,3) + pTemp.Phi(5,5) + pTemp.estGain;
    
end

% Results from run on 8/28/14:
%
% RMS = 
%     3.1597
%     1.9311
%     2.6818
%     1.8082
%     2.3822
%     1.8988
%     1.8542
%     1.9099
%
% paramSum = 
%    1.0e+12 *
%     0.0012
%     1.1616
%     0.0003
%     0.0099
%     0.0027
%     0.0042
%     0.0041
%     0.0470
%
% looks like a proper regularization term would be on the order of 10^-12
% to 10^-14.


% Results from run on 8/21/14:
%
% RMS = 
%     2.3529
%     1.6352
%     2.5514
%     1.8046
%     2.3835
%     1.8939
%     1.9204
%     1.7335
%
% paramSum = 
% ans =
%    1.0e+09 *
%     6.2819
%     0.6172
%     9.8275
%     7.3125
%     3.6376
%     5.5104
%     3.2724
%     4.0044
%
% looks like a proper regularization term would be on the order of 10^-9 to
% 10^-13
%
% CONCLUDE: use 10^-13, and see effects.