%% Compare RMS Methods
%
% Pos Only (non-normalized)
% Pos Only (z-score)
% Pos + Vel (z-score)
% Pos + Vel + Force (z-score)
% Pos + Vel + Force + Extra weight on end point
% Pos + Vel + Force + Extra weight on second half

fileNames = {'FinessEF1-PosOnly-20140820.mat',...   % For reference
             'FinessEF1-ZScorePos-20140819.mat',...
             'FinessEF1-ZScorePosVel-20140819.mat',...
             'FinessEF1-ZScorePosVelAcc-20140819.mat',...
             'FinessEF1-ZScorePosVelAccEndPtx10000-20140819.mat',...
             'FinessEF1-ZScorePosVelAcc2ndHalfx100-20140820.mat'};
% fileNames = {'FinessEF1-ZScorePosVelAcc2ndHalfx10-20140820.mat',...
%              'FinessEF1-ZScorePosVelAcc-DecLB-20140820.mat',...
%              'FinessEF1-ZScorePosVelAccEndPosx10000EndVelAccx100-20140819.mat',...
%              'FinessEF1-ZScorePosVelAccEndPtx100-20140819.mat',...
%              'FinessEF1-ZScorePosVelAccEndPtx1000-20140819.mat'};
% fileNames = {'FinessEF1-PosOnly-20140820.mat',...   % For reference
%              'FinessEF1-ZScorePos-20140819.mat',...
%              'FinessEF1-ZScorePosVel-20140819.mat',...
%              'FinessEF1-ZScorePosVelAcc-20140819.mat',...
%              'FinessEF1-ZScorePosVelAccEndPtx10000-20140819.mat',...
%              'FinessEF1-ZScorePosVelAcc2ndHalfx100-20140820.mat',...
%              'FinessEF1-ZScorePosVelAcc2ndHalfx10-20140820.mat',...
%              'FinessEF1-ZScorePosVelAcc-DecLB-20140820.mat',...
%              'FinessEF1-ZScorePosVelAccEndPosx10000EndVelAccx100-20140819.mat',...
%              'FinessEF1-ZScorePosVelAccEndPtx100-20140819.mat',...
%              'FinessEF1-ZScorePosVelAccEndPtx1000-20140819.mat'};
% fileNames = dir('*.mat');

colorArray = hsv(length(fileNames));
load(fileNames{1})
% load(fileNames(1).name)
time = 0:1/200:(length(yData.EF1.PxMean)-1)/200;
legendNames{1} = 'Data';

figure
subplot(1,2,1)
hold on
h(1) = plot(yData.EF1.PxMean,yData.EF1.PyMean,'k');
ciplotY(yData.EF1.PxMean - yData.EF1.PxSE,...
    yData.EF1.PxMean + yData.EF1.PxSE,...
    yData.EF1.PyMean,'k', 0.5)
axis equal
axis([-0.08 0.08 -.15 .15])

subplot(6,2,2)
hold on
plot(time,yData.EF1.PxMean,'k')

subplot(6,2,4)
hold on
plot(time,yData.EF1.PyMean,'k')

subplot(6,2,6)
hold on
plot(time,yData.EF1.VxMean,'k')

subplot(6,2,8)
hold on
plot(time,yData.EF1.VyMean,'k')

subplot(6,2,10)
hold on
plot(time,yData.EF1.FxMean,'k')

subplot(6,2,12)
hold on
plot(time,yData.EF1.FyMean,'k')


for ii = 1:length(fileNames)-1
    load(fileNames{ii})
    legendNames{ii+1} = fileNames{ii};
%     load(fileNames(ii).name)
    subplot(1,2,1)
    h(ii + 1) = plot(Young.EF1.Traj.x(:,1),Young.EF1.Traj.x(:,2),'Color',colorArray(ii,:));
    
    subplot(6,2,2)
    plot(time,Young.EF1.Traj.x(:,1),'Color',colorArray(ii,:));
    
    subplot(6,2,4)
    plot(time,Young.EF1.Traj.x(:,2),'Color',colorArray(ii,:));
    
    subplot(6,2,6)
    plot(time,Young.EF1.Traj.x(:,3),'Color',colorArray(ii,:));

    subplot(6,2,8)
    plot(time,Young.EF1.Traj.x(:,4),'Color',colorArray(ii,:));
    
    subplot(6,2,10)
    plot(time,Young.EF1.Traj.x(:,5),'Color',colorArray(ii,:));
  
    subplot(6,2,12)
    plot(time,Young.EF1.Traj.x(:,6),'Color',colorArray(ii,:));
    
end

% Post-Plotting Formatting
subplot(1,2,1)
title('XY-Trajectory')
legend(h,legendNames)
subplot(6,2,2)
title('X-Position')
xlim([time(1) time(end)])
subplot(6,2,4)
title('Y-Position')
xlim([time(1) time(end)])
subplot(6,2,6)
title('X-Velocity')
xlim([time(1) time(end)])
subplot(6,2,8)
title('Y-Velocity')
xlim([time(1) time(end)])
subplot(6,2,10)
title('X-Force')
xlim([time(1) time(end)])
subplot(6,2,12)
title('Y-Force')
xlim([time(1) time(end)])

suptitle('Comparison of Error Functions: EF1 Trajectory')