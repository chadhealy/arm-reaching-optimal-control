% RMS Fit Code for Young Subject

% Looks at Phases
% - LN1
% - EF1
% - LF2
% - EN2
function RMS = RMSfitSubjData_ConstrGainVals_TwoSetsCosts(X,subjData,FitIndices,Params)

count = 1;
if FitIndices(1)
    Params.phase1.R(1,1) = X(count);
    Params.phase1.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.phase1.Q(5,5) = X(count);
    Params.phase1.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.phase1.Q(1,1) = X(count);
    Params.phase1.Q(7,7) = X(count);
    Params.phase1.Q(1,7) = -1*X(count);
    Params.phase1.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.phase1.Q(2,2) = X(count);
    Params.phase1.Q(8,8) = X(count);
    Params.phase1.Q(2,8) = -1*X(count);
    Params.phase1.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.phase1.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.phase1.Phi(1,1) = X(count);
    Params.phase1.Phi(7,7) = X(count);
    Params.phase1.Phi(1,7) = -1*X(count);
    Params.phase1.Phi(7,1) = -1*X(count);
    Params.phase1.Phi(2,2) = X(count);
    Params.phase1.Phi(8,8) = X(count);
    Params.phase1.Phi(2,8) = -1*X(count);
    Params.phase1.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.phase1.Phi(3,3) = X(count);
    Params.phase1.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.phase1.Phi(5,5) = X(count);
    Params.phase1.Phi(6,6) = X(count);
    count = count + 1;
end
if FitIndices(9)
    Params.phase2.R(1,1) = X(count);
    Params.phase2.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(10)
    Params.phase2.Q(5,5) = X(count);
    Params.phase2.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(11)
    Params.phase2.Q(1,1) = X(count);
    Params.phase2.Q(7,7) = X(count);
    Params.phase2.Q(1,7) = -1*X(count);
    Params.phase2.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(12) 
    Params.phase2.Q(2,2) = X(count);
    Params.phase2.Q(8,8) = X(count);
    Params.phase2.Q(2,8) = -1*X(count);
    Params.phase2.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(13)
    Params.phase2.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(14) 
    Params.phase2.Phi(1,1) = X(count);
    Params.phase2.Phi(7,7) = X(count);
    Params.phase2.Phi(1,7) = -1*X(count);
    Params.phase2.Phi(7,1) = -1*X(count);
    Params.phase2.Phi(2,2) = X(count);
    Params.phase2.Phi(8,8) = X(count);
    Params.phase2.Phi(2,8) = -1*X(count);
    Params.phase2.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(15)
    Params.phase2.Phi(3,3) = X(count);
    Params.phase2.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(16) 
    Params.phase2.Phi(5,5) = X(count);
    Params.phase2.Phi(6,6) = X(count);
    count = count + 1;
end

% First Trajectory: LN1
Params.phase1.estGain = 0;
Params.phase1.curlGain = 0;
nLN1 = length(subjData.LN1.PxMean);
Params.phase1.trialLength = nLN1;
% TEST: Have initial position match experimental data
Params.phase1.x0 = [subjData.LN1.PxMean(1);
            subjData.LN1.PyMean(1);
            subjData.LN1.VxMean(1);
            subjData.LN1.VyMean(1);
            0; 0; 0; 0.1];
SOL_LN1 = OptimalReachSubjData(Params.phase1);

% Second Trajectory: EF1
Params.phase1.estGain = 0;
Params.phase1.curlGain = -20;
nEF1 = length(subjData.EF1.PxMean);
Params.phase1.trialLength = nEF1;
Params.phase1.x0 = [subjData.EF1.PxMean(1);
            subjData.EF1.PyMean(1);
            subjData.EF1.VxMean(1);
            subjData.EF1.VyMean(1);
            0; 0; 0; 0.1];
SOL_EF1 = OptimalReachSubjData(Params.phase1);

% Third Trajectory: LF2
if FitIndices(17)
    Params.phase2.estGain = X(count);
else
    Params.phase2.estGain = subjData.LF2.estGainMean;
end
Params.phase2.curlGain = -20;
nLF2 = length(subjData.LF2.PxMean);
Params.phase2.trialLength = nLF2;
Params.phase2.x0 = [subjData.LF2.PxMean(1);
            subjData.LF2.PyMean(1);
            subjData.LF2.VxMean(1);
            subjData.LF2.VyMean(1);
            0; 0; 0; 0.1];
SOL_LF2 = OptimalReachSubjData(Params.phase2);

% Fourth Trajectory: EN2
if FitIndices(17)
    Params.phase2.estGain = X(count);
else
    Params.phase2.estGain = subjData.EN2.estGainMean;
end
Params.phase2.curlGain = 0;
nEN2 = length(subjData.EN2.PxMean);
Params.phase2.trialLength = nEN2;
Params.phase2.x0 = [subjData.EN2.PxMean(1);
            subjData.EN2.PyMean(1);
            subjData.EN2.VxMean(1);
            subjData.EN2.VyMean(1);
            0; 0; 0; 0.1];
SOL_EN2 = OptimalReachSubjData(Params.phase2);




% Calculate Error
% Normalized by trial length, and number of arrays compared 
RMS_LN1 = sum((subjData.LN1.PxMean' - SOL_LN1.x(:,1)).^2) + ...
    sum((subjData.LN1.PyMean' - SOL_LN1.x(:,2)).^2);
RMS_LN1 = RMS_LN1/2/nLN1;

RMS_EF1 = sum((subjData.EF1.PxMean' - SOL_EF1.x(:,1)).^2) + ...
    sum((subjData.EF1.PyMean' - SOL_EF1.x(:,2)).^2);
RMS_EF1 = RMS_EF1/2/nEF1;

RMS_LF2 = sum((subjData.LF2.PxMean' - SOL_LF2.x(:,1)).^2) + ...
    sum((subjData.LF2.PyMean' - SOL_LF2.x(:,2)).^2);
RMS_LF2 = RMS_LF2/2/nLF2;

RMS_EN2 = sum((subjData.EN2.PxMean' - SOL_EN2.x(:,1)).^2) + ...
    sum((subjData.EN2.PyMean' - SOL_EN2.x(:,2)).^2);
RMS_EN2 = RMS_EN2/2/nEN2;

RMS = sqrt((RMS_LN1 + RMS_EF1 + RMS_LF2 + RMS_EN2)/4);
