% RMS Fit Code for Young Subject

% Looks at Individual Phase

function RMS = RMSfitSubjData_SingleTrajwTermCond(X,subjData,FitIndices,Params)

count = 1;
if FitIndices(1)
    Params.R(1,1) = X(count);
    Params.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.Q(5,5) = X(count);
    Params.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.Q(1,1) = X(count);
    Params.Q(7,7) = X(count);
    Params.Q(1,7) = -1*X(count);
    Params.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.Q(2,2) = X(count);
    Params.Q(8,8) = X(count);
    Params.Q(2,8) = -1*X(count);
    Params.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.Phi(1,1) = X(count);
    Params.Phi(7,7) = X(count);
    Params.Phi(1,7) = -1*X(count);
    Params.Phi(7,1) = -1*X(count);
    Params.Phi(2,2) = X(count);
    Params.Phi(8,8) = X(count);
    Params.Phi(2,8) = -1*X(count);
    Params.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.Phi(3,3) = X(count);
    Params.Phi(9,9) = X(count);
    Params.Phi(3,9) = -1*X(count);
    Params.Phi(9,3) = -1*X(count);
    Params.Phi(4,4) = X(count);
    Params.Phi(10,10) = X(count);
    Params.Phi(4,10) = -1*X(count);
    Params.Phi(10,4) = -1*X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.Phi(5,5) = X(count);
    Params.Phi(11,11) = X(count);
    Params.Phi(5,11) = -1*X(count);
    Params.Phi(11,5) = -1*X(count);
    Params.Phi(6,6) = X(count);
    Params.Phi(12,12) = X(count);
    Params.Phi(6,12) = -1*X(count);
    Params.Phi(12,6) = -1*X(count);
    count = count + 1;
end

% First Trajectory: LN1
if FitIndices(9)
    Params.estGain = X(count);
else
    eval(['Params.estGain = subjData.',Params.phase,'.estGainMean;'])
end
eval(['Params.curlGain = subjData.',Params.phase,'.curlGain;'])
eval(['n = length(subjData.',Params.phase,'.PxMean);'])
Params.trialLength = n;
% TEST: Have initial position match experimental data
% eval(['Params.x0 = [subjData.',Params.phase,...
%     '.PxMean(1);subjData.',Params.phase,...
%     '.PyMean(1);subjData.',Params.phase,...
%     '.VxMean(1);subjData.',Params.phase,...
%     '.VyMean(1);0; 0; 0; 0.1];'])

eval(['forceInit = Params.Mass*[subjData.',Params.phase,...
        '.VxMean(2) - subjData.',Params.phase,...
        '.VxMean(1); subjData.',Params.phase,...
        '.VyMean(2) - subjData.',Params.phase,...
        '.VyMean(1)];']);
    
eval(['forceFinal = Params.Mass*[subjData.',Params.phase,...
        '.VxMean(end) - subjData.',Params.phase,...
        '.VxMean(end-1); subjData.',Params.phase,...
        '.VyMean(end) - subjData.',Params.phase,...
        '.VyMean(end-1)];']);


eval(['Params.x0 = [subjData.',Params.phase,...
    '.PxMean(1);subjData.',Params.phase,...
    '.PyMean(1);subjData.',Params.phase,...
    '.VxMean(1);subjData.',Params.phase,...
    '.VyMean(1);forceInit(1);forceInit(2);subjData.',...
    Params.phase,'.PxMean(end);subjData.',...
    Params.phase,'.PyMean(end);subjData.',...
    Params.phase,'.VxMean(end);subjData.',...
    Params.phase,'.VyMean(end);',...
    'forceFinal(1);forceFinal(2)];'])

SOL = OptimalReachSubjData(Params);




% Calculate Error
% Normalized by trial length, and number of arrays compared 
eval(['RMS = sum((subjData.',Params.phase,...
    '.PxMean'' - SOL.x(:,1)).^2) + sum((subjData.',Params.phase,...
    '.PyMean'' - SOL.x(:,2)).^2);'])
RMS = sqrt(RMS/2/n);



%Regularization
%RMS = RMS + sum(X*1e-15);