% RMS Fit Code for Young Subject

% Looks at Phases
% - LN1
% - EF1
% - LF2
% - EN2
function RMS = RMSfitSubjData_4Phase_ZScorePosVelAcc(X,subjData,FitIndices,Params)


count = 1;
if FitIndices(1)
    Params.R(1,1) = X(count);
    Params.R(2,2) = X(count);
    count = count + 1;
end
if FitIndices(2)
    Params.Q(5,5) = X(count);
    Params.Q(6,6) = X(count);
    count = count + 1;
end
if FitIndices(3)
    Params.Q(1,1) = X(count);
    Params.Q(7,7) = X(count);
    Params.Q(1,7) = -1*X(count);
    Params.Q(7,1) = -1*X(count);
    count = count + 1;
end
if FitIndices(4) 
    Params.Q(2,2) = X(count);
    Params.Q(8,8) = X(count);
    Params.Q(2,8) = -1*X(count);
    Params.Q(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(5)
    Params.Q(3,3) = X(count);
    count = count + 1;
end
if FitIndices(6) 
    Params.Phi(1,1) = X(count);
    Params.Phi(7,7) = X(count);
    Params.Phi(1,7) = -1*X(count);
    Params.Phi(7,1) = -1*X(count);
    Params.Phi(2,2) = X(count);
    Params.Phi(8,8) = X(count);
    Params.Phi(2,8) = -1*X(count);
    Params.Phi(8,2) = -1*X(count);
    count = count + 1;
end
if FitIndices(7)
    Params.Phi(3,3) = X(count);
    Params.Phi(4,4) = X(count);
    count = count + 1;
end
if FitIndices(8) 
    Params.Phi(5,5) = X(count);
    Params.Phi(6,6) = X(count);
    count = count + 1;
end

% First Trajectory: LN1
if FitIndices(9)
    Params.estGain = X(count);
    count = count + 1;
else
    Params.estGain = subjData.LN1.estGainMean;
end
Params.curlGain = 0;
nLN1 = length(subjData.LN1.PxMean);
Params.trialLength = nLN1;        
forceInit_LN1 = Params.Mass*[(subjData.LN1.VxMean(2) - subjData.LN1.VxMean(1))/Params.dt;
                         (subjData.LN1.VyMean(2) - subjData.LN1.VyMean(1))/Params.dt];

Params.x0 = [subjData.LN1.PxMean(1);
            subjData.LN1.PyMean(1);
            subjData.LN1.VxMean(1);
            subjData.LN1.VyMean(1);
            forceInit_LN1(1);forceInit_LN1(2); 0; 0.1];
        
SOL_LN1 = OptimalReachSubjData(Params);
SOL_LN1.Ax = NumDiff_3pt(SOL_LN1.x(:,3),SOL_LN1.time);
SOL_LN1.Ay = NumDiff_3pt(SOL_LN1.x(:,4),SOL_LN1.time);

% Second Trajectory: EF1
if FitIndices(10)
    Params.estGain = X(count);
    count = count + 1;
else
    Params.estGain = subjData.EF1.estGainMean;
end
Params.curlGain = -20;
nEF1 = length(subjData.EF1.PxMean);
Params.trialLength = nEF1;
forceInit_EF1 = Params.Mass*[(subjData.EF1.VxMean(2) - subjData.EF1.VxMean(1))/Params.dt;
                         (subjData.EF1.VyMean(2) - subjData.EF1.VyMean(1))/Params.dt];
Params.x0 = [subjData.EF1.PxMean(1);
            subjData.EF1.PyMean(1);
            subjData.EF1.VxMean(1);
            subjData.EF1.VyMean(1);
            forceInit_EF1(1);forceInit_EF1(2); 0; 0.1];
SOL_EF1 = OptimalReachSubjData(Params);
SOL_EF1.Ax = NumDiff_3pt(SOL_EF1.x(:,3),SOL_EF1.time);
SOL_EF1.Ay = NumDiff_3pt(SOL_EF1.x(:,4),SOL_EF1.time);

% Third Trajectory: LF2
if FitIndices(11)
    Params.estGain = X(count);
    count = count + 1;
else
    Params.estGain = subjData.LF2.estGainMean;
end
Params.curlGain = -20;
nLF2 = length(subjData.LF2.PxMean);
Params.trialLength = nLF2;
forceInit_LF2 = Params.Mass*[(subjData.LF2.VxMean(2) - subjData.LF2.VxMean(1))/Params.dt;
                         (subjData.LF2.VyMean(2) - subjData.LF2.VyMean(1))/Params.dt];
Params.x0 = [subjData.LF2.PxMean(1);
            subjData.LF2.PyMean(1);
            subjData.LF2.VxMean(1);
            subjData.LF2.VyMean(1);
            forceInit_LF2(1);forceInit_LF2(2); 0; 0.1];
SOL_LF2 = OptimalReachSubjData(Params);
SOL_LF2.Ax = NumDiff_3pt(SOL_LF2.x(:,3),SOL_LF2.time);
SOL_LF2.Ay = NumDiff_3pt(SOL_LF2.x(:,4),SOL_LF2.time);

% Fourth Trajectory: EN2
if FitIndices(12)
    Params.estGain = X(count);
else
    Params.estGain = subjData.EN2.estGainMean;
end
Params.curlGain = 0;
nEN2 = length(subjData.EN2.PxMean);
Params.trialLength = nEN2;
forceInit_EN2 = Params.Mass*[(subjData.EN2.VxMean(2) - subjData.EN2.VxMean(1))/Params.dt;
                         (subjData.EN2.VyMean(2) - subjData.EN2.VyMean(1))/Params.dt];
Params.x0 = [subjData.EN2.PxMean(1);
            subjData.EN2.PyMean(1);
            subjData.EN2.VxMean(1);
            subjData.EN2.VyMean(1);
            forceInit_EN2(1);forceInit_EN2(2); 0; 0.1];
SOL_EN2 = OptimalReachSubjData(Params);
SOL_EN2.Ax = NumDiff_3pt(SOL_EN2.x(:,3),SOL_EN2.time);
SOL_EN2.Ay = NumDiff_3pt(SOL_EN2.x(:,4),SOL_EN2.time);


% Calculate Error
RMS_LN1 = sum(((subjData.LN1.PxMean' - SOL_LN1.x(:,1))./subjData.LN1.PxSE')).^2 + ...
    sum(((subjData.LN1.PyMean' - SOL_LN1.x(:,2))./subjData.LN1.PySE')).^2 + ...
    sum(((subjData.LN1.VxMean' - SOL_LN1.x(:,3))./subjData.LN1.VxSE')).^2 + ...
    sum(((subjData.LN1.VyMean' - SOL_LN1.x(:,4))./subjData.LN1.VySE')).^2 + ...
    sum(((subjData.LN1.AxMean' - SOL_LN1.Ax)./subjData.LN1.AxSE')).^2 + ...
    sum(((subjData.LN1.AyMean' - SOL_LN1.Ay)./subjData.LN1.AySE')).^2;
RMS_LN1 = (RMS_LN1/6/nLN1);

RMS_EF1 = sum(abs((subjData.EF1.PxMean' - SOL_EF1.x(:,1))./subjData.EF1.PxSE')).^2 + ...
    sum(((subjData.EF1.PyMean' - SOL_EF1.x(:,2))./subjData.EF1.PySE')).^2 + ...
    sum(((subjData.EF1.VxMean' - SOL_EF1.x(:,3))./subjData.EF1.VxSE')).^2 + ...
    sum(((subjData.EF1.VyMean' - SOL_EF1.x(:,4))./subjData.EF1.VySE')).^2 + ...
    sum(((subjData.EF1.AxMean' - SOL_EF1.Ax)./subjData.EF1.AxSE')).^2 + ...
    sum(((subjData.EF1.AyMean' - SOL_EF1.Ay)./subjData.EF1.AySE')).^2;
RMS_EF1 = (RMS_EF1/6/nEF1);

RMS_LF2 = sum(((subjData.LF2.PxMean' - SOL_LF2.x(:,1))./subjData.LF2.PxSE')).^2 + ...
    sum(((subjData.LF2.PyMean' - SOL_LF2.x(:,2))./subjData.LF2.PySE')).^2 + ...
    sum(((subjData.LF2.VxMean' - SOL_LF2.x(:,3))./subjData.LF2.VxSE')).^2 + ...
    sum(((subjData.LF2.VyMean' - SOL_LF2.x(:,4))./subjData.LF2.VySE')).^2 + ...
    sum(((subjData.LF2.AxMean' - SOL_LF2.Ax)./subjData.LF2.AxSE')).^2 + ...
    sum(((subjData.LF2.AyMean' - SOL_LF2.Ay)./subjData.LF2.AySE')).^2;
RMS_LF2 = (RMS_LF2/6/nLF2);

RMS_EN2 = sum(((subjData.EN2.PxMean' - SOL_EN2.x(:,1))./subjData.EN2.PxSE')).^2 + ...
    sum(((subjData.EN2.PyMean' - SOL_EN2.x(:,2))./subjData.EN2.PySE')).^2 + ...
    sum(((subjData.EN2.VxMean' - SOL_EN2.x(:,3))./subjData.EN2.VxSE')).^2 + ...
    sum(((subjData.EN2.VyMean' - SOL_EN2.x(:,4))./subjData.EN2.VySE')).^2 + ...
    sum(((subjData.EN2.AxMean' - SOL_EN2.Ax)./subjData.EN2.AxSE')).^2 + ...
    sum(((subjData.EN2.AyMean' - SOL_EN2.Ay)./subjData.EN2.AySE')).^2;
RMS_EN2 = (RMS_EN2/6/nEN2);

RMS = (RMS_LN1 + RMS_EF1 + RMS_LF2 + RMS_EN2)/4;
%Revalation: I dont need to sqrt these, I'm not squaring the difference of  distance...


