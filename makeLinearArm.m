
%
% code to compute linarized model of limb dynamics
%
% linearize limb dynamics by compute partial derivative of limb dynamics
% about nominal joint state. then we have linearized limb dynamics of form
% 
% Delta Theta/dot = A Delta Theta + B u
%
% note, not using delta u, since limb dynamics are linear in u to begin
% with, and don't want to have to compute delta torques, seems kind of
% clumsy.
%
% then, compute linearized map from joint state to end point state, 
%
% X = g(Theta) (which is the kinematics and jacobian), then get 
%
% Delta X = G Delta Theta,
%
% then linarized dynamics using hand/end point state are
%
% Delta X/dt = G*A*inv(G) X + G*B*u
%

% values in shadmehr and mussa-ivaldi
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

% values used in nature neuroscience paper
Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];
K = zeros(4);
K(1:2,1:2) = Pbody;
K(3:4,3:4) = Dbody;

% damping to ground
Bbody = [.2 .01; .01 .1];%zeros(2);
Kbody = zeros(2);


q1 = pi/4;
q2 = pi/3;
q1d = 0;
q2d = 0;
T1 = 0;
T2 = 0;

s2 = sin(q2);
c2 = cos(q2);
s1 = sin(q1);
c1 = cos(q1);
s12 = sin(q1+q2);
c12 = cos(q1+q2);

H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*c2;
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*c2 ) + I_2;
H22 = m2*a2_cm^2 + I_2;
h = -m2*a1*a2_cm*s2;

Inertia = [H11 H12; H12 H22];

den = ( H11 - H12*H12/H22 );
part = T2 + h*q1d^2;
f1 = ( T1 - h*q2d^2 - 2*h*q1d*q2d - H12*part/H22 ) / den;

% derivatives of link parameters 
h_q2 = -m2*a1*a2_cm*c2;
H11_q2 = -2*m2*a1*a2_cm*s2;
H12_q2 = -m2*a1*a2_cm*s2;

% derivatives of rate equations

f1_q1 = 0;

f1_q2 = ( -H12_q2*( T2 + h*q1d*q1d )/H22 ...
    - h_q2*(H12*q1d*q1d/H22 + 2*q1d*q2d + q2d*q2d) )/den ...
    - f1*( H11_q2 - 2*H12*H12_q2/H22 )/den;

f1_q1d = ( -H12*( 2*h*q1d )/H22 - 2*h*q2d )/den;

f1_q2d = ( -2*h*(q1d + q2d) )/den;

%
f2_q1 = ( -H12 * f1_q1 )/ H22;

f2_q2 = ( h_q2*q1d*q1d - H12_q2*f1 - H12*f1_q2 ) / H22;

f2_q1d = ( 2*h*q1d - H12*f1_q1d )/H22;

f2_q2d = ( -H12*f1_q2d )/H22;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% assemble partials
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

F_X = zeros(4);

F_X(1,3) = 1;
F_X(2,4) = 1;
%
F_X(3,1) = f1_q1;
F_X(3,2) = f1_q2;
F_X(3,3) = f1_q1d;
F_X(3,4) = f1_q2d;
%
F_X(4,1) = f2_q1;
F_X(4,2) = f2_q2;
F_X(4,3) = f2_q1d;
F_X(4,4) = f2_q2d;

F_U = zeros(4,2);
F_U(3:4,:) = inv(Inertia);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% now make linarized forward map
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

G = zeros(4);

% dx/dtheta
G(1,1) = -a1*s1 - a2*s12;
G(1,2) = -a2*s12;
% dy/dtheta
G(2,1) = a1*c1 + a2*c12;
G(2,2) = a2*c12;
% dvx/dtheta
G(3,1) = -a1*c1*q1d - a2*c12*(q1d + q2d);
G(3,2) = -a2*c12*(q1d + q2d);
% dvx/dtheta_d
G(3,3) = -a1*s1 - a2*s12;
G(3,4) = -a2*s12;
% dvy/dtheta
G(4,1) = -a1*s1*q1d - a2*s12*(q1d + q2d);
G(4,2) = -a2*s12*(q1d + q2d);
% dvy/dtheta_d
G(4,3) = a1*c1 + a2*c12;
G(4,4) = a2*c12;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% now make linarized limb dynamics in end-point state
%
% Xsi_dot = A*Xsi + B*U;  X = G*Xsi
%
% X_dot = G*A*inv(G) + G*B*U
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A = G*F_X/G;
B = G*F_U;

A = G*(F_X - K)/G;


%%

%
% second approach, based on matching energy between rotaitonal and
% translations domains.
%


% values in shadmehr and mussa-ivaldi
a1 = .33;
a2 = .34;
m1 = 1.93;
m2 = 1.52;
a1_cm = .165;
a2_cm = .19;
I_1 = .0141;
I_2 = .0188;

Pbody = [32 16; 16 21];
Dbody = [5 3; 3 4];

% damping to ground
%Bbody = [.2 .01; .01 .1];%zeros(2);
%Kbody = zeros(2);

% inverse position

x = 0;
y = .4;

D = (x^2 + y^2 - a1^2 - a2^2)/(2*a1*a2);

q(2) = atan2(abs(sqrt(1-D^2)),D);
q(1) = atan2(y,x) - atan2(a2*sin(q(2)),(a1 + a2*cos(q(2))));


% jacobian

J(1,1) = -(a1*sin(q(1)) + a2*sin(q(1) + q(2)));
J(1,2) = -a2*sin(q(1) + q(2));
J(2,1) = a1*cos(q(1)) + a2*cos(q(1) + q(2));
J(2,2) = a2*cos(q(1) + q(2));

% inertia matrix?
H11_a = m1*a1_cm^2;
H11_b = a1^2 + a2_cm^2 + 2*a1*a2_cm*cos(q(2));
H11 = H11_a + m2*H11_b + I_1 + I_2;
H12 = m2*( a2_cm^2 + a1*a2_cm*cos(q(2)) ) + I_2;
H22 = m2*a2_cm^2 + I_2;

Inertia = [H11 H12; H12 H22]

Mass = inv(J)'*Inertia*inv(J)

Stiff = inv(J)'*Pbody*inv(J)

Damp = inv(J)'*Dbody*inv(J)

